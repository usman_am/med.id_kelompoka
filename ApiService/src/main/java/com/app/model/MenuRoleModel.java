package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "m_menu_role")
public class MenuRoleModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private long id;
	
	@Column(name ="menuId", nullable = true)
	private long menuid;
	
	@Column(name ="roleId", nullable = true)
	private long roleid;
	
	@Column(name ="createBy", nullable = false)
	private long createby;
	
	@Column(name ="createOn", nullable = false)
	private LocalDateTime createon;
	
	@Column(name ="modifiedBy")
	private long modifiedby;
	
	@Column(name ="modifiedOn", columnDefinition ="timestamp default now()")
	private LocalDateTime modifiedon;
	
	@Column(name ="deleteBy")
	private long deleteby;
	
	@Column(name ="deleteOn", columnDefinition ="timestamp default now()")
	private LocalDateTime deleteon;
	
	@Column(name ="isDelete", nullable = false,columnDefinition = "boolean default false")
	private boolean isdelete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMenuid() {
		return menuid;
	}

	public void setMenuid(long menuid) {
		this.menuid = menuid;
	}

	public long getCode() {
		return roleid;
	}

	public void setCode(long code) {
		this.roleid = code;
	}

	public long getCreateby() {
		return createby;
	}

	public void setCreateby(long createby) {
		this.createby = createby;
	}

	public LocalDateTime getCreateon() {
		return createon;
	}

	public void setCreateon(LocalDateTime createon) {
		this.createon = createon;
	}

	public long getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(long modifiedby) {
		this.modifiedby = modifiedby;
	}

	public LocalDateTime getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(LocalDateTime modifiedon) {
		this.modifiedon = modifiedon;
	}

	public long getDeleteby() {
		return deleteby;
	}

	public void setDeleteby(long deleteby) {
		this.deleteby = deleteby;
	}

	public LocalDateTime getDeleteon() {
		return deleteon;
	}

	public void setDeleteon(LocalDateTime deleteon) {
		this.deleteon = deleteon;
	}

	public boolean isIsdelete() {
		return isdelete;
	}

	public void setIsdelete(boolean isdelete) {
		this.isdelete = isdelete;
	}
	
	

}
