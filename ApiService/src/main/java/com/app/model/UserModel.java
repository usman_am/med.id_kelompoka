package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mUser")
public class UserModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "biodataId")
	private long biodata_id;
	
	@Column(name = "roleId")
	private long role_id;
	
	@Column(name = "email", length = 100)
	private String email;
	
	@Column(name = "password", length = 255)
	private String password;
	
	@Column(name = "loginAttempt")
	private int login_attempt;
	
	@Column(name = "isLocked")
	private boolean is_locked;
	
	@Column(name = "lastLogin")
	private LocalDateTime last_login;
	
	@Column(name = "createdBy")
	private long created_by;
	
	@Column(name = "createdOn")
	private LocalDateTime created_on;
	
	@Column(name = "modifiedBy")
	private long modified_by;
	
	@Column(name = "modifiedOn")
	private LocalDateTime modified_on;
	
	@Column(name = "deletedBy")
	private long deleteded_by;
	
	@Column(name = "deletedOn")
	private LocalDateTime deleted_on;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean is_delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBiodata_id() {
		return biodata_id;
	}

	public void setBiodata_id(long biodata_id) {
		this.biodata_id = biodata_id;
	}

	public long getRole_id() {
		return role_id;
	}

	public void setRole_id(long role_id) {
		this.role_id = role_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getLogin_attempt() {
		return login_attempt;
	}

	public void setLogin_attempt(int login_attempt) {
		this.login_attempt = login_attempt;
	}

	public boolean isIs_locked() {
		return is_locked;
	}

	public void setIs_locked(boolean is_locked) {
		this.is_locked = is_locked;
	}

	public LocalDateTime getLast_login() {
		return last_login;
	}

	public void setLast_login(LocalDateTime last_login) {
		this.last_login = last_login;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public LocalDateTime getCreated_on() {
		return created_on;
	}

	public void setCreated_on(LocalDateTime created_on) {
		this.created_on = created_on;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public LocalDateTime getModified_on() {
		return modified_on;
	}

	public void setModified_on(LocalDateTime modified_on) {
		this.modified_on = modified_on;
	}

	public long getDeleteded_by() {
		return deleteded_by;
	}

	public void setDeleteded_by(long deleteded_by) {
		this.deleteded_by = deleteded_by;
	}

	public LocalDateTime getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(LocalDateTime deleted_on) {
		this.deleted_on = deleted_on;
	}

	public boolean isIs_delete() {
		return is_delete;
	}

	public void setIs_delete(boolean is_delete) {
		this.is_delete = is_delete;
	}
}
