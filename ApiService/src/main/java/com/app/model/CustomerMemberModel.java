package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_customer_member")
public class CustomerMemberModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "customer_id")
	private long customerId;

	@Column(name = "parent_biodata_id")
	private long parentBiodataId;

	@Column(name = "customer_relation_id")
	private long customerRelationId;
	
	@Column(name = "created_by")
	private long createdBy;
	
	@Column(name = "created_on")
	private LocalDateTime createdOn;
	
	@Column(name = "modifed_by")
	private long modifedBy;
	
	@Column(name = "modifed_on")
	private LocalDateTime modifedOn;
	
	@Column(name = "deleted_by")
	private long deletedBy;
	
	@Column(name = "deleted_on")
	private LocalDateTime deletedOn;
	
	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private boolean delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getParentBiodataId() {
		return parentBiodataId;
	}

	public void setParentBiodataId(long parentBiodataId) {
		this.parentBiodataId = parentBiodataId;
	}

	public long getCustomerRelationId() {
		return customerRelationId;
	}

	public void setCustomerRelationId(long customerRelationId) {
		this.customerRelationId = customerRelationId;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifedBy() {
		return modifedBy;
	}

	public void setModifedBy(long modifedBy) {
		this.modifedBy = modifedBy;
	}

	public LocalDateTime getModifedOn() {
		return modifedOn;
	}

	public void setModifedOn(LocalDateTime modifedOn) {
		this.modifedOn = modifedOn;
	}

	public long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}


}
