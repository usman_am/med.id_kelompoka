package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.scheduling.annotation.Scheduled;

@Entity
@Table(name = "tToken")
public class TokenModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "email", length = 100)
	private String email;
	
	@Column(name = "userId")
	private long user_id;
	
	@Column(name = "token", length = 50)
	private String token;
	
	@Column(name = "expiredOn")
	private LocalDateTime expired_on;
	
	@Column(name = "isExpired")
	private boolean is_expired;
	
	@Column(name = "usedFor")
	private String used_for;
	
	@Column(name = "createdBy")
	private long created_by;
	
	@Column(name = "createdOn")
	private LocalDateTime created_on;
	
	@Column(name = "modifiedBy")
	private long modified_by;
	
	@Column(name = "modifiedOn")
	private LocalDateTime modified_on;
	
	@Column(name = "deletedBy")
	private long deleteded_by;
	
	@Column(name = "deletedOn")
	private LocalDateTime deleted_on;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean is_delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public LocalDateTime getExpired_on() {
		return expired_on;
	}

	public void setExpired_on(LocalDateTime expired_on) {
		this.expired_on = expired_on;
	}

	public boolean isIs_expired() {
		return is_expired;
	}

	public void setIs_expired(boolean is_expired) {
		this.is_expired = is_expired;
	}

	public String getUsed_for() {
		return used_for;
	}

	public void setUsed_for(String used_for) {
		this.used_for = used_for;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public LocalDateTime getCreated_on() {
		return created_on;
	}

	public void setCreated_on(LocalDateTime created_on) {
		this.created_on = created_on;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public LocalDateTime getModified_on() {
		return modified_on;
	}

	public void setModified_on(LocalDateTime modified_on) {
		this.modified_on = modified_on;
	}

	public long getDeleteded_by() {
		return deleteded_by;
	}

	public void setDeleteded_by(long deleteded_by) {
		this.deleteded_by = deleteded_by;
	}

	public LocalDateTime getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(LocalDateTime deleted_on) {
		this.deleted_on = deleted_on;
	}

	public boolean isIs_delete() {
		return is_delete;
	}

	public void setIs_delete(boolean is_delete) {
		this.is_delete = is_delete;
	}
	
	

}
