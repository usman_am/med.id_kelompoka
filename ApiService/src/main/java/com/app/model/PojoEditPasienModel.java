package com.app.model;

import java.util.Date;

public class PojoEditPasienModel {

	private long customer_id;
	private String fullname;
	private String gender;
	
	private Date dob;
	private long blood_group_id;
	private String rhesus_type;
	private double height;
	private double weight;
	private long customer_relation_id;
	private long biodata_id;
	private long customer_member_id;
	
	
	
	
	public PojoEditPasienModel(long customer_id, String fullname, String gender, Date dob, long blood_group_id,
			String rhesus_type, double height, double weight, long customer_relation_id, long biodata_id, long customer_member_id) {
		super();
		this.customer_id = customer_id;
		this.fullname = fullname;
		this.gender = gender;
		this.dob = dob;
		this.blood_group_id = blood_group_id;
		this.rhesus_type = rhesus_type;
		this.height = height;
		this.weight = weight;
		this.customer_relation_id = customer_relation_id;
		this.biodata_id = biodata_id;
		this.customer_member_id = customer_member_id;
	}
	
	public PojoEditPasienModel() {

	}
	
	public long getCustomer_member_id() {
		return customer_member_id;
	}

	public void setCustomer_member_id(long customer_member_id) {
		this.customer_member_id = customer_member_id;
	}

	public long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(long customer_id) {
		this.customer_id = customer_id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public long getBlood_group_id() {
		return blood_group_id;
	}
	public void setBlood_group_id(long blood_group_id) {
		this.blood_group_id = blood_group_id;
	}
	public String getRhesus_type() {
		return rhesus_type;
	}
	public void setRhesus_type(String rhesus_type) {
		this.rhesus_type = rhesus_type;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public long getCustomer_relation_id() {
		return customer_relation_id;
	}
	public void setCustomer_relation_id(long customer_relation_id) {
		this.customer_relation_id = customer_relation_id;
	}
	public long getBiodata_id() {
		return biodata_id;
	}
	public void setBiodata_id(long biodata_id) {
		this.biodata_id = biodata_id;
	}
	
}
