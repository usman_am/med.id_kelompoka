package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="t_appointment_reschedule_history")
public class RescheduleJanjiModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name="appointment_id")
	private long appointmentId;
	
	@Column(name="customer_id")
	private long customerId;
	
	@Column(name="doctor_office_id")
	private long doctorOfficeId;
	
	@Column(name="doctor_office_schedule_id")
	private long doctorOfficeScheduleId;
	
	@Column(name="doctor_office_treatment_id")
	private long doctorOfficeTreatmentId;
	
	@Column(name="appointment_date")
	private LocalDateTime appointmentDate;
	
	@Column(name = "created_by", nullable = true)
	private long createdBy;

	@Column(name = "created_on")
	private LocalDateTime createdOn;

	@Column(name = "modify_by")
	private long modifyBy;

	@Column(name = "modify_on")
	private LocalDateTime modifyOn;

	@Column(name = "delete_by")
	private long deleteBy;

	@Column(name = "delete_on")
	private LocalDateTime deleteOn;

	@Column(name = "is_delete",columnDefinition = "boolean default false")
	private boolean delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getAppointmentId() {
		return appointmentId;
	}

	public void setAppointmentId(long appointmentId) {
		this.appointmentId = appointmentId;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getDoctorOfficeId() {
		return doctorOfficeId;
	}

	public void setDoctorOfficeId(long doctorOfficeId) {
		this.doctorOfficeId = doctorOfficeId;
	}

	public long getDoctorOfficeScheduleId() {
		return doctorOfficeScheduleId;
	}

	public void setDoctorOfficeScheduleId(long doctorOfficeScheduleId) {
		this.doctorOfficeScheduleId = doctorOfficeScheduleId;
	}

	public long getDoctorOfficeTreatmentId() {
		return doctorOfficeTreatmentId;
	}

	public void setDoctorOfficeTreatmentId(long doctorOfficeTreatmentId) {
		this.doctorOfficeTreatmentId = doctorOfficeTreatmentId;
	}

	public LocalDateTime getAppointmentDate() {
		return appointmentDate;
	}

	public void setAppointmentDate(LocalDateTime appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(long modifyBy) {
		this.modifyBy = modifyBy;
	}

	public LocalDateTime getModifyOn() {
		return modifyOn;
	}

	public void setModifyOn(LocalDateTime modifyOn) {
		this.modifyOn = modifyOn;
	}

	public long getDeleteBy() {
		return deleteBy;
	}

	public void setDeleteBy(long deleteBy) {
		this.deleteBy = deleteBy;
	}

	public LocalDateTime getDeleteOn() {
		return deleteOn;
	}

	public void setDeleteOn(LocalDateTime deleteOn) {
		this.deleteOn = deleteOn;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
}
