package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_doctor")
public class DokterModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "biodata_id")
	private long biodataId;
	
	@Column(name = "str",length = 50)
	private String str;
	
	@Column(name = "created_by")
	private long createdBy;
	
	@Column(name = "created_on")
	private LocalDateTime createdOn;
	
	@Column(name = "modified_by")
	private long modifiedBy;
	
	@Column(name = "modified_on")
	private LocalDateTime modifiedOn;
	
	@Column(name = "deleted_by")
	private long deletedBy;
	
	@Column(name = "deleted_on")
	private LocalDateTime deletedOn;
	
	
	@Column(name = "is_delete",columnDefinition = "boolean default false")
	private boolean delete;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public long getBiodataId() {
		return biodataId;
	}


	public void setBiodataId(long biodataId) {
		this.biodataId = biodataId;
	}


	public String getStr() {
		return str;
	}


	public void setStr(String str) {
		this.str = str;
	}


	public long getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}


	public LocalDateTime getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}


	public long getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}


	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}


	public long getDeletedBy() {
		return deletedBy;
	}


	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}


	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}


	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}


	public boolean isDelete() {
		return delete;
	}


	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
	
	

}
