package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.web.bind.annotation.RequestMapping;

@Entity
@Table(name = "m_doctor_education")
public class DokterEducationModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "doctor_id")
	private long doctorId;
	
	@Column(name = "education_level_id")
	private long educationLevelId;
	
	@Column(name = "institution_name",length = 100)
	private String institutionName;
	
	@Column(name = "major",length = 100)
	private String major;
	
	@Column(name = "start_year",length = 4)
	private String  startYear;
	
	@Column(name = "end_year",length = 4)
	private String  endYear;
	
	@Column(name = "is_last_education",columnDefinition = "boolean default false")
	private boolean lastEducation;
	
	@Column(name = "created_by")
	private long createdBy;
	
	@Column(name = "created_on")
	private LocalDateTime createdOn;
	
	@Column(name = "modified_by")
	private long modifiedBy;
	
	@Column(name = "modified_on")
	private LocalDateTime modifiedOn;
	
	@Column(name = "deleted_by")
	private long deletedBy;
	
	@Column(name = "deleted_on")
	private LocalDateTime deletedOn;
	
	
	@Column(name = "is_delete",columnDefinition = "boolean default false")
	private boolean delete;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public long getDoctorId() {
		return doctorId;
	}


	public void setDoctorId(long doctorId) {
		this.doctorId = doctorId;
	}


	public long getEducationLevelId() {
		return educationLevelId;
	}


	public void setEducationLevelId(long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}


	public String getInstitutionName() {
		return institutionName;
	}


	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}


	public String getMajor() {
		return major;
	}


	public void setMajor(String major) {
		this.major = major;
	}


	public String getStartYear() {
		return startYear;
	}


	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}


	public String getEndYear() {
		return endYear;
	}


	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}


	public boolean isLastEducation() {
		return lastEducation;
	}


	public void setLastEducation(boolean lastEducation) {
		this.lastEducation = lastEducation;
	}


	public long getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}


	public LocalDateTime getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}


	public long getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public LocalDateTime getModifiedOn() {
		return modifiedOn;
	}


	public void setModifiedOn(LocalDateTime modifiedOn) {
		this.modifiedOn = modifiedOn;
	}


	public long getDeletedBy() {
		return deletedBy;
	}


	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}


	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}


	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}


	public boolean isDelete() {
		return delete;
	}


	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
	

}
