package com.app.model;

import java.time.LocalDateTime;
import java.util.Date;

public class CustomerJoinModel {

	private String fullname;
	private Date dob;
	private String gender;
	private long bloodGroupId;
	private String rhesusType;
	private double height;
	private double weight;
	private long customerRelationId;
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public Date getDob() {
		return dob;
	}
	public void setDob(Date dob) {
		this.dob = dob;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public long getBloodGroupId() {
		return bloodGroupId;
	}
	public void setBloodGroupId(long bloodGroupId) {
		this.bloodGroupId = bloodGroupId;
	}
	public String getRhesusType() {
		return rhesusType;
	}
	public void setRhesusType(String rhesusType) {
		this.rhesusType = rhesusType;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public long getCustomerRelationId() {
		return customerRelationId;
	}
	public void setCustomerRelationId(long customerRelationId) {
		this.customerRelationId = customerRelationId;
	}
	
}
