package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tResetPassword")
public class ResetPasswordModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "oldPassword", length = 255)
	private String old_password;
	
	@Column(name = "ewPassword", length = 255)
	private String new_password;
	
	@Column(name = "resetFor", length = 20)
	private String reset_for;
	
	@Column(name = "createdBy")
	private long created_by;
	
	@Column(name = "createdOn")
	private LocalDateTime created_on;
	
	@Column(name = "modifiedBy")
	private long modified_by;
	
	@Column(name = "modifiedOn")
	private LocalDateTime modified_on;
	
	@Column(name = "deletedBy")
	private long deleteded_by;
	
	@Column(name = "deletedOn")
	private LocalDateTime deleted_on;
	
	@Column(name = "isDelete", columnDefinition = "boolean default false")
	private boolean is_delete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOld_password() {
		return old_password;
	}

	public void setOld_password(String old_password) {
		this.old_password = old_password;
	}

	public String getNew_password() {
		return new_password;
	}

	public void setNew_password(String new_password) {
		this.new_password = new_password;
	}

	public String getReset_for() {
		return reset_for;
	}

	public void setReset_for(String reset_for) {
		this.reset_for = reset_for;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public LocalDateTime getCreated_on() {
		return created_on;
	}

	public void setCreated_on(LocalDateTime created_on) {
		this.created_on = created_on;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public LocalDateTime getModified_on() {
		return modified_on;
	}

	public void setModified_on(LocalDateTime modified_on) {
		this.modified_on = modified_on;
	}

	public long getDeleteded_by() {
		return deleteded_by;
	}

	public void setDeleteded_by(long deleteded_by) {
		this.deleteded_by = deleteded_by;
	}

	public LocalDateTime getDeleted_on() {
		return deleted_on;
	}

	public void setDeleted_on(LocalDateTime deleted_on) {
		this.deleted_on = deleted_on;
	}

	public boolean isIs_delete() {
		return is_delete;
	}

	public void setIs_delete(boolean is_delete) {
		this.is_delete = is_delete;
	}
	
	
}
