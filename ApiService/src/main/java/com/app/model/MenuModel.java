package com.app.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "m_menu")
public class MenuModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="id")
	private long id;
	
	@Column(name ="name", length = 20)
	private String name;
	
	@Column(name ="url", length = 50)
	private String url;
	
	@Column(name ="parentId")
	private long parentid;
	
	@Column(name ="bigIcon", length = 100)
	private String bigicon;
	
	@Column(name ="smallIcon", length = 100)
	private String smallicon;
	
	@Column(name ="createBy")
	private long createby;
	
	@Column(name ="createOn")
	private LocalDateTime createon;
	
	@Column(name ="modifiedBy")
	private long modifiedby;
	
	@Column(name ="modifiedOn", columnDefinition ="timestamp default now()")
	private LocalDateTime modifiedon;
	
	@Column(name ="deleteBy")
	private long deleteby;
	
	@Column(name ="deleteOn", columnDefinition ="timestamp default now()")
	private LocalDateTime deleteon;
	
	@Column(name ="isDelete", nullable = false,columnDefinition = "boolean default false")
	private boolean isdelete;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getParentid() {
		return parentid;
	}

	public void setParentid(long parentid) {
		this.parentid = parentid;
	}

	public String getBigicon() {
		return bigicon;
	}

	public void setBigicon(String bigicon) {
		this.bigicon = bigicon;
	}

	public String getSmallicon() {
		return smallicon;
	}

	public void setSmallicon(String smallicon) {
		this.smallicon = smallicon;
	}

	public long getCreateby() {
		return createby;
	}

	public void setCreateby(long createby) {
		this.createby = createby;
	}

	public LocalDateTime getCreateon() {
		return createon;
	}

	public void setCreateon(LocalDateTime createon) {
		this.createon = createon;
	}

	public long getModifiedby() {
		return modifiedby;
	}

	public void setModifiedby(long modifiedby) {
		this.modifiedby = modifiedby;
	}

	public LocalDateTime getModifiedon() {
		return modifiedon;
	}

	public void setModifiedon(LocalDateTime modifiedon) {
		this.modifiedon = modifiedon;
	}

	public long getDeleteby() {
		return deleteby;
	}

	public void setDeleteby(long deleteby) {
		this.deleteby = deleteby;
	}

	public LocalDateTime getDeleteon() {
		return deleteon;
	}

	public void setDeleteon(LocalDateTime deleteon) {
		this.deleteon = deleteon;
	}

	public boolean isIsdelete() {
		return isdelete;
	}

	public void setIsdelete(boolean isdelete) {
		this.isdelete = isdelete;
	}
	
	
	

}
