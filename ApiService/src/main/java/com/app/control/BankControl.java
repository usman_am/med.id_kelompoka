package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.BankModel;
import com.app.repository.BankRepository;

@RestController
@RequestMapping("api/bank")
@CrossOrigin(origins = "*")
public class BankControl {

	@Autowired
	private BankRepository br;

	public int deleteValid = 0;

	@GetMapping("list")
	public List<BankModel> BankOrder() {
		return br.bankOrder();
	}

	@GetMapping("list/{id}")
	public BankModel bankById(@PathVariable long id) {
		return br.bankById(id);
	}

	@GetMapping("listbyName/{nm}")
	public List<BankModel> bankByNama(@PathVariable String nm) {
		return br.bankByNama(nm);
	}

	@GetMapping("listbyKodeName/{nm}/{va}")
	public List<BankModel> bankByNamaKode(@PathVariable String nm, @PathVariable String va) {
		return br.bankByNamaKode(nm, va);
	}

	@GetMapping("listbyKode/{va}")
	public List<BankModel> bankByKode(@PathVariable String va) {
		return br.bankByKode(va);
	}

	@PostMapping("add")
	public void addBank(@RequestBody BankModel bm) {
		LocalDateTime today = LocalDateTime.now();
		bm.setId(br.getlastid() + 1);
		bm.setCreated_on(today);
		bm.setCreated_by((long) 1);
		;
		bm.setModified_by(0);
		bm.setModified_on(today);
		bm.setDeleted_on(today);
		bm.setDeleted_by(0);
		bm.setIs_delete(false);
		br.save(bm);
	}

	@PutMapping("edit")
	public void editBank(@RequestBody BankModel bm) {
		br.save(bm);
	}

	@GetMapping("count/{nm}")
	public Long getCountid(@PathVariable String nm) {

		return br.getCountid(nm);
	}

	@DeleteMapping("delete")
	public void deleteBank(@RequestBody BankModel bm) {
		LocalDateTime today = LocalDateTime.now();

		if (deleteValid == 0) {
			bm.setDeleted_on(today);
			bm.setDeleted_by(1);

			br.deleteBank(bm.getId());
		} else {
			System.out.println("data masih dipakai");
		}
	}

}
