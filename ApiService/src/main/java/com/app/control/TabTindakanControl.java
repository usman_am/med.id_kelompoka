package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.BiodataAddressModel;
import com.app.model.DokterTreatmentModel;
import com.app.repository.TabTindakanRepository;

@RestController
@RequestMapping("api/tabtindakan")
@CrossOrigin(origins = "*")
public class TabTindakanControl {

	@Autowired
	private TabTindakanRepository ttr;
	
	@GetMapping("list/{id}")
	public List<Map<String, Object>> tindakanDokterById(@PathVariable long id) {
		return ttr.tindakanDokterById(id);
	}
	
	@GetMapping("id/{id}")
	public DokterTreatmentModel tindakan(@PathVariable long id) {
		return ttr.idtindakan(id);
	}
	
	@GetMapping("search/{id}/{nama}")
	public List<DokterTreatmentModel> search(@PathVariable long id,@PathVariable String nama) {
		return ttr.search(id,nama);
	}
	
	@PostMapping("add")
	public void addAlamat(@RequestBody DokterTreatmentModel dtm) {
		LocalDateTime today = LocalDateTime.now();
		dtm.setCreatedBy(dtm.getId());
		dtm.setCreatedOn(today);
		ttr.save(dtm);
	}
	
	@DeleteMapping("delete")
	public void deleteAlamat(@RequestBody DokterTreatmentModel dtm){
		LocalDateTime today = LocalDateTime.now();
		dtm.setDeletedBy(dtm.getId());
		dtm.setDeletedOn(today);
		ttr.deleteTindakan(dtm.getId());	
	}
	

}
