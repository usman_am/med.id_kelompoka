package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.BiodataModel;
import com.app.model.CustomerModel;
import com.app.model.DokterModel;
import com.app.model.JanjiModel;
import com.app.model.UserModel;
import com.app.repository.JanjiRepository;
import com.app.repository.TabProfilRepository;

@RestController
@RequestMapping("api/janji")
@CrossOrigin(origins = "*")
public class JanjiControl {

	
	@Autowired
	private JanjiRepository jr;
	
	
	@GetMapping("listbio")
	public List<BiodataModel> listBiodata(){
		return jr.listBiodataActive();
	}
	
	@GetMapping("listcustomer")
	public List<CustomerModel> listCustomer(){
		return jr.listCustomerActive();
	}
	
	@GetMapping("listdoctor")
	public List<DokterModel> listDokter(){
		return jr.listDokterActive();
	}
	
	@PostMapping("buatjanji")
	public void editProfil(@RequestBody BiodataModel bm,
			@RequestBody CustomerModel cm,
			@RequestBody DokterModel um) {
		LocalDateTime today = LocalDateTime.now();
		bm.setModifiedBy(bm.getId());
		bm.setModifiedOn(today);
	//	jr.save(bm);
	}
}
