package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.CustomerMemberModel;
import com.app.repository.CustomerMemberRepository;





@RestController
@RequestMapping("api/customermember")
@CrossOrigin("*")
public class CustomerMemberControl {

	@Autowired
	private CustomerMemberRepository cmr;
	
	@GetMapping("list")
	public List<CustomerMemberModel> listCustomerMember(){
		return cmr.listCustomerMember();
	}
	
	@GetMapping("list/{id}")
	public CustomerMemberModel customerMemberById(@PathVariable long id) {
		return cmr.customerMemberById(id);
	}
	
	@GetMapping("listbyname/{name}")
	public List<CustomerMemberModel> listCustomerMemberByName(@PathVariable String name){
		return cmr.listCustomerMemberByName(name);
	}
	
	@PostMapping("add")
	public void addCustomerMember(@RequestBody CustomerMemberModel cmm) {
		LocalDateTime today = LocalDateTime.now();
		cmm.setCreatedBy(1);
		cmm.setCreatedOn(today);
		cmr.save(cmm);
	}
	
	@PutMapping("edit")
	public void editCustomerMember(@RequestBody CustomerMemberModel cmm) {
		LocalDateTime today = LocalDateTime.now();
		cmm.setCreatedBy(1);
		cmm.setCreatedOn(today);
		cmr.save(cmm);
	}
	
	@DeleteMapping("delete")
	public void deleteCustomerMember(@RequestBody CustomerMemberModel cmm) {
		cmr.deleteCustomerMember(cmm.getId());
	}
	
}