package com.app.control;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.repository.TabProfilRepository;

@RestController
@RequestMapping("api/tabprofil")
@CrossOrigin(origins = "*")
public class TabProfilControl {

	@Autowired
	private TabProfilRepository tpr;
	
	@GetMapping("list/{id}")
	public List<Map<String, Object>> profil(@PathVariable long id) {
		return tpr.listprofil(id);
	}
}
