package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.MenuRoleModel;
import com.app.repository.MenuRoleRepository;


@RestController
@RequestMapping("api/menurole")
@CrossOrigin(origins = "*")
public class MenuRoleControl {
	@Autowired
	private MenuRoleRepository mrr;
	
	@GetMapping("list")
	public List<MenuRoleModel> listmenurole(){
		return mrr.listmenurole();
	}
	@PostMapping("add")
	public void addmenurole( @RequestBody MenuRoleModel mrm) {
		LocalDateTime today = LocalDateTime.now();
		mrm.setCreateon(today);
		mrm.setCreateby(1);
		mrr.save(mrm);
	}
	
	@GetMapping("list/{id}")
	public MenuRoleModel menuroleById(@PathVariable long id){
		return mrr.menurolebyid(id);
	}
	
	@PutMapping("update")
	public void updatemenurole(@RequestBody MenuRoleModel mrm) {
		LocalDateTime today = LocalDateTime.now();
		mrm.setModifiedon(today);
		mrm.setModifiedby(1);
		mrr.save(mrm);
	}
	
	@DeleteMapping("delete")
	public void deletemenurole(@RequestBody MenuRoleModel mrm){
		LocalDateTime today = LocalDateTime.now();
		mrm.setDeleteon(today);
		mrm.setModifiedby(1);
		mrr.deletemenurole(mrm.getId());
	}
}
