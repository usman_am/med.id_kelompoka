package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.ResetPasswordModel;
import com.app.repository.ResetPasswordRepository;

@RestController
@RequestMapping("api/resetpassword")
@CrossOrigin("*")
public class ResetPasswordControl {

	@Autowired
	private ResetPasswordRepository rpr;
	
	@PostMapping("post")
	public void post(@RequestBody ResetPasswordModel rpm) {
		rpm.setCreated_on(LocalDateTime.now());
		rpm.setCreated_by(0);
		rpr.save(rpm);
	}
	
	@GetMapping("list")
	public List<ResetPasswordModel> list(){
		return rpr.listResetPassword();
	}
}
