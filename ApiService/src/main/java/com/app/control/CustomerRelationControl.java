package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.CustomerRelationModel;
import com.app.repository.CustomerRelationRepository;



@RestController
@RequestMapping("api/hubunganpasien")
@CrossOrigin("*")
public class CustomerRelationControl {

	@Autowired
	private CustomerRelationRepository crr;
	
	@GetMapping("list")
	public List<CustomerRelationModel> listCustomerRelation(){
		return crr.listCustomerRelation();
	}
	
	@GetMapping("list/{id}")
	public CustomerRelationModel customerRelationById(@PathVariable long id) {
		return crr.customerRelationById(id);
	}
	
	@GetMapping("listbyname/{name}")
	public List<CustomerRelationModel> listCustomerRelationByName(@PathVariable String name){
		return crr.listCustomerRelationByName(name);
	}
	
	@PostMapping("add")
	public void addCustomerRelation(@RequestBody CustomerRelationModel crm) {
		LocalDateTime today = LocalDateTime.now();
		crm.setCreatedBy(1);
		crm.setCreatedOn(today);
		crr.save(crm);
	}
	
	@PutMapping("edit")
	public void editCustomerRelation(@RequestBody CustomerRelationModel crm) {
		LocalDateTime today = LocalDateTime.now();
		crm.setCreatedBy(1);
		crm.setCreatedOn(today);
		crr.save(crm);
	}
	
	@DeleteMapping("delete")
	public void deleteCustomerRelation(@RequestBody CustomerRelationModel crm) {
		crr.deleteCustomerRelation(crm.getId());
	}
	
}