package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.GolonganDarahModel;
import com.app.repository.GolonganDarahRepository;

@RestController
@RequestMapping("api/golongandarah")
@CrossOrigin("*")
public class GolonganDarahControl {

	@Autowired
	private GolonganDarahRepository gdr;
	
	@GetMapping("list")
	public List<GolonganDarahModel> listGolonganDarah(){
		return gdr.listGolonganDarah();
	}
	
	@GetMapping("list/{id}")
	public GolonganDarahModel golonganDarahById(@PathVariable long id) {
		return gdr.golonganDarahById(id);
	}
	
	@GetMapping("listbyname/{code}")
	public List<GolonganDarahModel> listGolonganDarahByName(@PathVariable String code){
		return gdr.listGolonganDarahByName(code);
	}
	
	@PostMapping("add")
	public void addGolongaDarah(@RequestBody GolonganDarahModel gdm) {
		LocalDateTime today = LocalDateTime.now();
		gdm.setCreatedBy(1);
		gdm.setCreatedOn(today);
		gdr.save(gdm);
	}
	
	@PutMapping("edit")
	public void editGolonganDarah(@RequestBody GolonganDarahModel gdm) {
		LocalDateTime today = LocalDateTime.now();
		gdm.setCreatedBy(1);
		gdm.setCreatedOn(today);
		gdr.save(gdm);
	}
	
	@DeleteMapping("delete")
	public void deleteGolongaDarah(@RequestBody GolonganDarahModel gdm) {
		gdr.deleteGolonganDarah(gdm.getId());
	}
	
}
