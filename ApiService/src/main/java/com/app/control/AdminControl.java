package com.app.control;

import java.time.LocalDateTime;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.AdminModel;
import com.app.repository.AdminRepository;
import com.app.repository.UserRepository;

@RestController
@RequestMapping("api/admin")
@CrossOrigin("*")
public class AdminControl {

	@Autowired
	private AdminRepository ar;
	
	@Autowired
	private UserRepository us;
	
	@PostMapping("post")
	public void post(@RequestBody Map<String, Object> am) {
		int id = (int) am.get("biodata_id");
		String email = (String) am.get("email");
		AdminModel amo = new AdminModel();
		amo.setBiodata_id(id);
		ar.save(amo);
		post2(email, id);
	}
	
	public void post2(String email,int biodata_id) {
		int id_update = us.selectIdbyEmail(email);
		ar.adminRegisterPart(id_update, biodata_id);
	}
}
