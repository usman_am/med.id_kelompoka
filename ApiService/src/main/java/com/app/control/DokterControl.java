package com.app.control;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.ApiServiceApplication;
import com.app.model.DokterModel;
import com.app.repository.DokterRepository;

@RestController
@RequestMapping("api/dokter")
@CrossOrigin(origins = "*")
public class DokterControl {

	@Autowired
	private DokterRepository dr;

	@GetMapping("namadokterbyid/{id}")
	public Map<String, Object> namaDokterById(@PathVariable long id) {
		return dr.namaDokterById(id);
	}
	
	@GetMapping("spesialisdokterbyid/{id}")
	public Map<String, Object> spesialisDokterById(@PathVariable long id) {
		return dr.spesialisDokterById(id);
	}
	
	@GetMapping("tindakandokterbyid/{id}")
	public List<Map<String, Object>> tindakanDokterById(@PathVariable long id) {
		return dr.tindakanDokterById(id);
	}
	
	@GetMapping("riwayatdokterbyid/{id}")
	public List<Map<String, Object>> riwayatDokterById(@PathVariable long id) {
		return dr.riwayatDokterById(id);
	}
	
	@GetMapping("pendidikandokterbyid/{id}")
	public List<Map<String, Object>> pendidikanDokterById(@PathVariable long id) {
		return dr.pendidikanDokterById(id);
	}
	
//	@GetMapping("tahundokterbyid/{id}")
//	public List<Map<String, Object>> tahunDokterById(@PathVariable long id) {
//		return dr.tahunDokterById(id);
//	}
	
	@GetMapping("spesialisofficebyid/{id}")
	public Map<String, Object> spesialisOfficeById(@PathVariable long id) {
		return dr.spesialisOfficeById(id);
	}
	@GetMapping("pengalamanbyid/{id}")
	public Map<String, Object> pengalamanById(@PathVariable long id) {
		return dr.pengalamanById(id);
	}
	
	@GetMapping("hargachatbyid/{id}")
	public Map<String, Object> hargaChatById(@PathVariable long id) {
		return dr.hargaChatById(id);
	}
	
	@GetMapping("hargakonsultasibyid/{id}")
	public List<Map<String, Object>> hargaKonsultasiById(@PathVariable long id) {
		return dr.hargaKonsultasiById(id);
	}

	@GetMapping("lokasipraktekbyid/{id}")
	public List<Map<String, Object>> lokasiPraktekById(@PathVariable long id) {
		return dr.lokasiPraktekById(id);
	}
	
	@GetMapping("hargakonsulbyid/{id}")
	public List<Map<String, Object>> hargaKonsul(@PathVariable long id) {
		return dr.hargaKonsul(id);
	}
	

}
