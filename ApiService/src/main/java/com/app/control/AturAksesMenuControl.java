package com.app.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.BiodataAddressModel;
import com.app.model.RoleModel;
import com.app.repository.AturAksesMenuRepository;

@RestController
@RequestMapping("api/aksesmenu")
@CrossOrigin(origins = "*")
public class AturAksesMenuControl {

	@Autowired
	private AturAksesMenuRepository aamr ;
	
	@GetMapping("list")
	public List<RoleModel> list(){
		return aamr.listrole();
	}
	
	@GetMapping("search/{a}")
	public List<RoleModel> search(@PathVariable String a){
		return aamr.search(a);
	}
}
