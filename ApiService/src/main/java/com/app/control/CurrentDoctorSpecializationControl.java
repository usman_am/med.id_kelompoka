package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.CurrentDoctorSpecializationModel;
import com.app.model.SpecializationModel;
import com.app.repository.CurrentDoctorSpecializationRepository;
import com.app.repository.SpecializationRepository;

@RestController
@RequestMapping("api/currentspesialis")
@CrossOrigin(origins = "*")
public class CurrentDoctorSpecializationControl {
	
	@Autowired
	private CurrentDoctorSpecializationRepository cdsr;
	
	@GetMapping("list")
	public List<Map<String, Object>> listCurrentSpecializationActive() {
		return cdsr.listCurrentSpecializationActive();
	}
	
	@GetMapping("list/{id}")
	public Map<String, Object> currentSpecializationById(@PathVariable long id) {
		return cdsr.currentSpecializationById(id);
	}
	
	@PostMapping("add")
	public void addcurrentSpesialis(@RequestBody CurrentDoctorSpecializationModel cdsm) {
		LocalDateTime today = LocalDateTime.now();
		cdsm.setCreatedOn(today);
		cdsm.setCreatedBy(1);
		cdsr.save(cdsm);
		
	}

	@PutMapping("edit")
	public void editSpesialis(@RequestBody CurrentDoctorSpecializationModel cdsm) {
		LocalDateTime today = LocalDateTime.now();
		cdsm.setModifiedOn(today);
		cdsm.setModifiedBy(1);
		cdsr.save(cdsm);
	}
	

}
