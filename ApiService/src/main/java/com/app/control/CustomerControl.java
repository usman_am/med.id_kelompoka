package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.BiodataModel;
import com.app.model.CustomerJoinModel;
import com.app.model.CustomerMemberModel;
import com.app.model.CustomerModel;
import com.app.model.PojoEditPasienModel;
import com.app.repository.BiodataRepository;
import com.app.repository.CustomerMemberRepository;
import com.app.repository.CustomerRepository;
import com.app.repository.UserRepository;


@RestController
@RequestMapping("api/customer")
@CrossOrigin("*")
public class CustomerControl {

	@Autowired
	private CustomerRepository cr;

	@Autowired
	private BiodataRepository br;
	
	@Autowired
	private CustomerMemberRepository cmr;
	
	@Autowired
	private UserRepository usr;
	
	@GetMapping("list")
	public List<CustomerModel> listCustomer(){
		return cr.listCustomer();
	}
	
	@GetMapping("listcs")
	public List<Map<String, Object>> listCS(){
		return cr.listCS();
	}

	@GetMapping("listcs/{id}")
	public List<Map<String, Object>> listCSById(@PathVariable long id){
		return cr.listCSById(id);
	}

	@GetMapping("list/{id}")
	public CustomerModel customerById(@PathVariable long id) {
		return cr.customerById(id);
	}
	
	@GetMapping("listbyname/{nama}")
	public List<CustomerModel> listCustomerByName(@PathVariable String nama){
		return cr.listCustomerByName(nama);
	}
	
	@PostMapping("add")
	public void addCustomer(@RequestBody Map<String, Object> cm) {
		int id = (int) cm.get("biodata_id");
		String email = (String) cm.get("email");
		CustomerModel com = new CustomerModel();
		com.setBiodataId(id);
		cr.save(com);
		addCustomer2(email, id);
	}
	
	public void addCustomer2(String email, int biodata_id) {
		int id_update = usr.selectIdbyEmail(email);
		cr.customerRegisterPart(id_update, biodata_id);
	}

	@PostMapping("addcs")
	public void addCs(@RequestBody CustomerJoinModel cjm) {
		//save fullname
		BiodataModel bm = new BiodataModel();
		bm.setFullname(cjm.getFullname());
		br.save(bm);

		//save customer
		CustomerModel cm = new CustomerModel();
		cm.setBiodataId(bm.getId());
		cm.setDob(cjm.getDob());
		cm.setGender(cjm.getGender());
		cm.setBloodGroupId(cjm.getBloodGroupId());
		cm.setRhesusType(cjm.getRhesusType());
		cm.setHeight(cjm.getHeight());
		cm.setWeight(cjm.getWeight());
		cr.save(cm);
		
		//save customer member model
		CustomerMemberModel cmm = new CustomerMemberModel();
		cmm.setParentBiodataId(bm.getId());
		cmm.setCustomerId(cm.getId());
		cmm.setCustomerRelationId(cjm.getCustomerRelationId());
		cmr.save(cmm);
	}
	
	@PutMapping("edit")
	public void editCustomer(@RequestBody CustomerModel cm) {
		LocalDateTime today = LocalDateTime.now();
		cm.setCreatedBy(1);
		cm.setCreatedOn(today);
		cr.save(cm);
	}

	@PutMapping("editcs")
    	public void editcs(@RequestBody PojoEditPasienModel pepm) {

        // update biodata
        BiodataModel bm = br.findById(pepm.getBiodata_id()).get();
        bm.setFullname(pepm.getFullname());
        br.save(bm);

        //
        // save customer
        CustomerModel cm = cr.findById(pepm.getCustomer_id()).get();
        cm.setDob(pepm.getDob());
        cm.setGender(pepm.getGender()); // gender
        cm.setBloodGroupId(pepm.getBlood_group_id()); // goldar
        cm.setRhesusType(pepm.getRhesus_type()); // rhesus darah
        cm.setHeight(pepm.getHeight()); // tinggi
        cm.setWeight(pepm.getWeight()); // berat
        cr.save(cm);

        // save customer member model
        CustomerMemberModel cmm = cmr.findById(pepm.getCustomer_member_id()).get();
        cmm.setParentBiodataId(bm.getId());
        cmm.setCustomerId(cm.getId());
        cmm.setCustomerRelationId(pepm.getCustomer_relation_id());
        cmr.save(cmm);
    	}
	
	@DeleteMapping("delete")
	public void deleteCustomer(@RequestBody CustomerModel cm) {
		cr.deleteCustomer(cm.getId());
	}
	
	@DeleteMapping("deletecs")
	public void deleteCustomer(@RequestBody PojoEditPasienModel pepm) {
		cr.deleteCustomer(pepm.getCustomer_id());
		cr.deleteCustomerMember(pepm.getCustomer_member_id());
		cr.deleteBiodata(pepm.getBiodata_id());
	}
	
	@DeleteMapping("deletemulti")
	public String delete(@RequestBody List<Integer> ids) {
        //System.out.println("deleting");
        cr.deleteCs(ids);
        return String.join(",", ids.stream().map(value ->  Integer.toString(value)).collect(Collectors.toList()));
    	}
}
