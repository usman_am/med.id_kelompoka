package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.ResetPasswordModel;
import com.app.model.UserModel;

import com.app.repository.BiodataRepository;
import com.app.repository.ResetPasswordRepository;

import com.app.repository.UserRepository;

@RestController
@RequestMapping("api/user")
@CrossOrigin("*")
public class UserControl {

	@Autowired
	private UserRepository us;
	
	@Autowired
	private ResetPasswordRepository rpr;
	
	@Autowired
	private BiodataRepository brs;
	
	@GetMapping("list")
	public List<Map<String, Object>> list(){
		return us.listUser();
	}
	
	@PostMapping("post")
	public void post(@RequestBody UserModel um) {
		um.setIs_locked(false);
		um.setLast_login(LocalDateTime.now());
		um.setCreated_on(LocalDateTime.now());
		um.setCreated_by(0);
		us.save(um);
		long nil = us.selectById();
		us.setDefaultCreatedBy(nil);
		brs.biodataRegisterPart(nil, um.getBiodata_id());
	}
	
	@GetMapping("cekemail/{email}")
	public Map<String, Object> cekEmail(@PathVariable String email){
		return us.cekEmail(email);
	}
	
	@GetMapping("cekpassword/{email}/{password}")
	public int cekPassword(@PathVariable String email, @PathVariable String password){
		int hasilcekpswd =  us.cekPassword(email, password);
		if(hasilcekpswd==0) { loginattempt(email);}
		else {us.loginAttemptreset(email);}
		return us.cekPassword(email, password);
	}
	
	@GetMapping("cekpassword2/{email}/{password}")
	public int cekPassword2(@PathVariable String email, @PathVariable String password){
		return us.cekPassword(email, password);
	}
	
	public void loginattempt(String email) {
		us.loginAttempt(email);
		us.updateAttempt();
	}
	
//	@GetMapping("validpasword/{email}/{password}")
//	public int getvalid(@PathVariable String email, @PathVariable String password){
//		return us.selectIdbyEmail(email);
//	}
	
	@GetMapping("getidbyemail/{email}")
	public int getIdbyEmail(@PathVariable String email){
		return us.selectIdbyEmail(email);
	}
	
	@PutMapping("update")
	public void update(@RequestBody UserModel um) {
		String oldpass = us.getPassword(um.getEmail());
		Map<String, Object> untuk = us.cekEmail(um.getEmail());
		boolean isitlocked = (boolean) untuk.get("is_locked");
		savepassword(oldpass, um.getPassword(), isitlocked, um.getId());
		us.updatePassword(um.getPassword(),um.getModified_by(), um.getId(), um.getEmail());
	}
	
	public void savepassword(String old_password,String new_password, boolean isitlocked, Long id) {
		String reset_for = "";
		if(isitlocked==false) {
			reset_for = "forgot password";
		}else if(isitlocked==true) {
			reset_for = "unlocked account";
		}
		ResetPasswordModel map = new ResetPasswordModel();
		map.setOld_password(old_password);
		map.setNew_password(new_password);
		map.setReset_for(reset_for);
		map.setCreated_by(id);
		map.setCreated_on(LocalDateTime.now());
		rpr.save(map);
	}
	
	@GetMapping("getrole/{email}")
	public int getrole(@PathVariable String email){
		return us.getRole2(email);

	}
	
	@GetMapping("getfullname/{email}")
	public String getFullname(@PathVariable String email) {
		int idya = us.getBiodataId(email);
		return brs.getNameBiodata(idya);
	}
	
}
