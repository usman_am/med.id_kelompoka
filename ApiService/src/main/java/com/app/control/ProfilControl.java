package com.app.control;

import java.util.stream.Stream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.model.BiodataModel;
import com.app.repository.ProfilRepository;

@RestController
@RequestMapping("api/profil")
@CrossOrigin(origins = "*")
public class ProfilControl {

	@Autowired
	private ProfilRepository pr;

	@GetMapping("list/{id}")
	public BiodataModel profil(@PathVariable long id) {
		return pr.listprofil(id);
	}

	public int store(MultipartFile file, long id) throws IOException {
		return pr.updateGbr(id, file.getBytes());
	}

	public BiodataModel getFile(long id) {
		return pr.findById(id).get();
	}

	public Stream<BiodataModel> getAllFiles() {
		return pr.findAll().stream();
	}

	@PostMapping("uploadgbr")
	public void uploadGbr(@RequestParam("uploadFile") MultipartFile uploadfile, @RequestParam("id") long id)
			throws IllegalStateException, IOException {
		try {
			store(uploadfile, id);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@GetMapping("/files/{id}")
	public ResponseEntity<byte[]> getFileGbr(@PathVariable long id) {
		BiodataModel fileDB = getFile(id);

		return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(fileDB.getImage());
	}
}
