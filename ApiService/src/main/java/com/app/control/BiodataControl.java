package com.app.control;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.app.model.BiodataModel;
import com.app.repository.BiodataRepository;

@RestController
@RequestMapping("api/boidata")
@CrossOrigin("*")
public class BiodataControl {
	
	@Autowired
	private BiodataRepository br;
	
	@GetMapping("list")
	public List<Map<String, Object>> list(){
		return br.listBiodata();
	}
	
	@PostMapping("post")
	public void post(@RequestBody BiodataModel bm) {
		bm.setCreatedOn(LocalDateTime.now());
		bm.setCreatedBy(0);
		br.save(bm);
	}
	
	@GetMapping("newid")
	public int newid(){
		return br.selectById();
	}

	public int store(MultipartFile file,long id) throws IOException {
		    return br.updateGbr(id, file.getBytes());
	 }

	 public BiodataModel getFile(long id) {
		    return br.findById(id).get();
	 }

	  public Stream<BiodataModel> getAllFiles() {
	    return br.findAll().stream();
	  }

	@PostMapping("uploadgbr")
	 public void uploadGbr(@RequestParam("uploadFile") MultipartFile uploadfile,
			 				@RequestParam("id") long id) throws IllegalStateException, IOException { 
		 try {
			
			 store(uploadfile,id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		 
	 }

	@GetMapping("/files/{id}")
	 public ResponseEntity<byte[]> getFileGbr(@PathVariable long id) {
		 BiodataModel fileDB = getFile(id);

	    return ResponseEntity.ok()
	    	.contentType(MediaType.IMAGE_JPEG)
	        .body(fileDB.getImage());
	  }

	

}
