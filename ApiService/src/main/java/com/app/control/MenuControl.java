package com.app.control;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.MenuModel;
import com.app.repository.MenuRepository;


@RestController
@RequestMapping("api/menu")
@CrossOrigin(origins = "*")
public class MenuControl {
	@Autowired
	private MenuRepository mr;
	
	@GetMapping("list")
	public List<MenuModel> listmenu(){
		return mr.listmenu();
	}
	@PostMapping("add")
	public void addmenu( @RequestBody MenuModel mm) {
		LocalDateTime today = LocalDateTime.now();
		mm.setCreateon(today);
		mm.setCreateby(1);
		mr.save(mm);
	}
	
	@GetMapping("list/{id}")
	public MenuModel menuById(@PathVariable long id){
		return mr.menubyid(id);
	}
	
	@PutMapping("update")
	public void menuuser(@RequestBody MenuModel um) {
		LocalDateTime today = LocalDateTime.now();
		um.setModifiedon(today);
		um.setModifiedby(1);
		mr.save(um);
	}
	
	@DeleteMapping("delete")
	public void deleteuser(@RequestBody MenuModel mm){
		LocalDateTime today = LocalDateTime.now();
		mm.setDeleteon(today);
		mm.setModifiedby(1);
		mr.deletemenu(mm.getId());
	}

}
