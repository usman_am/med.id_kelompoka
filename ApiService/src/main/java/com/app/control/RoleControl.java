package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.RoleModel;
import com.app.repository.RoleRepository;
import com.app.repository.UserRepository;

//import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@RequestMapping("api/role")
@CrossOrigin("*")
public class RoleControl {

	@Autowired
	private RoleRepository rr;
	
	@Autowired
	private UserRepository usrep;
	
	@GetMapping("list")
	public List<Map<String, Object>>roleorder(){
		return rr.roleorder();
	}
	
	@GetMapping("lis2")
	public List<Map<String, Object>>list2(){
		return rr.listRole();
	}
	@PostMapping("add")
	public void addrole(@RequestBody RoleModel rm) {
		
		LocalDateTime today = LocalDateTime.now();
		rm.setCreated_on(today);
		rm.setCreated_by(1);
		rr.save(rm);
	}

	@GetMapping("list/{id}")
	public RoleModel roleById(@PathVariable long id){
		return rr.rolebyid(id);
	}

	@PutMapping("update")
	public void updaterole(@RequestBody RoleModel rm) {
		LocalDateTime today = LocalDateTime.now();
		rm.setModified_on(today);
		rm.setModified_by(1);
		rr.save(rm);
	}

//	@DeleteMapping("delete")
//	public void deleterole(@RequestBody RoleModel rm){
//		LocalDateTime today = LocalDateTime.now();
//		rm.setDeleted_on(today);
//		rm.setModified_by(1);
//		rr.deleterole(rm.getId());
//	}
//	
	@GetMapping("listadmin")
	public List<Map<String, Object>> listadmin(){
		return rr.listRoleAdmin();
	}
	
	@GetMapping("listadminbyname/{name}")
	public List<Map<String, Object>>listAdminByName(@PathVariable String name){
		return rr.listModelByNama(name);
	}
	
	@PostMapping("post")
	public void post(@RequestBody RoleModel rm) {
		rm.setCreated_on(LocalDateTime.now());
		rr.save(rm);
	}
	
	@GetMapping("rolebyid/{id}")
	public Map<String, Object> listRoleById(@PathVariable int id){
		return rr.roleById(id);
	}
	
	@PutMapping("edit")
	public void editRole(@RequestBody Map<String, Object> rm) {
		String id = (String) rm.get("id");
		int idcons = Integer.valueOf(id);
		String name = (String) rm.get("name");
		String code = (String) rm.get("code");
		String email = (String) rm.get("update_by");
		int update = (int) usrep.selectIdbyEmail(email);
		rr.updateRole(name, code, update, idcons);
	}
	
	@DeleteMapping("delete")
	public void deleteRole(@RequestBody Map<String, Object> rm) {
		String id = (String) rm.get("id");
		int idcons = Integer.valueOf(id);
		String email = (String) rm.get("update_by");
		int update = (int) usrep.selectIdbyEmail(email);
		rr.deleteRole(update, idcons);
	}
	
	@GetMapping("rolename/{name}")
	public int roleName(@PathVariable String name){
		return rr.roleNameCek(name);
	}
	
	@GetMapping("rolecode/{code}")
	public int roleCode(@PathVariable String code){
		return rr.roleCodeCek(code);
	}
}