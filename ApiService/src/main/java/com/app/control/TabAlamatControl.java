package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.BiodataAddressModel;
import com.app.repository.TabAlamatRepository;

@RestController
@RequestMapping("api/tabalamat")
@CrossOrigin(origins = "*")
public class TabAlamatControl {

	@Autowired
	private TabAlamatRepository tar;
	
	@GetMapping("listalamat/{biodataid}")
	public List<BiodataAddressModel> listAlamat(@PathVariable long biodataid){
		return tar.listAddressActive(biodataid);
	}
	
	@GetMapping("listLasc")
	public List<BiodataAddressModel> listAlamat1(){
		return tar.listLabelAsc();
	}
	@GetMapping("listLdesc")
	public List<BiodataAddressModel> listAlamat2(){
		return tar.listLabelDesc();
	}
	@GetMapping("listRasc")
	public List<BiodataAddressModel> listAlamat3(){
		return tar.listRecipAsc();
	}
	@GetMapping("listRdesc")
	public List<BiodataAddressModel> listAlamat4(){
		return tar.listRecipDesc();
	}
	
	@GetMapping("list/{id}")
	public BiodataAddressModel listId(@PathVariable long id){
		return tar.listId(id);
	}
	
	@GetMapping("sort/{xx}/{yy}")
	public List<Map<String, Object>> sortAlamat(@PathVariable String xx, @PathVariable String yy){
		return tar.sortAddress(xx,yy);
	}
	
	@GetMapping("search/{a}")
	public List<BiodataAddressModel> search(@PathVariable String a){
		return tar.search(a);
	}
	
	@GetMapping("search/Lasc/{bid}/{a}")
	public List<BiodataAddressModel> search1(@PathVariable long bid, @PathVariable String a){
		return tar.searchLabelAsc(bid,a);
	}
	@GetMapping("search/Ldesc/{bid}/{a}")
	public List<BiodataAddressModel> search2(@PathVariable long bid, @PathVariable String a){
		return tar.searchLabelDesc(bid,a);
	}
	@GetMapping("search/Rasc/{bid}/{a}")
	public List<BiodataAddressModel> search3(@PathVariable long bid, @PathVariable String a){
		return tar.searchRecipAsc(bid,a);
	}
	@GetMapping("search/Rdesc/{bid}/{a}")
	public List<BiodataAddressModel> search4(@PathVariable long bid, @PathVariable String a){
		return tar.searchRecipDesc(bid,a);
	}
	
	@GetMapping("search/valid/{id}/{a}")
	public List<BiodataAddressModel> sValid(@PathVariable String a,
											@PathVariable long id){
		return tar.searchValid(a,id);
	}
	
	@GetMapping("listkotakec")
    public List<Map<String, Object>> lokasilist() {
        return tar.listlokasi();
    }
	
	@PostMapping("add")
	public void addAlamat(@RequestBody BiodataAddressModel mbam) {
		LocalDateTime today = LocalDateTime.now();
		mbam.setCreatedBy(mbam.getId());
		mbam.setCreatedOn(today);
		tar.save(mbam);
	}
	
	@PutMapping("edit")
	public void editAlamat(@RequestBody BiodataAddressModel mbam) {
		LocalDateTime today = LocalDateTime.now();
		mbam.setModifiedBy(mbam.getId());
		mbam.setModifiedOn(today);
		tar.save(mbam);
	}
	
	@DeleteMapping("delete")
	public void deleteAlamat(@RequestBody BiodataAddressModel mbam){
		LocalDateTime today = LocalDateTime.now();
		mbam.setDeletedBy(mbam.getId());
		mbam.setDeletedOn(today);
		tar.deleteAlamat(mbam.getId());	
	}
	
}
