package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.model.SpecializationModel;
import com.app.repository.SpecializationRepository;

@RestController
@RequestMapping("api/specialization")
@CrossOrigin(origins = "*")
public class SpecializationControl {
	
	@Autowired	
	private SpecializationRepository sr;
	
	@GetMapping("nama")
	public String nama() {
		return "dhila";
	}
	
	@GetMapping("list")
	public List<SpecializationModel> listSpecialization(){
		return sr.listSpecialization();
	}
	
	@GetMapping("list/{id}")
	public SpecializationModel listSpecializationById(@PathVariable long id) {
		return sr.SpecializationById(id);
	}
	
	@GetMapping("listbyname/{name}")
	public List<SpecializationModel> listSpecializationByName(@PathVariable String name){
		return sr.listSpecializationByName(name);
	}
	
	@PostMapping("add")
	public void addSpecialization(@RequestBody SpecializationModel sm) {
		LocalDateTime today = LocalDateTime.now();
		sm.setCreatedBy(1);
		sm.setCreatedOn(today);
		sr.save(sm);
	}
	
	@PutMapping("edit")
	public void editSpecialization(@RequestBody SpecializationModel sm) {
		LocalDateTime today = LocalDateTime.now();
		sm.setCreatedBy(1);
		sm.setCreatedOn(today);
		sr.save(sm);
	}
	
	@DeleteMapping("delete")
	public void deleteSpecialization(@RequestBody SpecializationModel sm) {
		sr.deleteSpecialization(sm.getId());
	}

}
