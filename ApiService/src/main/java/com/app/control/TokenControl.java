package com.app.control;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.config.MailConfig;
import com.app.model.PesanModel;
import com.app.model.TokenModel;
import com.app.repository.TokenRepository;

@RestController
@RequestMapping("api/token")
@CrossOrigin("*")
@Component
public class TokenControl {

	@Autowired
	private TokenRepository tr;
	
	@Autowired
	private MailConfig mc;
	
	//place holder
	@GetMapping("list")
	public List<Map<String, Object>> list(){
		return tr.listToken();
	}
	
	//buat token by email
	@PostMapping("post")
	public void post(@RequestBody TokenModel tm) {
		Random rand = new Random();
		int token = rand.nextInt(1000);
		tm.setToken(String.valueOf(token));
		tm.setExpired_on(LocalDateTime.now().plusMinutes(10));
		tm.setIs_expired(false);
		tm.setCreated_by(0);
		tm.setCreated_on(LocalDateTime.now());
		tr.save(tm);
	}
	
	//ambil id yang paling baru dibuat berdasarkan email
	@GetMapping("gettoken/{email}")
	public int getToken(@PathVariable String email){
		String token = tr.sendToken(email);
		sendToken(email, token);
		return tr.selectToken(email);
	}
	//kirim email
	public void sendToken(@PathVariable String email, @PathVariable String token){
		mc.kirim(email, "Verifikasi token", "token anda adalah : "+token);
	}
	
	//cek expired token
	@GetMapping("cekvalid/{id}")
	public String cekValid(@PathVariable int id){
		return tr.cekValid(id);
	}
	
	//cek valid token
	@GetMapping("cektoken/{id}/{token}")
	public int cekToken(@PathVariable int id ,@PathVariable String token){
		return tr.cekToken(id,token);
	}
	
	//update filed expired setiap detik
	@Scheduled(fixedDelay = 1000)
	@PutMapping("updatetoken")
	public void updateToken() {
		tr.updateExpired();
	}
	
	@PutMapping("setexpired/{email}")
	public void spoilToken(@PathVariable String email) {
		tr.setExpired(email);
	}
}
