package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataModel;

@Transactional
public interface BiodataRepository extends JpaRepository<BiodataModel, Long>{

	 @Modifying
	 @Query(value = "update m_biodata set image=:gbr where id=:id ",nativeQuery =
	  true) int updateGbr(long id, byte[] gbr);
	 
	 @Query(value = "select id, fullname, mobile_phone, image, image_path"
				+ " from m_biodata where is_delete=false", nativeQuery = true)
		List<Map<String, Object>>listBiodata();
		
		@Query(value = "select id from m_biodata where is_delete=false order by id desc limit 1", nativeQuery = true)
		int selectById();
		
		@Query(value = "select * from m_biodata where id=:id limit 1", nativeQuery = true)
		BiodataModel biodataById(long id);
		
		@Query(value = "select fullname from m_biodata where id=:id limit 1", nativeQuery = true)
		String getNameBiodata(long id);
		
		@Query(value = "select * from m_biodata where lower(name) like lower(concat('%',:nm,'%')) and is_delete=false order by id", nativeQuery = true)
		List<BiodataModel> listBiodataByName(String nm);
		
		@Modifying
		@Query(value = "update m_biodata set is_delete=true where id=:id", nativeQuery = true)
		int deleteBiodata(long id);
		
		@Modifying
		@Query(value = "update m_biodata set created_by= :user_id, created_on=now() where id= :id", nativeQuery = true)
		void biodataRegisterPart(long user_id, long id);
}
