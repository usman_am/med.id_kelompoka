package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.ResetPasswordModel;

public interface ResetPasswordRepository extends JpaRepository<ResetPasswordModel, Long>{

	@Query(value = "select * from t_reset_password where is_delete=false order by id", nativeQuery = true)
	List<ResetPasswordModel> listResetPassword();
}
