package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataModel;
import com.app.model.DokterModel;
import com.app.model.DokterTreatmentModel;

@Transactional
public interface TabTindakanRepository extends JpaRepository<DokterTreatmentModel, Long> {
	
	@Query(value = "select dt.name as name, dt.id as id, dt.doctor_id as doctorid from (select * from t_doctor_treatment where is_delete=false) as dt join m_doctor d on d.id=dt.doctor_id where d.id= :id",nativeQuery = true)
	List<Map<String, Object>> tindakanDokterById(long id);
	
	@Modifying
	@Query(value = "update t_doctor_treatment set is_delete=true where id=:id", nativeQuery = true)
	int deleteTindakan(long id);
	
	@Query(value = "select * from (select * from t_doctor_treatment where is_delete=false) as tdt where doctor_id=:id and lower(name) like :tindakan",nativeQuery = true)
	List<DokterTreatmentModel> search(long id,String tindakan);
	
	@Query(value = "select * from t_doctor_treatment where is_delete=false and id= :id limit 1",nativeQuery = true)
	DokterTreatmentModel idtindakan(long id);
	
}
