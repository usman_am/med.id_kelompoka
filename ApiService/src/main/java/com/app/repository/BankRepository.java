package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BankModel;

@Transactional
public interface BankRepository extends JpaRepository<BankModel, Long> {

	@Query(value="select * from m_bank where va_code is not null order by id",nativeQuery = true)
	List<BankModel>bankOrder();
	
	@Query(value = "select * from m_bank where id= :id limit 1",nativeQuery = true)
	BankModel bankById(long id);
	
	@Query(value = "select * from m_bank where lower(name) like lower(concat('%',:nm,'%')) and va_code is not null order by id",nativeQuery = true) 
	List<BankModel> bankByNama(String nm);
	
	@Query(value = "select * from m_bank where lower(va_code) like lower(concat('%',:va,'%')) or lower(name) like lower(concat('%', :nm ,'%')) and is_delete = 'false' order by id",nativeQuery = true)
	List<BankModel> bankByNamaKode(String nm,String va);
	
	@Query(value = "select * from m_bank where lower(va_code) like lower(concat('%',:va,'%')) and is_delete = 'false' order by id",nativeQuery = true)
	List<BankModel> bankByKode(String va);
	
	@Modifying
	@Query(value = "update m_bank set va_code=null where id= :id  ",nativeQuery = true)
	int deleteBank(long id);
	
	@Query(value = "select max(id) from m_bank",nativeQuery = true)
	Long getlastid();
	
	@Query(value = "select count(id) from m_bank where name= :nm and is_delete = false",nativeQuery = true)
	Long getCountid(String nm);
	
}
