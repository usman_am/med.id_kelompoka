package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.GolonganDarahModel;

@Transactional
public interface GolonganDarahRepository extends JpaRepository<GolonganDarahModel , Long> {

	@Query(value = "select * from m_blood_group where is_delete=false order by id", nativeQuery = true)
	List<GolonganDarahModel> listGolonganDarah();
	
	@Query(value = "select * from m_blood_group where id=:id limit 1", nativeQuery = true)
	GolonganDarahModel golonganDarahById(long id);
	
	@Query(value = "select * from m_blood_group where lower(code) like lower(concat('%',:nm,'%')) and is_delete=false order by id", nativeQuery = true)
	List<GolonganDarahModel> listGolonganDarahByName(String nm);
	
	@Modifying
	@Query(value = "update m_blood_group set is_delete=true where id=:id", nativeQuery = true)
	int deleteGolonganDarah(long id);
	
}
