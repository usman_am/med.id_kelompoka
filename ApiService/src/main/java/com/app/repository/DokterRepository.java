package com.app.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataModel;
import com.app.model.DokterModel;

public interface DokterRepository extends JpaRepository<DokterModel, Long> {

	@Query(value = "select b.fullname from m_doctor d join m_biodata b on b.id=d.biodata_id where d.id= :id limit 1",nativeQuery =true) 
	Map<String, Object> namaDokterById(long id);

	@Query(value = "select s.name from m_specialization s join t_current_doctor_specialization cdt "
			+ "on s.id=cdt.specialization_id join m_doctor d on d.id=cdt.doctor_id where d.id= :id limit 1",nativeQuery = true)
	Map<String, Object> spesialisDokterById(long id);
	
	@Query(value = "select dt.name from  t_doctor_treatment dt join m_doctor d "
			+ "on d.id=dt.doctor_id where d.id= :id",nativeQuery = true)
	List<Map<String, Object>> tindakanDokterById(long id);
	
	@Query(value = "select mf.name as namars,l.name as namakota,dof.specialization,dof.is_delete,date_part('year',dof.created_on) as created,date_part('year',dof.deleted_on) as deleted from m_location l join m_medical_facility mf "
			+ "on  mf.location_id=l.id join t_doctor_office dof on dof.medical_facility_id=mf.id "
			+ "join m_doctor d on d.id=dof.doctor_id where d.id= :id ",nativeQuery = true)
	List<Map<String, Object>> riwayatDokterById(long id);
	
	@Query(value = "select de.institution_name,de.major,de.end_year from m_doctor_education de join m_doctor d "
			+ "on de.doctor_id=d.id where d.id= :id",nativeQuery = true)
	List<Map<String, Object>> pendidikanDokterById(long id);
	
//	@Query(value = "select dof.is_delete,date_part('year',dof.created_on) as created,date_part('year',dof.deleted_on) as deleted from t_doctor_office dof "
//			+ "join m_doctor d on dof.doctor_id=d.id where d.id= :id",nativeQuery = true)
//	List<Map<String, Object>> tahunDokterById(long id);
	
	@Query(value = "select dof.specialization,dof.is_delete from t_doctor_office dof join m_doctor d on d.id=dof.doctor_id where d.id= :id and dof.is_delete=false",nativeQuery = true)
	Map<String, Object> spesialisOfficeById(long id);
	
	@Query(value = "select DATE_PART('YEAR', AGE(CASE WHEN(SELECT count(id) FROM t_doctor_office dof WHERE dof.deleted_on IS NULL) > 0 THEN NOW() "
			+ "ELSE max(dof.deleted_on) "
			+ "END,min(dof.created_on))) from t_doctor_office dof join m_doctor d on dof.doctor_id=d.id where d.id= :id",nativeQuery = true)
	Map<String, Object> pengalamanById(long id);
	
	@Query(value = "select ccn.nominal from t_customer_custom_nominal ccn join t_customer_chat cch "
			+ "on ccn.customer_id=cch.customer_id join m_doctor d on d.id=cch.doctor_id where d.id= :id",nativeQuery = true)
	Map<String, Object> hargaChatById(long id);
	
	@Query(value = "select dotp.price_start_from from t_doctor_office_treatment_price dotp join t_doctor_office_treatment dot "
			+ "on dotp.doctor_office_treatment_id=dot.id join t_doctor_office dof on dot.doctor_office_id=dof.id join m_doctor d "
			+ "on dof.doctor_id=d.id where d.id= :id",nativeQuery = true)
	List<Map<String, Object>> hargaKonsultasiById(long id);
	
	@Query(value = "select mf.id mfid, mfs.day,mfs.time_schedule_start,mfs.time_schedule_end,mf.name as namars,mfc.name as kategori,mf.full_address,ll.name as name,ll.abbreviation from m_medical_facility_category mfc "
			+ "join m_medical_facility mf on mf.medical_facility_category_id=mfc.id join m_location l on l.id=mf.location_id "
			+ "join m_location_level ll on ll.id=l.location_level_id join t_doctor_office dof on dof.medical_facility_id=mf.id "
			+ "join m_medical_facility_schedule mfs on mfs.medical_facility_id=mf.id "
			+ "join t_doctor_office_schedule dos on mfs.id=dos.medical_facility_schedule_id "
			+ "join m_doctor d on d.id=dof.doctor_id where d.id= :id",nativeQuery = true)
	List<Map<String, Object>> lokasiPraktekById(long id);

//	@Query(value = "select mf.name as namars,mfc.name as kategori,mf.full_address,ll.name as name,ll.abbreviation from m_medical_facility_category mfc "
//			+ "join m_medical_facility mf on mf.medical_facility_category_id=mfc.id join m_location l on l.id=mf.location_id "
//			+ "join m_location_level ll on ll.id=l.location_level_id join t_doctor_office dof on dof.medical_facility_id=mf.id "
//			+ "join m_doctor d on d.id=dof.doctor_id where d.id= :id",nativeQuery = true)
//	List<Map<String, Object>> lokasiPraktekById(long id);
	
	@Query(value = "select mf.id as mfid, mfs.day,mfs.time_schedule_start,mfs.time_schedule_end,mf.name as namars,mfc.name as kategori,mf.full_address,ll.name as name,ll.abbreviation,dotp.price_start_from from m_medical_facility_category mfc "
			+ "join m_medical_facility mf on mf.medical_facility_category_id=mfc.id "
			+ "join m_location l on l.id=mf.location_id "
			+ "join m_location_level ll on ll.id=l.location_level_id "
			+ "join t_doctor_office dof on dof.medical_facility_id=mf.id "
			+ "join t_doctor_office_treatment dot on dot.doctor_treatment_id=dof.id "
			+ "join t_doctor_office_treatment_price dotp on dotp.doctor_office_treatment_id=dotp.id "
			+ "join m_medical_facility_schedule mfs on mfs.medical_facility_id=mf.id "
			+ "join t_doctor_office_schedule dos on mfs.id=dos.medical_facility_schedule_id "
			+ "join m_doctor d on d.id=dof.doctor_id where d.id= :id",nativeQuery = true)
	List<Map<String, Object>> hargaKonsul(long id);

	 

	
	
	
}
