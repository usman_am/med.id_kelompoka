package com.app.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.AdminModel;

@Transactional
public interface AdminRepository extends JpaRepository<AdminModel, Long>{

	@Modifying
	@Query(value = "update m_admin set created_by= :id, created_on=now() where biodata_id= :biodata_id", nativeQuery = true)
	void adminRegisterPart(long id, long biodata_id);
}
