package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.RoleModel;

@Transactional
public interface RoleRepository extends JpaRepository<RoleModel, Long>{

	@Query(value = "select id, name, code from m_role where is_delete=false and name!='Admin'", nativeQuery = true)
	List<Map<String, Object>>listRole();
	
	@Query(value = "select id, name, code from m_role where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>listRoleAdmin();
	
	@Query(value = "select id, name, code from m_role where lower(name) like lower(concat('%',:name,'%')) and is_delete=false",nativeQuery = true)
	List<Map<String, Object>>listModelByNama(String name);
	
	@Query(value = "select id, name, code from m_role where id= :id and is_delete=false", nativeQuery = true)
	Map<String, Object> roleById(long id);
	
	@Modifying
	@Query(value = "update m_role set name= :name, code= :code, modified_by= :modified_by, modified_on=now() where id= :id and is_delete=false", nativeQuery = true)
	void updateRole(String name, String code, long modified_by, long id);
	
	@Modifying
	@Query(value = "update m_role set is_delete=true, deleted_on=now(), deleted_by= :deleted_by where id= :id", nativeQuery = true)
	void deleteRole(long deleted_by, long id);

	@Query(value="select id, name, code from m_role where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>roleorder();
	

	@Query(value = "select * from m_role where id= :id limit 1", nativeQuery = true)
	RoleModel rolebyid(long id);
	
	@Query(value = "select coalesce(count(name),0) as valid from m_role where name= :name and is_delete=false", nativeQuery = true)
	int roleNameCek(String name);
	
	@Query(value = "select coalesce(count(code),0) as valid from m_role where code= :code and is_delete=false", nativeQuery = true)
	int roleCodeCek(String code);
	
	@Modifying
	@Query(value = "update m_role set is_delete=true where id= :id ", nativeQuery = true)
	int deleterole(long id);

}
