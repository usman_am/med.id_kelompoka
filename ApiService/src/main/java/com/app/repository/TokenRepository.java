package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.TokenModel;

@Transactional
public interface TokenRepository extends JpaRepository<TokenModel, Long>{

	//place holder
	@Query(value = "select id, email, user_id, token, expired_on,"
			+ " is_expired, used_for from t_token where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>listToken();
	
	//ambil id yang paling baru dibuat berdasarkan email
	@Query(value = "select id from t_token where email= :email and is_delete=false order by id desc limit 1", nativeQuery = true)
	int selectToken(String email);
	
	//ambil token berdasarkan id untuk dikirim via email
		@Query(value = "select token from t_token where email= :email and is_delete=false order by id desc limit 1", nativeQuery = true)
		String sendToken(String email);
	
	//cek expired token
	@Query(value = "select coalesce(count(is_expired),0) from t_token where id = :id and is_expired=false and is_delete=false", nativeQuery = true)
	String cekValid(int id);
	
	//cek valid token
	@Query(value = "select coalesce(sum(id),0) from t_token where id = :id and token= :token and is_delete=false", nativeQuery = true)
	int cekToken(int id, String token);
	
	//update filed expired setiap detik
	@Modifying
	@Query(value = "update t_token set is_expired=true where expired_on < now() and is_delete=false", nativeQuery = true)
	void updateExpired();
	
	//expiring token by id
	@Modifying
	@Query(value = "update t_token set is_expired=true where email= :email and is_delete=false", nativeQuery = true)
	void setExpired(String email);
}
