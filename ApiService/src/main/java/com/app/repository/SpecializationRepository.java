package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.SpecializationModel;
@Transactional
public interface SpecializationRepository extends JpaRepository<SpecializationModel, Long> {

	@Query(value = "select * from m_specialization where is_delete=false order by id",nativeQuery = true)
	List<SpecializationModel> listSpecialization();
	
	@Query(value = "select * from m_specialization where id= :id limit 1",nativeQuery = true)
	SpecializationModel SpecializationById(long id);
	
	@Query(value = "select * from m_specialization where lower (name) like lower(concat('%',:nm,'%')) and is_delete=false order by id",nativeQuery = true)
	List<SpecializationModel> listSpecializationByName(String nm);
	
	@Modifying
	@Query(value = "update m_specialization set is_delete=true where id= :id",nativeQuery = true)
	int deleteSpecialization(long id);

}
