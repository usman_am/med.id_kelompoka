package com.app.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataModel;

public interface TabProfilRepository extends JpaRepository<BiodataModel, Long>{

	@Query(value = "select b.fullname as fullname, b.mobile_phone as phone, u.email as email, u.password as password, to_char(c.dob, 'dd/mm/yyyy') as dob from m_biodata b join m_user u on b.id=u.biodata_id join m_customer c on b.id=c.biodata_id where b.id=:id",nativeQuery = true)
	List<Map<String, Object>> listprofil(long id);
}
