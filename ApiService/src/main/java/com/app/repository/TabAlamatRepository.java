package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataAddressModel;

@Transactional
public interface TabAlamatRepository extends JpaRepository<BiodataAddressModel, Long>{

	@Query(value = "select * from m_biodata_address where is_delete=false and biodata_id=:biodataid order by label", nativeQuery = true)
	List<BiodataAddressModel> listAddressActive(long biodataid);
	
	@Query(value = "select * from m_biodata_address where is_delete=false and id=:id limit 1", nativeQuery = true)
	BiodataAddressModel listId(long id);
	
	@Modifying
	@Query(value = "update m_biodata_address set is_delete=true where id=:id", nativeQuery = true)
	int deleteAlamat(long id);
	
	@Query(value = "select * from (select * from m_biodata_address where is_delete=false) as uhuy where lower(address) like (concat('%',:a,'%')) or lower(recipient) like (concat('%',:a,'%'))", nativeQuery = true)
	List<BiodataAddressModel> search(String a);
	
	@Query(value = "select * from m_biodata_address where is_delete=false order by :sortingby :sort", nativeQuery = true)
	List<Map<String, Object>> sortAddress(String sortingby, String sort);
	
	@Query(value = "select * from m_biodata_address where is_delete=false order by label", nativeQuery = true)
	List<BiodataAddressModel> listLabelAsc();
	@Query(value = "select * from m_biodata_address where is_delete=false order by label desc", nativeQuery = true)
	List<BiodataAddressModel> listLabelDesc();
	
	@Query(value = "select * from m_biodata_address where is_delete=false order by recipient", nativeQuery = true)
	List<BiodataAddressModel> listRecipAsc();
	@Query(value = "select * from m_biodata_address where is_delete=false order by recipient desc", nativeQuery = true)
	List<BiodataAddressModel> listRecipDesc();
	
	@Query(value = "select * from (select * from m_biodata_address where is_delete=false and biodata_id=:biodataid) as uhuy where lower(label) like (concat('%',:a,'%')) or lower(recipient) like (concat('%',:a,'%')) order by label", nativeQuery = true)
	List<BiodataAddressModel> searchLabelAsc(long biodataid,String a);
	
	@Query(value = "select * from (select * from m_biodata_address where is_delete=false and biodata_id=:biodataid) as uhuy where lower(label) like (concat('%',:a,'%')) or lower(recipient) like (concat('%',:a,'%')) order by label desc", nativeQuery = true)
	List<BiodataAddressModel> searchLabelDesc(long biodataid,String a);
	
	@Query(value = "select * from (select * from m_biodata_address where is_delete=false and biodata_id=:biodataid) as uhuy where lower(label) like (concat('%',:a,'%')) or lower(recipient) like (concat('%',:a,'%')) order by recipient", nativeQuery = true)
	List<BiodataAddressModel> searchRecipAsc(long biodataid,String a);
	
	@Query(value = "select * from (select * from m_biodata_address where is_delete=false and biodata_id=:biodataid) as uhuy where lower(label) like (concat('%',:a,'%')) or lower(recipient) like (concat('%',:a,'%')) order by recipient desc", nativeQuery = true)
	List<BiodataAddressModel> searchRecipDesc(long biodataid,String a);
	
	@Query(value = "select * from (select * from m_biodata_address where is_delete=false and biodata_id=:id) as uhuy where lower(label) like :a", nativeQuery = true)
	List<BiodataAddressModel> searchValid(String a,long id);
	
	@Query(value = "select l.id as id, CONCAT(LL.abbreviation,' ',l.name) keckota from m_location l JOIN m_location_level ll ON ll.id = l.location_level_id",nativeQuery = true)
    List<Map<String, Object>> listlokasi();
	
}
