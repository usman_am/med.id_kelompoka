package com.app.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataModel;

@Transactional
public interface ProfilRepository extends JpaRepository<BiodataModel, Long> {

	@Query(value = "select * from m_biodata where id=:id limit 1", nativeQuery = true)
	BiodataModel listprofil(long id);

	@Modifying
	@Query(value = "update m_biodata set image=:gbr where id=:id ", nativeQuery = true)
	int updateGbr(long id, byte[] gbr);
}
