package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.CustomerMemberModel;


@Transactional
public interface CustomerMemberRepository extends JpaRepository<CustomerMemberModel, Long>{

	@Query(value = "select * from m_customer_member where is_delete=false order by id", nativeQuery = true)
	List<CustomerMemberModel> listCustomerMember();
	
	@Query(value = "select * from m_customer_member where id=:id limit 1", nativeQuery = true)
	CustomerMemberModel customerMemberById(long id);
	
	@Query(value = "select * from m_customer_member where lower(name) like lower(concat('%',:nm,'%')) and is_delete=false order by id", nativeQuery = true)
	List<CustomerMemberModel> listCustomerMemberByName(String nm);
	
	@Modifying
	@Query(value = "update m_customer_member set is_delete=true where id=:id", nativeQuery = true)
	int deleteCustomerMember(long id);
	
}
