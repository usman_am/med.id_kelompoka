package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.UserModel;

@Transactional
public interface UserRepository extends JpaRepository<UserModel, Long>{

	@Query(value = "select id, biodata_id, role_id, email, password,"
			+ " login_attempt, is_locked, last_login from m_user where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>listUser();
	
	@Query(value = "select id from m_user where is_delete=false order by id desc limit 1", nativeQuery = true)
	int selectById();
	
	@Query(value = "select id from m_user where email= :email and is_delete=false order by id desc limit 1", nativeQuery = true)
	int selectIdbyEmail(String email);
	
	@Modifying
	@Query(value = "update m_user set created_by= :id where id= :id and is_delete=false", nativeQuery = true)
	void setDefaultCreatedBy(long id);
	
	@Query(value = "select coalesce(sum(id),0) as valid, is_locked from m_user where email = :email and is_delete=false group by is_locked", nativeQuery = true)
	Map<String, Object> cekEmail(String email);
	
	
	@Query(value = "select coalesce(sum(id),0) from m_user where email = :email and password = :password and is_delete=false", nativeQuery = true)
	int cekPassword(String email, String password);
	
	@Query(value = "select password from m_user where email = :email and is_delete=false", nativeQuery = true)
	String getPassword(String email);
	
	@Modifying
	@Query(value = "update m_user set password = :password,login_attempt=0, is_locked=false, modified_by= :modified_by, modified_on=now() where id= :id and email= :email", nativeQuery = true)
	int updatePassword(String password,long modified_by, long id, String email);
	
	@Modifying
	@Query(value = "update m_user set is_locked=true where login_attempt=3", nativeQuery = true)
	void updateAttempt();
	
	@Modifying
	@Query(value = "update m_user set login_attempt = login_attempt+1 where email= :email", nativeQuery = true)
	void loginAttempt(String email);
	
	@Modifying
	@Query(value = "update m_user set login_attempt = 0, last_login=now() where email= :email", nativeQuery = true)
	void loginAttemptreset(String email);


	@Query(value = "select m.url,m.name from m_user u join m_biodata b on b.id=u.biodata_id "
			+ "join role r on r.id=u.role_id join m_menu m on m.parent_id=r.id where u.id= :id", nativeQuery = true)
	List<Map<String, Object>> getrole(long id);

	@Query(value = "select role_id from m_user where email= :email and is_delete=false", nativeQuery = true)
	int getRole2(String email);
	
	@Query(value = "select biodata_id from m_user where email= :email and is_delete=false", nativeQuery = true)
	int getBiodataId(String email);

}
