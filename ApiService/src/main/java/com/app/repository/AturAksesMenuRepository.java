package com.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataAddressModel;
import com.app.model.RoleModel;

public interface AturAksesMenuRepository extends JpaRepository<RoleModel, Long>{

	@Query(value = "select * from m_role where is_delete=false", nativeQuery = true)
	List<RoleModel> listrole();
	
	@Query(value = "select * from (select * from m_role where is_delete=false) as uhuy where lower(name) like (concat('%',:a,'%'))", nativeQuery = true)
	List<RoleModel> search(String a);
}
