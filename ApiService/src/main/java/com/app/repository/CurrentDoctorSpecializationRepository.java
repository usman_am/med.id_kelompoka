package com.app.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.CurrentDoctorSpecializationModel;

public interface CurrentDoctorSpecializationRepository extends JpaRepository<CurrentDoctorSpecializationModel, Long> {
	
	@Query(value = "select s.name,cds.id,cds.doctor_id,cds.specialization_id from m_specialization s "
			+ "join t_current_doctor_specialization cds on s.id=cds.specialization_id "
			+ "join m_doctor d on d.id=cds.doctor_id where d.is_delete=false order by d.id",nativeQuery = true)
	List<Map<String, Object>> listCurrentSpecializationActive();
	
	@Query(value = "select s.name,cds.id,cds.doctor_id,cds.specialization_id from m_specialization s "
			+ "join t_current_doctor_specialization cds on s.id=cds.specialization_id "
			+ "join m_doctor d on d.id=cds.doctor_id where d.id= :id",nativeQuery = true)
	Map<String, Object> currentSpecializationById(long id);

}
