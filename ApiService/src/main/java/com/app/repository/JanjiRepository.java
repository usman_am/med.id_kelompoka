package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.model.BiodataModel;
import com.app.model.CustomerModel;
import com.app.model.DokterModel;
import com.app.model.JanjiModel;
import com.app.model.UserModel;

@Transactional
public interface JanjiRepository extends JpaRepository<JanjiModel, Long>{

	@Query(value="select \r\n"
			+ " mb.fullname as nama_keluarga,\r\n"
			+ " mcr.name as hubungan_keluarga,\r\n"
			+ " tdt.name as tindakan_medis\r\n"
			+ " from t_appointment ta join m_customer mc on mc.id=ta.customer_id\r\n"
			+ " join t_doctor_office tdo on ta.doctor_office_id=tdo.id\r\n"
			+ " join t_doctor_office_schedule tdos on tdos.id=ta.doctor_office_schedule_id\r\n"
			+ " join t_doctor_office_treatment tdot on tdot.id=ta.doctor_office_treatment_id\r\n"
			+ " join t_appointment_done tad on tad.appointment_id= ta.id\r\n"
			+ " join t_appointment_cancellation tac on tac.appointment_id=ta.id\r\n"
			+ " join m_customer_member mcm on mcm.customer_id=mc.id\r\n"
			+ " join m_customer_relation mcr on mcm.customer_relation_id=mcr.id\r\n"
			+ " join m_biodata mb on mb.id=mc.biodata_id\r\n"
			+ " join m_doctor md on md.biodata_id=mb.id\r\n"
			+ " join t_doctor_treatment tdt on tdt.doctor_id=md.id where mb.id = :id and mu.id= :id", nativeQuery = true)
			List<Map<String, Object>> listBuatJanji(long id);
	
	@Query(value = "select * from m_biodata where is_delete=false order by id", nativeQuery = true)
	List<BiodataModel> listBiodataActive();
	
	@Query(value = "select * from m_customer where is_delete=false order by id", nativeQuery = true)
	List<CustomerModel> listCustomerActive();
	
	@Query(value = "select * from m_doctor where is_delete=false order by id", nativeQuery = true)
	List<DokterModel> listDokterActive();
	
	
}

