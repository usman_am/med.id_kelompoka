package com.app.repository;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.CustomerModel;


@Transactional
public interface CustomerRepository extends JpaRepository<CustomerModel , Long> {

	@Query(value ="select"
			+ "	c.id,"
			+ "	b.fullname,"
			+ "	c.gender,"
			+ "	bg.code,"
			+ "	date_part('year', age(date(c.dob))) as usia,"
			+ "	c.rhesus_type,"
			+ "	c.height,"
			+ "	c.weight,"
			+ "	cr.name"
			+ " from m_biodata b "
			+ "join m_customer c on b.id = c.biodata_id "
			+ "join m_blood_group bg on c.blood_group_id = bg.id "
			+ "join m_customer_member cm on c.id = cm.customer_id "
			+ "join m_customer_relation cr on cr.id = cm.customer_relation_id "
			+ "where c.is_delete = false "
			+ "order by c.id", nativeQuery = true)
	List<Map<String, Object>> listCS();

	@Query(value ="select"
            + "    c.id,"
            + "    b.fullname,"
            + "    c.gender,"
            + "    c.blood_group_id,"
            + "    cm.customer_relation_id,"
            + "    bg.code,"
            + " to_char(c.dob, 'DD-MM-YYYY') as tanggalLahir,"
            + "    date_part('year', age(date(c.dob))) as usia,"
            + "    c.rhesus_type,"
            + "    c.height,"
            + "    c.weight,"
            + "    cr.name,"
            + "    c.biodata_id,"
            + "    cm.id as cmid"
            + " from m_biodata b "
            + "join m_customer c on b.id = c.biodata_id "
            + "join m_blood_group bg on c.blood_group_id = bg.id "
            + "join m_customer_member cm on c.id = cm.customer_id "
            + "join m_customer_relation cr on cr.id = cm.customer_relation_id "
            + "where c.id = :id", nativeQuery = true)
    	List<Map<String, Object>> listCSById(long id);
	
	@Query(value = "select * from m_customer where is_delete=false order by id", nativeQuery = true)
	List<CustomerModel> listCustomer();
	
	@Query(value = "select * from m_customer where id=:id and is_delete=false limit 1", nativeQuery = true)
	CustomerModel customerById(long id);
	
	@Query(value = "select * from m_customer where lower(name) like lower(concat('%',:nm,'%')) and is_delete=false order by id", nativeQuery = true)
	List<CustomerModel> listCustomerByName(String nm);
	
	@Modifying
	@Query(value = "update m_customer set is_delete=true where id=:id and is_delete=false", nativeQuery = true)
	int deleteCustomer(long id);
	
	@Modifying
	@Query(value = "update m_customer_member set is_delete=true where id=:id", nativeQuery = true)
	int deleteCustomerMember(long id);
	
	@Modifying
	@Query(value = "update m_biodata set is_delete=true where id=:id", nativeQuery = true)
	int deleteBiodata(long id);
	
	@Modifying
	@Query(value = "update m_customer set is_delete=true where id in (:id)", nativeQuery = true)
	void deleteCs(List<Integer> id);
	
	@Modifying
	@Query(value = "update m_customer set created_by= :id, created_on=now() where biodata_id= :biodata_id", nativeQuery = true)
	void customerRegisterPart(long id, long biodata_id);

}
