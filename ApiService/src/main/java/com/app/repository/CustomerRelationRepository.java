package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.CustomerRelationModel;

@Transactional
public interface CustomerRelationRepository extends JpaRepository<CustomerRelationModel, Long>{

	@Query(value = "select * from m_customer_relation where is_delete=false order by id", nativeQuery = true)
	List<CustomerRelationModel> listCustomerRelation();
	
	@Query(value = "select * from m_customer_relation where id=:id limit 1", nativeQuery = true)
	CustomerRelationModel customerRelationById(long id);
	
	@Query(value = "select * from m_customer_relation where lower(name) like lower(concat('%',:nm,'%')) and is_delete=false order by id", nativeQuery = true)
	List<CustomerRelationModel> listCustomerRelationByName(String nm);
	
	@Query(value = "select * from m_customer_relation where lower(name) = lower(:nama) and is_delete=false order by id", nativeQuery = true)
	List<CustomerRelationModel> listCustomerRelationDuplicate(String nama);

	@Modifying
	@Query(value = "update m_customer_relation set is_delete=true where id=:id", nativeQuery = true)
	int deleteCustomerRelation(long id);
	
}
