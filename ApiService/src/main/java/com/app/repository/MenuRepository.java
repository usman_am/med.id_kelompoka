package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.MenuModel;


@Transactional
public interface MenuRepository extends JpaRepository<MenuModel, Long>{
	@Query(value = "select * from m_menu where is_delete=false order by id", nativeQuery = true)
	List<MenuModel> listmenu();
	
	@Query(value = "select * from m_menu where id= :id limit 1", nativeQuery = true)
	MenuModel menubyid(long id);
	
	@Modifying
	@Query(value = "update m_menu set is_delete=true where id= :id ", nativeQuery = true)
	int deletemenu(long id);

}
