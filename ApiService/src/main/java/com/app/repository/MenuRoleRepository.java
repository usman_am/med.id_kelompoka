package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.model.MenuRoleModel;


@Transactional
public interface MenuRoleRepository extends JpaRepository<MenuRoleModel, Long>{
	@Query(value = "select * from m_menu_role where is_delete=false order by id", nativeQuery = true)
	List<MenuRoleModel> listmenurole();
	
	@Query(value = "select * from m_menu_role where id= :id limit 1", nativeQuery = true)
	MenuRoleModel menurolebyid(long id);
	
	@Modifying
	@Query(value = "update m_menu_role set is_delete=true where id= :id ", nativeQuery = true)
	int deletemenurole(long id);

}
