insert into m_role(name,code) values
('admin','AD001'),('pasien','PA001'),('dokter','DO001'),('faskes','FK001');

insert into m_biodata
(fullname,image_path,mobile_phone,created_by,created_on,modified_by,deleted_by)
values
('dr.Tatjana Saphira,Sp.A','imagee','1234567',1,now(),1,1),
('dr.Maudy Ayunda,Sp.M','imagee','12346789',1,now(),1,1),
('dr.Fabian,Sp.N','imagee','12346789',1,now(),1,1),
('Iori Moe','imagee','12346789',1,now(),1,1),
('Reinhard Apriadi','imagee','12346789',1,now(),1,1);

insert into m_customer_member(customer_id,customer_relation_id) values
('4','2');

insert into m_user(biodata_id,role_id,email,password,login_attempt,is_locked,last_login)values
(1,1,'index@hotmail.com','123',0,false,now()),
(2,3,'default@hotmail.com','123',0,false,now()),
(3,2,'tottemokawaii@gmail.com','241','1',false,now()),
(4,2,'iori_moe@gmail.com','123','1',false,now());

insert into m_blood_group(code, description, created_by, created_on, modifed_by, modifed_on, deleted_by, deleted_on)
values('A', '-', 1, now(), 1, now(), 1, now()),('B', '-', 1, now(), 1, now(), 1, now()),('C', '-', 1, now(), 1, now(), 1, now());

insert into m_customer_relation(name, created_by, created_on, modifed_by, modifed_on, deleted_by, deleted_on)
values('Anak', 1, now(), 1, now(), 1, now()),('Suami', 1, now(), 1, now(), 1, now()),('Istri', 1, now(), 1, now(), 1, now());

insert into m_customer(biodata_id, dob, gender, blood_group_id, rhesus_type, height, weight, created_by, created_on, modifed_by, modifed_on, deleted_by, deleted_on)
values
(1, now(), 'L', 1, '+', 172, 78, 1, now(), 1, now(), 1, now()), 
(2, now(), 'P', 2, '-', 172, 78, 1, now(), 1, now(), 1, now()),
(3, now(), 'L', 2, '-', 170, 70, 1, now(), 1, now(), 1, now()),
(4, '1993-01-24', 'P', 2, '+', 161, 48, 1, now(), 1, now(), 1, now());

insert into m_biodata_address (biodata_id,label,recipient,recipient_phone_number,location_id,postal_code,address,created_by,created_on,modified_by,deleted_by) values 
(4,'Kantor','Anton','088122321787',1,'14141','Jl. taubat nasuha',666,now(),0,0),
(5,'Rumah','Slamet','088109874576',1,'14142','Jl. jalan',666,now(),0,0);

insert into m_doctor (biodata_id,str,created_by,created_on,modified_by,deleted_by) values
(1,'cek',1,now(),1,1),
(2,'cek 2',1,now(),1,1),
(3,'cek 3',1,now(),1,1);

insert into m_specialization
(name,created_by,created_on,modified_by,deleted_by)
values
('Spesialis Anak',1,now(),1,1),
('Spesialis Mata',1,now(),1,1),
('Spesialis Saraf',1,now(),1,1),
('Spesialis Bedah',1,now(),1,1);

insert into t_current_doctor_specialization
(doctor_id,specialization_id,created_by,created_on,modified_by,deleted_by)
values
(1,1,1,now(),1,1),
(2,2,1,now(),1,1);

insert into t_doctor_treatment
(doctor_id,name,created_by,created_on,modified_by,deleted_by)
values
(1,'- Fisioterapi Anak',1,now(),1,1),
(1,'- Konsultasi Kesehatan Anak',1,now(),1,1),
(1,'- Skrining Tumbuh Kembang Anak',1,now(),1,1),
(1,'- Vaksin Anak',1,now(),1,1),
(1,'- Konsultasi Alergi Anak',1,now(),1,1),
(1,'- Konsultasi Psikologi Anak',1,now(),1,1),
(1,'- Tes Pendengaran OAE',1,now(),1,1);

insert into m_medical_facility
(name,medical_facility_category_id,location_id,full_address,email,phone_code,phone,fax,created_by,created_on,modified_by,deleted_by)
values
('RS Mitra',1,1,'Jln Depok No.5','tatjana@gmail.com','1234512345','6789','fax_abcd',1,now(),1,1),
('RSIA Bunda',1,2,'Jln Depok No.5','tatjana@gmail.com','1234512345','6789','fax_abcd',1,now(),1,1),
('RSUP Hasan Sadikin',1,3,'Jln Depok No.5','tatjana@gmail.com','1234512345','6789','fax_abcd',1,now(),1,1);

insert into m_location
(name,parent_id,location_level_id,created_by,created_on,modified_by,deleted_by)values
('Depok',1,1,1,now(),1,1),
('Jakarta Pusat',1,1,1,now(),1,1),
('Bandung',1,1,1,now(),1,1);

insert into t_doctor_office
(doctor_id,medical_facility_id,specialization,created_by,created_on,modified_by,deleted_by,deleted_on,is_delete)
values
(1,1,'Dokter Anak',1,now(),1,1,null,false),
(1,2,'Dokter Anak',1,'2020-03-06 23:02:40.340756',1,1,'2022-03-06 23:02:40.340756',true);

insert into m_doctor_education
(doctor_id,education_level_id,institution_name,major,start_year,end_year,created_by,created_on,modified_by,deleted_by)
values
(1,1,'Universitas Padjadjaran','Spesialis Anak','2012','2016',1,now(),1,1),
(1,1,'Universitas Indonesia','Dokter Anak','2013','2018',1,now(),1,1);

insert into m_bank (name,va_code,created_by,created_on,modified_by,deleted_by) values
('BCA','1','666',now(),1,1),
('Bank BNI','2','666',now(),1,1),
('Bank BRI','3','666',now(),1,1),
('Bank BJB','4','666',now(),1,1),
('HSBC','5','666',now(),1,1),
('SeaBank','6','666',now(),1,1);


insert into t_customer_chat
(customer_id,doctor_id,created_by,created_on,modified_by,deleted_by)
values
(1,1,1,now(),1,1),
(2,2,1,now(),1,1);

insert into t_customer_custom_nominal
(customer_id,nominal,created_by,created_on,modified_by,deleted_by)
values
(1,30000,1,now(),1,1),
(2,35000,1,now(),1,1);

insert into t_doctor_office_treatment
(doctor_treatment_id,doctor_office_id,created_by,created_on,modified_by,deleted_by)
values
(1,1,1,now(),1,1),
(2,2,1,now(),1,1);

insert into t_doctor_office_treatment_price
(doctor_office_treatment_id,price,price_start_from,price_until_from,created_by,created_on,modified_by,deleted_by)
values
(1,400000,300000,500000,1,now(),1,1),
(2,350000,250000,500000,1,now(),1,1);

insert into m_medical_facility_category
(name,created_by,created_on,modified_by,deleted_by)
values
('Poliklinik Anak',1,now(),1,1),
('Poliklinik Anak-Anak',1,now(),1,1);

insert into m_location_level
(name,abbreviation,created_by,created_on,modified_by,deleted_by)
values
('Kecamatan','Kota',1,now(),1,1),
('Kecamatan','Kota',1,now(),1,1);

insert into m_medical_facility_schedule
(medical_facility_id,day,time_schedule_start,time_schedule_end,created_by,created_on,modified_by,deleted_by)
values
(1,'Senin','18:00','21:00',1,now(),1,1),
(2,'Rabu','19:00','22:00',1,now(),1,1);


insert into t_doctor_office_schedule
(doctor_id,medical_facility_schedule_id,slot,created_by,created_on,modified_by,deleted_by)
values
(1,1,1,1,now(),1,1),
(1,2,1,1,now(),1,1);


--insert into m_role(name,code) values
--('admin','AD001'),('pasien','PA001'),('dokter','DO001'),('faskes','FK001');

insert into m_menu(name,url,parent_id,create_by,create_on,delete_by,modified_by) values
('User Management','/usermanagement',1,1,now(),0,0), 
('Hak Akses','/hakakses',3,1,now(),0,0),
('Menu Management','/aturhakakses',3,1,now(),0,0),
('Dokter','/dokter',2,1,now(),0,0),
('Golongan Darah','/blood',3,1,now(),0,0),
('Hubungan','/hubungan',3,1,now(),0,0),
('Bank','/bank',3,1,now(),0,0),
('Lokasi','/lokasi',3,1,now(),0,0),
('Lokasi Level','/lokasilevel',3,1,now(),0,0),
('Cari Dokter','/caridokter',3,1,now(),0,0),
('Cari Obat','/cariobat',3,1,now(),0,0),
('Buat Janji','/buatjanji',3,1,now(),0,0),
('Profile','/profile',1,1,now(),0,0);

insert into m_menu_role(menu_id,role_id,create_by,create_on,delete_by,modified_by) values
(1,1,1,now(),0,0),
(2,1,1,now(),0,0),
(3,1,1,now(),0,0), 
(13,2,1,now(),0,0),
(10,2,1,now(),0,0),
(11,2,1,now(),0,0),
(12,2,1,now(),0,0), 
(5,1,1,now(),0,0),
(6,1,1,now(),0,0),
(7,1,1,now(),0,0),
(8,1,1,now(),0,0),
(9,1,1,now(),0,0), 
(4,3,1,now(),0,0);
