$(document).ready(function(){
	$("#inBankCari").keyup(function(){
		let cari = $(this).val()
		list(cari)
	})
	
	list()
	function list(isi){
		let alamat=""
		if(isi==undefined || isi==""){
		alamat="http://localhost:81/api/bank/list"
	}else{
		alamat= "http://localhost:81/api/bank/listbyName/"+isi
	}	
		$.ajax({
				url:alamat,
				type:"GET",
				success : function(hasil){
				let txt=""
				txt+="<table class='thetable table table-hover'>"
				txt+="<thead>"
				txt+="<tr>"
				txt+="<th>Id</th>"
				txt+="<th>Name</th>"
				txt+="<th>Kode</th>"
				txt+="<th>Aksi</th>"
				txt+="</tr>"
				txt+="</thead>"
				txt+="<tbody>"
				for(i=0; i<hasil.length;i++){
					txt+="<tr>"
					txt+="<td>" + hasil[i].id+"</td>"
					txt+="<td>" + hasil[i].name+"</td>"
					txt+="<td>" + hasil[i].vacode+"</td>"
					txt+="<td> <input type= 'button' value='Edit'class='btnEdit btn btn-success btn-sm col-xs-2 margin-left' name='"+hasil[i].id+"'> <input type= 'button' value='Delete'class='btnDelete btn btn-danger btn-sm col-xs-2 margin-left' name='"+hasil[i].id+"'> </td>"
					txt+="</tr>"
				}
				$(".tb1").empty();
				$(".tb1").append(txt)
				
				$(".btnEdit").click(function(){
					let id=$(this).attr("name");
					sessionStorage.setItem("id",id);
					$.ajax({
						url 	 :"http://localhost/editbank",
						type	 :"GET",
						dataType :"html",
						success  : function(hasil){
							$("#modalBank").modal("show")
							$(".isiModalBank").html(hasil)
						}
					})
					
				})
				
				$(".thetable").dataTable({//buat datatable
					dom:  '<lf<t>ip>', //turunin posisi entries
					searching: false, 
					paging: true, 
					info: false,
					"language": {
						"paginate": {
				            "first": ">",
				            "last": "<",
				            "next": ">",
				            "previous": "<"
				           
			        }}, //rename next back
			        
					pageLength : 3, //custome nilai paging
    				lengthMenu: [[3, 5, 10, -1], [3, 5, 10, 'ALL']],
   					"bDestroy": true
					});//hapus cari
					
				
				$(".btnDelete").click(function(){
					let id=$(this).attr("name");
					sessionStorage.setItem("id",id);
					
					$.ajax({
						url 	 :"http://localhost/deletebank",
						type	 :"GET",
						dataType :"html",
						success  : function(hasil){
							$("#modalBank").modal("show")
							$(".isiModalBank").html(hasil)
						}
					})
					return false;
					
				})
					
			}
		})
	}
	
	$("#tbreset").click(function(){
		list()
	})
	
	
	$("#tadd1").click(function(){	
		$.ajax({ 
			url  		:"http://localhost/addbank",
			type 		:"GET",
			dataType	:"html",
			success		: function(hasil){
					$("#modalBank").modal("show")
					$(".isiModalBank").html(hasil)
				}
		})
		return false;
	})
	
	



});