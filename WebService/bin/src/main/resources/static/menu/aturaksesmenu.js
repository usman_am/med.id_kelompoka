$(document).ready(function() {

	$(".search").keyup(function() {
		let cari = $(this).val()
		list(cari)
	})

	function list(isi) {
		let alamat = ""
		if (isi == undefined || isi.replace(/ /g, "").length == 0) {
			alamat = "http://localhost:81/api/aksesmenu/list"
		} else {
			alamat = "http://localhost:81/api/aksesmenu/search/" + isi
			$(".tb1").empty()
		}
		$.ajax({
			url: alamat,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				let txt = ""
				txt += '<thead><td>Nama</td><td>Kode</td><td>#</td></thead>'
				for (i = 0; i < hasil.length; i++) {
					txt += "<tbody>"
					txt += "<td style='width:40%'>" + hasil[i].name + "</td>"
					txt += "<td style='width:40%'>" + hasil[i].code + "</td>"
					txt += "<td style='width:20% '><input type='button' value='Atur' class='btnatur btn btn-primary btn-sm' name=" + hasil[i].id + "></td>"
					txt += "</tbody>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)
				
				$(".btnatur").click(function() {
					let id = $(this).attr("name")
					sessionStorage.setItem("roleid", id)
					$.ajax({
						url: "http://localhost/modalaturakses",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#ModalAturAkses").modal("show")
							$(".isiModalAturAkses").html(hasil)
						}
					})
					return false
				})
			}
		})
	}

	list()
})