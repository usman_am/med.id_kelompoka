$(document).ready(function(){
	
	list()
	
	function list(isi){
		let alamat = ""
		if(isi == undefined || isi == "") {
			alamat = "http://localhost:81/api/hubunganpasien/list"
		} else {
			alamat = "http://localhost:81/api/hubunganpasien/listbyname/"+isi
		}
		
		$.ajax({
			url: alamat,
			type: "GET",
			success:function(hasil){
				let txt = ""
				for(i=0; i<hasil.length; i++){
					txt += "<tr>"
					txt += "<td>"+hasil[i].name+"</td>"
					txt += "<td><button class='btnedit btn btn-warning' name='"+hasil[i].id+"'>Edit</button>   <button class='btndelete btn btn-danger' name='"+hasil[i].id+"'>Delete</button></td>"
					txt += "</tr>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)
				
				$(".btnedit").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/edithubunganpasien",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							//window.open("http://localhost/editgolongandarah")
							$("#modalHubunganPasien").modal('show')
							$(".isiModal").html(hasil)
						}
					})
					//window.location.reload(true);
					return false
				})
				
				$(".btndelete").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/deletehubunganpasien",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							//window.open("http://localhost/deletegolongandarah")
							$("#modalHubunganPasien").modal('show')
							$(".isiModal").html(hasil)
						}
					})
					return false
				})
			}
		})
	}
	
	$("#iSearch").keyup(function(){
	//	alert($(this).val())
		let cari = $(this).val()
		list(cari)
	})
	
	$("#btnTambah").click(function(){
		$.ajax({
			url: "http://localhost/addhubunganpasien",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				//window.open("http://localhost/editgolongandarah")
				$("#modalHubunganPasien").modal('show')
				$(".isiModal").html(hasil)
			}
		})
		//window.location.reload(true);
		return false
	})
	
})