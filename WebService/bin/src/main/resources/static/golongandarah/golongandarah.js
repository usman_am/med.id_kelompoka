$(document).ready(function(){
	
	list()
	
	function list(isi){
		let alamat = ""
		if(isi == undefined || isi == "") {
			alamat = "http://localhost:81/api/golongandarah/list"
		} else {
			alamat = "http://localhost:81/api/golongandarah/listbyname/"+isi
		}
		
		$.ajax({
			url: alamat,
			type: "GET",
			success:function(hasil){
				let txt = ""
				for(i=0; i<hasil.length; i++){
					txt += "<tr>"
					txt += "<td>"+hasil[i].code+"</td>"
					txt += "<td>"+hasil[i].description+"</td>"
					txt += "<td><button class='btnedit btn btn-warning' name='"+hasil[i].id+"'>Edit</button>   <button class='btndelete btn btn-danger' name='"+hasil[i].id+"'>Delete</button></td>"
					txt += "</tr>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)
				
				$(".btnedit").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/editgolongandarah",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							//window.open("http://localhost/editgolongandarah")
							$("#modalGolonganDarah").modal('show')
							$(".isiModal").html(hasil)
						}
					})
					//window.location.reload(true);
					return false
				})
				
				$(".btndelete").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/deletegolongandarah",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							//window.open("http://localhost/deletegolongandarah")
							$("#modalGolonganDarah").modal('show')
							$(".isiModal").html(hasil)
						}
					})
					return false
				})
			}
		})
	}
	
	$("#iSearch").keyup(function(){
		let cari = $(this).val()
		list(cari)
	})
	
	$("#btnTambah").click(function(){
		$.ajax({
			url: "http://localhost/addgolongandarah",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				//window.open("http://localhost/editgolongandarah")
				$("#modalGolonganDarah").modal('show')
				$(".isiModal").html(hasil)
			}
		})
		//window.location.reload(true);
		return false
	})
	
})