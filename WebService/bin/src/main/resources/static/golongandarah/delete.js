$(document).ready(function(){
	
	golonganDarahById()
	
	function golonganDarahById(){
		let id = sessionStorage.getItem("id")
		$.ajax({
			url : "http://localhost:81/api/golongandarah/list/"+id,
			type : "GET",
			success : function(hasil){
				$("#inKode").text("Apakah ingin menghapus golongan darah "+hasil.code+" ?")
			}
		})
	}
	
	$("#btnHapus").click(function(){
		let id = sessionStorage.getItem("id")
		
		var obj={}
		obj.id = id
		obj.code = $("#inKode").val()
		obj.description = $("#inDeskripsi").val()
		
		var myjson = JSON.stringify(obj)
		
		$.ajax({
			url : "http://localhost:81/api/golongandarah/delete",
			type : "DELETE",
			contentType : "application/json",
			data : myjson,
			success : function(hasil){
				$("#modalGolonganDarah").modal('hide')
				alert("Delete Berhasil")
				//kembali()
			},
			error: function(ts) { alert(ts.responseText) }
		})
	})
	
	$("#btnBatal").click(function(){
		$("#modalGolonganDarah").modal('hide')
	})
	
})