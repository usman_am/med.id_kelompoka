$(document).ready(function() {

	$(".btnbatal").click(function() {
		$("#ModalTindakan").modal("hide")
	})

	let id = sessionStorage.getItem("treatmentid")

	tindakan()

	function tindakan() {
		$.ajax({
			url: "http://localhost:81/api/tabtindakan/id/" + id,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				$("#hapusTindakan").text(hasil.name)
			}
		})
	}

	$(".btnhapus").click(function() {
		var obj = {}
		obj.id = id
		var myjson = JSON.stringify(obj)

		$.ajax({
			url: "http://localhost:81/api/tabtindakan/delete",
			type: "DELETE",
			contentType: "application/json",
			data: myjson,
			success: function(hasil) {
				$("#ModalTindakan").modal('hide')
				alert("Delete Berhasil")
				kembali()
			},
		})
	})

	function kembali() {
		$.ajax({
			url: "/tabtindakan",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalTindakan").modal('hide')
				$(".isiMain").html(hasil)
			}
		})
	}
})