$(document).ready(function() {
	let doctorid = sessionStorage.getItem("doctorid")

	$(".pesan").hide()
	$(".pesan1").hide()

	$(".btnbatal").click(function() {
		kembali()
	})

	$(".btnsimpan").click(function() {
		let tdk = $(".inTindakan").val()
		let tindakan = tdk.toLowerCase()
		let valid = tdk.replace(/ /g, "")

		if (valid.length == 0) {
			$(".pesan").show()
		} else {
			$(".pesan").hide()
			$.ajax({
				url: "http://localhost:81/api/tabtindakan/search/" + doctorid + "/" + tindakan,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					console.log(hasil)
					if (hasil.length != 0) {
						$(".pesan1").show()
					} else {
						$(".pesan1").hide()
						var obj = {}
						obj.doctorId = doctorid
						obj.name = $(".inTindakan").val()
						var myJson = JSON.stringify(obj)
						$.ajax({
							url: "http://localhost:81/api/tabtindakan/add",
							type: "POST",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								console.log(hasil)
								alert("Tindakan berhasil disimpan")
								kembali()
							}
						})
					}
				}
			})
		}
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/tabtindakan",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				console.log(hasil)
				$("#ModalTindakan").modal('hide')
				$(".isiMain").html(hasil)
			}
		})
	}
})