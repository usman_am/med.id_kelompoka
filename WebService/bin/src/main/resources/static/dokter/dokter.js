$(document).ready(function() {
	//pura2 lohjin
	let doctorid = 3;
	localStorage.setItem("doctorid", doctorid)

	//let idnama = localStorage.getItem("doctorid") // simpan id dulu
	$.ajax({
		url: "http://localhost:81/api/dokter/namadokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#nama").html(hasil.fullname)

		}
	})

	//let idspesialis = localStorage.getItem("doctorid")
	$.ajax({
		url: "http://localhost:81/api/dokter/spesialisdokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			if (hasil.name == null) {
				$("#spesialis").html("Belum Ada Spesialisasi")

			} else {
				$("#spesialis").html(hasil.name)
			}
			/*$("#spesialis").html(hasil.name)*/

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/tindakandokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			//console.log(hasil)
			for (i = 0; i < hasil.length; i++) {
				$("#tindakan").append(hasil[i].name + '<br>')
			}

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/riwayatdokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			let txt = ""
			for (i = 0; i < hasil.length; i++) {

				txt += "<div class='mx-1 my-0 row'>"
				txt += "<div class='col-12 my-0>"
				txt += "<p class='namars my-0'>" + (hasil[i].namars + ", " + hasil[i].namakota + "</p>")
				txt += "</div>"
				txt += "<div class='my-0 col-12>"
				txt += "<div class='col-4'>"
				if (hasil[i].is_delete == true)
					txt += "<p style='font-size: 13px' class='mx-4' >" + (hasil[i].specialization) + "<text class='mx-4'></text>" + (hasil[i].created + " - " + hasil[i].deleted) + "</p>"
				if (hasil[i].is_delete == false)
					txt += "<p style='font-size: 13px'  class='mx-4'>" + (hasil[i].specialization) + "<text class='mx-4'></text>" + (hasil[i].created + " - sekarang") + "</p>"
				txt += "</div>"
				txt += "</div>"
				txt += "</div>"

			}
			$(".tb2").append(txt)

		}
	})


	/*$.ajax({
		url			: "http://localhost:81/api/dokter/tahundokterbyid/"+doctorid,
		type		: "GET",
		contentType	: "application/json",
		success 	: function(hasil) {
			let txt = ""
			for(i=0;i<hasil.length;i++) {
				if(hasil[i].is_delete == true) 
					txt += "<text>"+(hasil[i].created + " - " + hasil[i].deleted)+"</text>" 
				if(hasil[i].is_delete == false)
					txt += "<text>"+(hasil[i].created + " - sekarang")+"<br>"+"</text>"
			}
			$(".tb3").append(txt)
			
		}
	})*/

	$.ajax({
		url: "http://localhost:81/api/dokter/pendidikandokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			let txt = ""
			for (i = 0; i < hasil.length; i++) {

				txt += "<div class='mx-1 my-0 row'>"
				txt += "<div class='col-12 my-0>"
				txt += "<p class='namars my-0'>" + (hasil[i].institution_name) + "</p>"
				txt += "</div>"
				txt += "<div class='my-0 col-12>"
				txt += "<div class='col-4'>"
				txt += "<p style='font-size: 13px' class='mx-4'>" + (hasil[i].major) + "<text class='mx-4'></text>" + (hasil[i].end_year) + "</p>"
				txt += "</div>"
				txt += "</div>"
				txt += "</div>"

			}
			$(".tb1").append(txt)

		}
	})

	$.ajax({
		url: "http://localhost:81/api/currentspesialis/list/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			if (hasil.name == null) {
				$(".isimaincurrent").html("Anda Belum Menambahkan Spesialisasi")

				str = ""
				str += "<button type=\"button\" id=\"btnadd\" style = \"font-size: 20px; text-align: justify;\"class=\"add btn btn-primary btn \" > <i class=\"bi bi-plus-circle-fill\"></i></button > "
				$(".btn1").append(str)
				
				$(".add").click(function(){
					$.ajax({
						url : "adddokter",
						type : "GET",
						dataType : "html",
						success : function(hasil) {
							$("#modalCurrentSpecialization").modal('show')
							$(".isiModalCurrentSpecialization").html(hasil)
						}
					})
					return false
				})

			} else {
				$(".isimaincurrent").html(hasil.name)
			
				str = ""
				str += "<button type=\"button\" id=\"btnedit\" style = \"font-size: 20px; text-align: justify;\"class=\"edit btn btn-primary btn \" ><i class=\"bi bi-pencil-fill\"></i> </button > "
				$(".btn1").append(str)
				
				$(".edit").click(function(){
					$.ajax({
						url : "editdokter",
						type : "GET",
						dataType : "html",
						success : function(hasil) {
							$("#modalCurrentSpecialization").modal('show')
							$(".isiModalCurrentSpecialization").html(hasil)
						}
					})
					return false
				})
			}


		}
	})

	/*$("#btnadd").click(function() {
		$.ajax({
			url: "http://localhost/add",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#modalSpesialis").modal("show")
				$(".isiModalSpesialis").html(hasil)
			}
		})
		return false
	})*/

	/*$(".b").click(function(){
		$.ajax({
			url: "http://localhost/editdokter",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#modalSpesialis").modal('show')
				$(".isiModalSpesialis").html(hasil)
			}
		})
		return false
	})
	*/

	/*function button() {
		$.ajax({
			url: "http://localhost/button",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				console.log(hasil)
				$(".b").html(hasil)
				}
		})
		return false
	}*/


	/*$("#spesialis-tab").click(function(){
		let idtab = localStorage.getItem("doctorid")
		$.ajax({
			url: "http://localhost:81/api/currentspesialis/list/"+idtab,
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				console.log(hasil)
				var obj = $.parseJSON(hasil);
				if(hasil == null){
					$(".isispesialis").html('Anda Belum Menambahkan Spesialisasi')
				} else {
					$(".isispesialis").html(obj.name)
					
				}
				button()
				
				}
		})
		return false
	})*/


	/*$("#profil").click(function(){
		var form = new FormData();
		form.append("uploadFile", fileupload.files[0]);
		form.append("id",doctorid)
		
		var settings = {
		  "url": "http://localhost:81/api/biodata/uploadgbr",
		  "method": "POST",
		  "timeout": 0,
		  "processData": false,
		  "mimeType": "multipart/form-data",
		  "contentType": false,
		  "data": form
		};
		
		$.ajax(settings).done(function (response) {
			setGambaraDokter()
		  console.log(response);
		});																						
	})*/


	setGambaraDokter()

	function setGambaraDokter() {
		$.ajax({
			url: "http://localhost:81/api/biodata/files/" + doctorid,
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#profileImage").attr("src", "http://localhost:81/api/biodata/files/" + doctorid)
			}
		})

	}

	$("#profilgbr").click(function() {
		var str = '<form id="formUp">'
		str += '<div class="form-group">'
		str += '<input id="images" type="file" name="images" accept="image/png, image/jpeg" />'
		str += '<span id="alert" style="color:red;"></span><br><br>'
		str += '</div>'
		str += '</form>'
		$('.modal-title').html('Change Image')
		$('.modal-body').html(str)
		$('#modal').modal('show')
		$('#btnSubmit').html('Save')
		$('#btnSubmit').removeClass()
		$('#btnSubmit').addClass('btn btn-success')
		$('#btnSubmit').off('click').on('click', function() { saveEditImage() })
	})



	function saveEditImage() {
		var formData = new FormData();
		formData.append("uploadFile", images.files[0]);
		formData.append("id", doctorid)
		var settings = {
			"url": "http://localhost:81/api/biodata/uploadgbr",
			"method": "POST",
			"timeout": 0,
			"processData": false,
			"mimeType": "multipart/form-data",
			"contentType": false,
			"data": formData
		};

		/*$.ajax(settings).done(function (response) {
			$('#modal').modal('toggle')
		  console.log(response);
		});		*/

		$.ajax({
			url: "http://localhost:81/api/biodata/uploadgbr",
			method: "POST",
			timeout: 0,
			processData: false,
			mimeType: "multipart/form-data",
			contentType: false,
			data: formData,
			success: function() {
				$('#modal').modal('toggle')
				setGambaraDokter()

			}
		})
	}


	//SaveImage
	/*function saveEditImage0() {
		var formData = new FormData();
		//formData.append('image', $('#profileImage')[0].files[0])
		console.log(profileImage.images)
		console.log(profileImage)
		//Validasi Masukkan Data
		if ($('#profileImage')[0].files[0] !== undefined) {
			//Validasi Format Image
			if ($('#profileImage')[0].files[0].type.includes("image")) {
				//Validasi Ukuran Image
				if ($('#profileImage')[0].files[0].size > 150000) {
					$('#alert').html('')
					$('#alert').html('<div><i>Ukuran file lebih dari 150kb</i></div>')
					//Jika Image memenuhi kriteria, maka iamge akan di save
				} else {
					$.ajax({
						url: "http://localhost:81/api/biodata/uploadimage/"+doctorid,
						type: 'put',
						data: formData,
						enctype: 'multipart/form-data',
						contentType: false,
						processData: false,
						success: function() {
							$('#modal').modal('toggle')
							$(".profileImage").attr("src","http://localhost:81/api/biodata/uploadimage/"+doctorid)
							
						}
					})
				}
				} else {
				$('#alert').html('<div><i>File Harus dalam format Image</i></div>')
			}
		} else {
			$('#alert').html('<div><i>Anda Belum Memilih File</i></div>')
		}
	}*/


})


