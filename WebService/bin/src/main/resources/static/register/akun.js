$(document).ready(function(){
	$("#alertNl").hide()
			$("#alertNh").hide()
			$("#alertRl").hide()
			$("#alertRllocked").hide()

	optionrole()
	function optionrole(){
		$.ajax({
			url			: "http://localhost:81/api/role/lis2",
			type		: "GET",
			success		: function(hasil){
				$("#irole").append("<option value='"+0+"'>Pilih Role</option>")
				for(i=0;i<hasil.length;i++){
					$("#irole").append("<option value='"+hasil[i].id+"'>"+hasil[i].name+"</option>")
				}
			}
		})
	}
	
	$("#btndaftar").click(function(){
		$("#alertNl").hide()
		$("#alertNh").hide()
		$("#alertRl").hide()
		$("#alertRllocked").hide()
		
		var lowerCaseLetters = /[a-z]/g;
		var upperCaseLetters = /[A-Z]/g;
		var specialChars = "!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?`~";
		var hps = $("#irole").val()
		var hp = $("#iphone").val()
		var nama = $("#iname").val()
		
		var numhp=false
		for (let i = 0; i < specialChars.length; i++) {
            for (let j = 0; j < hp.length; j++) {
           		if (specialChars[i] == hp[j]) {
              		numhp = true;
             	}
           	}
        }
        var nulli=false
        firstrole()
		function firstrole(){
			if(nama=="" || nama==undefined){
			$("#alertNl").show()
		}else if(hp=="" || hp==undefined){
			//nextrole(hps)
		}else if(hp.match(lowerCaseLetters) || hp.match(upperCaseLetters) || numhp==true || hp.length<8 || hp.length>13){
			$("#alertNh").show()
		}
		}
		
		nextrole(hps)
		function nextrole(hps){
			if(hps==0){
				$("#alertRl").show()
			}else if(hps==1 || hps==2 || hps==3){
				createbiodata()
			}else{
				$("#alertRllocked").show()
			}
		}
	})
	
	//create biodata
	function createbiodata(){
		var obj={}
		obj.fullname = $("#iname").val()
		obj.mobile_phone =  $("#iphone").val()
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/boidata/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				getbiodataid()
			}
		})
	}
	
	//getting biodataid
	
	function getbiodataid(){
		var biodataid = 0;
		$.ajax({
		url			: "http://localhost:81/api/boidata/newid",
		type		: "GET",
		success		: function(hasil){
			biodataid = hasil
			createuser(biodataid)
		}
	})
	}
	
	function createuser(biodataid){
		let email = sessionStorage.getItem("email")
		let password = sessionStorage.getItem("password")
		var obj={}
		obj.biodata_id = biodataid
		obj.role_id =  $("#irole").val()
		obj.email = email
		obj.password =  password
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/user/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				createrole(biodataid, email)
			}
		})
	}
	
	function createrole(biodataid, email){
		var obj={}
		var opsi =  $("#irole").val()

		if(opsi==1){
			obj.biodata_id = biodataid
			obj.email = email
			$.ajax({
			url			: "http://localhost:81/api/admin/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				registersuccess()
			}
		})
		}else if(opsi==2){
			obj.biodata_id = biodataid
			obj.email = email
			var myJson = JSON.stringify(obj)

			$.ajax({
			url			: "http://localhost:81/api/customer/add",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				registersuccess()
			}
		})
		}else if(opsi==3){
			obj.biodata_id = biodataid
			obj.email = email
			var myJson = JSON.stringify(obj)
			$.ajax({
			url			: "http://localhost:81/api/dokter/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				registersuccess()
			}
		})
		}
	}
	
	function registersuccess(){
		$.ajax({
			url		: "register/success",
			type	: "GET",
			dataType: "html",
			success	: function(hasil){
				$(".content").html(hasil)
			}
		})	
	}
})
