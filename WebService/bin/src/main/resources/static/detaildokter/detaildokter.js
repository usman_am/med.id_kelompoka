$(document).ready(function() {
	//pura2 lohjin
	let doctorid = 3;
	localStorage.setItem("doctorid", doctorid)

	$.ajax({
		url: "http://localhost:81/api/dokter/namadokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#nama").html(hasil.fullname)

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/spesialisdokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#spesialis").html(hasil.name)

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/tindakandokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			console.log(hasil)
			for (i = 0; i < hasil.length; i++) {
				$("#tindakan").append(hasil[i].name + '<br>')
			}

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/riwayatdokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			let txt = ""
			for (i = 0; i < hasil.length; i++) {

				/*txt += "<p class='mx-4 my-0'>" + (hasil[i].namars + "," + hasil[i].namakota + "</p>")
				txt += "<p class='mx-4'>"
				txt += "<text class='mx-4' style='font-size: 13px'>" + (hasil[i].specialization + "</text>")
				txt += "</p>"*/
				
				txt += "<div class='mx-1 my-0 row'>"
				txt += "<div class='col-12 my-0>"
				txt += "<p class='namars my-0'>" + (hasil[i].namars + ", " + hasil[i].namakota + "</p>")
				txt += "</div>"
				txt += "<div class='my-0 col-12>"
				txt += "<div class='col-4'>"
				if (hasil[i].is_delete == true)
				txt += "<p style='font-size: 13px' >" + (hasil[i].specialization) + "<text class='mx-4'></text>" + (hasil[i].created + " - " + hasil[i].deleted) + "</p>"
				if (hasil[i].is_delete == false)
					txt += "<p style='font-size: 11px'>" + (hasil[i].specialization) + "<text class='mx-4'></text>" + (hasil[i].created + " - sekarang" ) + "</p>"
				txt += "</div>"
				txt += "</div>"
				txt += "</div>"

			}
			$(".tb2").append(txt)

		}
	})

	/*$.ajax({
		url: "http://localhost:81/api/dokter/tahundokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			let txt = ""
			for (i = 0; i < hasil.length; i++) {
				if (hasil[i].is_delete == true)
					txt += "<text>" + (hasil[i].created + " - " + hasil[i].deleted) + "</text>"
				if (hasil[i].is_delete == false)
					txt += "<text>" + (hasil[i].created + " - sekarang") + "<br>" + "</text>"
			}
			$(".tb3").append(txt)

		}
	})*/


	$.ajax({
		url: "http://localhost:81/api/dokter/pendidikandokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			let txt = ""
			for (i = 0; i < hasil.length; i++) {

				txt += "<div class='mx-1 my-0 row'>"
				txt += 		"<div class='col-12 my-0>"
				txt += 			"<p class='namars my-0'>" + (hasil[i].institution_name) + "</p>"
				txt += 		"</div>"
				txt += 		"<div class='my-0 col-12>"
				txt += 			"<div class='col-4'>"
				txt += 				"<p style='font-size: 13px' class='mx-3'>" + (hasil[i].major) + "<text class='mx-4'></text>" +(hasil[i].end_year) +"</p>"
				txt +=	 		"</div>"
				txt += 		"</div>"
				txt += 	"</div>"

			}
			$(".tb1").append(txt)

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/spesialisofficebyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#spesialisoffice").html(hasil.specialization)

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/pengalamanbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#pengalaman").html(hasil.date_part+" Tahun Pengalaman")

		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/hargachatbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#hargachat").html("Rp. " + hasil.nominal)

		}
	})

	$.ajax({
		url			: "http://localhost:81/api/dokter/hargakonsultasibyid/"+1,
		type 		: "GET",
		contentType : "application/json",
		success		: function(hasil) {
			for(i=0;i<hasil.length;i++) {
				$("#hargakonsultasi").append("Rp. "+hasil[i].price_start_from+'<br>')
			}
			
				
		}
	})

	$.ajax({
		url: "http://localhost:81/api/dokter/lokasipraktekbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			let txt = ""
			let b = 0

			for (i = 0; i < hasil.length; i++) {
				console.log(hasil[i])
				if (hasil[i].mfid != b) {
					txt += "<div>"
					txt += "<div>" + (hasil[i].namars) + "</div>"
					txt += "<div class='mx-4'>" + (hasil[i].kategori) + "</div>"
					txt += "<div class='mx-4'>"+ "<i class='bi bi-geo-alt-fill'></i>" + (hasil[i].full_address) + " " +hasil[i].name + " ," + hasil[i].abbreviation + "</div>"
					txt += "<a href='#hari' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle collapsed mx-4'>Sembunyikan Jadwal Praktek</a>"
					b = hasil[i].mfid
				}
				txt += "<div class='mx-4'>" + (hasil[i].day + "<text class='mx-4'></text>" +hasil[i].time_schedule_start + " - " + hasil[i].time_schedule_end) + "</div>"
				txt += "<ul id='hari' class='list-unstyled collapse'>senin</ul>"
				txt += "</div>"
			}

			$("#tlokasi").append(txt)

		}
	})
	
	$.ajax({
		url: "http://localhost:81/api/biodata/files/" + doctorid,
		type: "GET",
		dataType: "html",
		success: function(hasil) {
			$("#profileImage").attr("src", "http://localhost:81/api/biodata/files/" + doctorid)
		}
	})




})
