$(document).ready(function(){
	$("#ipswdrp").val("")
	$("#irepswdrp").val("")
	
	$("#pswHelprp").hide()
	$("#pswemptyHelprp").hide()
	$("#pswreHelprp").hide()
	$("#pswreemptyHelprp").hide()
	$("#hide_eyererp").hide()
	$("#hide_eyerp").hide()
	
	$("#show_eyerp").click(function(){
		$("#hide_eyerp").show()
		$("#show_eyerp").hide()
		$("#ipswdrp").attr("type","text")
	})
	
	$("#hide_eyerp").click(function(){
		$("#show_eyerp").show()
		$("#hide_eyerp").hide()
		$("#ipswdrp").attr("type","password")
	})
	
	$("#show_eyererp").click(function(){
		$("#hide_eyererp").show()
		$("#show_eyererp").hide()
		$("#irepswdrp").attr("type","text")
	})
	
	$("#hide_eyererp").click(function(){
		$("#show_eyererp").show()
		$("#hide_eyererp").hide()
		$("#irepswdrp").attr("type","password")
	})
		
	
	let password = "";
	let password2 = "";
	$("#btnacceptpswrp").click(function(){
		$("#pswHelprp").hide()
		$("#pswemptyHelprp").hide()
		$("#pswreHelprp").hide()
		$("#pswreemptyHelprp").hide()
		
		password = $("#ipswdrp").val();
		password2 = $("#irepswdrp").val();
		cekpassword(password, password2)
	})
	
	
	var lowerCaseLetters = /[a-z]/g;
		var upperCaseLetters = /[A-Z]/g;
		var numbers = /[0-9]/g;
		var numBoolean=false
    	var specialChars = "!@#$%^&*()-_=+[{]}\\|;:'\",<.>/?`~";
    	
	function cekpassword(password, password2){
		for (let i = 0; i < specialChars.length; i++) {
            for (let j = 0; j < password.length; j++) {
                if (specialChars[i] == password[j]) {
                    numBoolean = true;
                }
            }
        }
        if(password.length==0){
			$("#pswemptyHelprp").show()
		}else if(!(password.match(lowerCaseLetters))>0 || !(password.match(upperCaseLetters))>0 || !(password.match(numbers))>0 || numBoolean==false || password.length<8){
			$("#pswHelprp").show()
		}else if(password2.length==0){
			$("#pswreemptyHelprp").show()
		}else if(password!=password2){
			$("#pswreHelprp").show()
		}else{
			let email = sessionStorage.getItem("emailreset")
			var iduserlp = "";
			$.ajax({
				url		: "http://localhost:81/api/user/getidbyemail/"+email,
				type	: "GET",
				dataType: "html",
				success	: function(hasil){
					alert(hasil)
					iduserlp = hasil;
					resetearly(iduserlp,email,password)
				}
			})	
		}
	}
	
	function resetearly(iduserlp,email,password){
		$.ajax({
				url		: "http://localhost:81/api/user/cekpassword2/"+email+"/"+password,
				type	: "GET",
				success	: function(hasil){
					if(hasil==1){
						alert("password tidak boleh sama")
					}else{
						resetpassword(iduserlp,email,password)
					}
					//iduserlp = hasil;
					
				}
			})	
	}
	
	function resetpassword(iduserlp, email, password){
		var obj={}
			obj.password=password
			obj.modified_by=iduserlp
			obj.id=iduserlp
			obj.email=email
			var myJson = JSON.stringify(obj)
			$.ajax({
				url			: "http://localhost:81/api/user/update",
				type		: "PUT",
				contentType	: "application/json",
				data		: myJson,
				success		: function(hasil){
					$.ajax({
						url		: "resetpassword/part4",
						type	: "GET",
						dataType: "html",
						success	: function(hasil){
							$(".content5").html(hasil)
						}
					})
				}
			})
	}
	
})

