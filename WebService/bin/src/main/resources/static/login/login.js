$(document).ready(function(){
	$("#iemaillg").val("");
	$("#ipswdlg").val("");
	$("#hide_eye_lg").hide()
	$("#pswHelplg").hide()
	$("#pswemptyHelplg").hide()
	$("#emailHelplg").hide()
	$("#emailemptyHelplg").hide()
	$("#emailwrongHelplg").hide()
	$("#emaillockedHelplg").hide()
	
	$("#toresetpassword").click(function(){
		 $.ajax({
			url		: "http://localhost/resetpassword/part1",
			type	: "GET",
			dataType: "html",
			success	: function(hasil){
				$(".content5").html(hasil)
				}
		})
		return false
	})
	
	$("#toregister").click(function(){
		$.ajax({
			url		: "register",
			type	: "GET",
			dataType: "html",
			success	: function(hasil){
				$(".content").html(hasil)
			}
		})
		$("#modalemail").modal('show')
		$("#iemail").val("")
		$("#modallogin").modal('hide')
	})
	
	$("#hide_eye_lg").click(function(){
		$("#hide_eye_lg").hide()
		$("#show_eye_lg").show()
		$("#ipswdlg").attr("type","password")
	})
	
	$("#show_eye_lg").click(function(){
		$("#hide_eye_lg").show()
		$("#show_eye_lg").hide()
		$("#ipswdlg").attr("type","text")
	})
	
	$("#btnlogin").click(function(){
		$("#pswHelplg").hide()
		$("#pswemptyHelplg").hide()
		$("#emailHelplg").hide()
		$("#emailemptyHelplg").hide()
		$("#emailwrongHelplg").hide()
		$("#emaillockedHelplg").hide()
		
		var emaillogin = $("#iemaillg").val();
		var passwordlogin = $("#ipswdlg").val();
		var mail=IsEmail(emaillogin)
		
		if(emaillogin==""){
			$("#emailemptyHelplg").show()
		}else if(mail==false){
			$("#emailwrongHelplg").show()
		}else{
			$.ajax({
					url		: "http://localhost:81/api/user/cekemail/"+emaillogin,
					type	: "GET",
					success	: function(hasil){
						if(hasil.is_locked==true){$("#emaillockedHelplg").show()}
						else if(hasil.valid=="" || hasil.valid==undefined){$("#emailHelplg").show()}
						else{
							if(passwordlogin==""){
								$("#pswemptyHelplg").show()
							}else{
								$.ajax({
								url		: "http://localhost:81/api/user/cekpassword/"+emaillogin+"/"+passwordlogin,
								type	: "GET",
								success	: function(hasil){
									if(hasil>0){
										login(emaillogin, passwordlogin)
									}else{
										$("#pswHelplg").show()
										
									}
								}
							})
						}
					}
				}
			})
		}
	})
	
	function IsEmail(email) {
  		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		if(!regex.test(email)) {
    		return false;
  		}else{
    		return true;
  		}
  	}
  	
  	function login(emaillogin, passwordlogin){
		  $.ajax({
					url		: "http://localhost:80/setsession/"+emaillogin+"/"+passwordlogin,
					type	: "POST",
					success	: function(hasil){
						//$("#modallogin").modal('hide')
						//location.reload("/");
						window.location.replace("/");
					}
			})
	  }
})