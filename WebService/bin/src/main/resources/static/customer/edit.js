$(document).ready(function() {

	$(".pesan").hide()

	addgoldar()
	addRelation()
	customerById()

	let data_biodata_id = 0
	let data_customer_member_id = 0
	let data_customer_id = 0

	function customerById() {
		let id = sessionStorage.getItem("id")
		$.ajax({
			url: "http://localhost:81/api/customer/listcs/" + id,
			type: "GET",
			success: function(hasil) {
				$("#iNama").val(hasil[0].fullname)
				$("#iDob").val(hasil[0].tanggallahir)
				$("#iTB").val(hasil[0].height)
				$("#iBB").val(hasil[0].weight)
				$(".iGender").val(hasil[0].gender)
				$(".iRh").val(hasil[0].rhesus_type)
				data_biodata_id = hasil[0].biodata_id
				data_customer_member_id = hasil[0].cmid
				data_customer_id = hasil[0].id


				if (hasil[0].gender == "L") {
					$("#inlineRadio1").attr("checked", true)
				}
				else if (hasil[0].gender == "W") {
					$("#inlineRadio2").attr("checked", true)
				}

				if (hasil[0].rhesus_type == 'Rh +' || hasil[0].rhesus_type == '+') {
					$("#inlineRadio3").attr("checked", true)
				}
				else if (hasil[0].rhesus_type == 'Rh -' || hasil[0].rhesus_type == '-') {
					$("#inlineRadio4").attr("checked", true)
				}

				$(".iGol").val(hasil[0].blood_group_id)
				$(".iRel").val(hasil[0].customer_relation_id)
			}
		})
	}
	

	$("#btnSimpan").click(function() {
		let id = sessionStorage.getItem("id")

		let jk = $('input[name="jk"]:checked').val()
		let rh = $('input[name="rh"]:checked').val()
		
		var obj = {}
		obj.fullname = $("#iNama").val()
		obj.dob = $("#iDob").val()
		obj.gender = jk
		obj.blood_group_id = $(".iGol").val()
		obj.rhesus_type = rh
		obj.height = $("#iTB").val()
		obj.weight = $("#iBB").val()
		obj.customer_relation_id = $(".iRel").val()
		obj.biodata_id = data_biodata_id;
		obj.customer_member_id = data_customer_member_id;
		obj.customer_id = data_customer_id
		var myJson = JSON.stringify(obj)

		console.log(obj)
		console.log(rh)
		console.log(jk)

		$.ajax({
			url: "http://localhost:81/api/customer/editcs",
			type: "PUT",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				$("#modalCustomer").modal('hide')
				alert("simpan berhasil")
				//kembali();
			}
		})

	})

	function addgoldar() {
		$.ajax({

			url: "http://localhost:81/api/golongandarah/list",
			type: "GET",
			success: function(hasil) {
				for (i = 0; i < hasil.length; i++) {
					$(".iGol").append("<option value='" + hasil[i].id + "'>" + hasil[i].code + "</option>")
				}

			}
		})
	}

	function addRelation() {
		$.ajax({

			url: "http://localhost:81/api/hubunganpasien/list",
			type: "GET",
			success: function(hasil) {
				for (i = 0; i < hasil.length; i++) {
					$(".iRel").append("<option value='" + hasil[i].id + "'>" + hasil[i].name + "</option>")
				}

			}
		})
	}

	function kembali() {
		$.ajax({
			url: "http://localhost/customer/list",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".isimain").html(hasil)
			}
		})
	}

})