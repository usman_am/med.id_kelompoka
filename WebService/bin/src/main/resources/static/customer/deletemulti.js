$(document).ready(function(){
	
	customerById()
	
	let data_biodata_id = 0
	let data_customer_member_id = 0
	let data_customer_id = 0
	
	function customerById() {
		let id = sessionStorage.getItem("id")
		$.ajax({
			url: "http://localhost:81/api/customer/listcs/" + id,
			type: "GET",
			success: function(hasil) {
				$(".iPasien").text(hasil[0].fullname)
				data_biodata_id = hasil[0].biodata_id
				data_customer_member_id = hasil[0].cmid
				data_customer_id = hasil[0].id
			}
		})
	}
	
	$(".btnHapus").click(function(){
		let m = sessionStorage.getItem("m")
		
		var multi = m.split("")
		console.log(multi)
		
		var obj = {m}
		//obj.biodata_id = data_biodata_id;
		//obj.customer_member_id = data_customer_member_id;
		//obj.customer_id = data_customer_id
		var myJson = JSON.stringify(multi)
		
		$.ajax({
			url: "http://localhost:81/api/customer/deletemulti/",
			type: "DELETE",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				$("#modalCustomer").modal('hide')
				alert("Delete berhasil")
				//kembali();
			}
		})
	})
	
	$("#btnBatal").click(function(){
		$("#modalCustomer").modal('hide')
	})
	
})