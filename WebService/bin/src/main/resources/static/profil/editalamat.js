$(document).ready(function(){
	let biodataId = sessionStorage.getItem("biodataid")
	let id= sessionStorage.getItem("id")
	
	function kembali() {
		$.ajax({
			url: "http://localhost/tabalamat",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".isiMain").html(hasil)
			}
		})
	}
	
	alamatbyId()
	let iid=0
	function alamatbyId(){
		$.ajax({
			url:"http://localhost:81/api/tabalamat/list/"+id,
			type:"GET",
			success: function(hasil){
				console.log(hasil)
				iid=hasil.id
				$(".inLabel").val(hasil.label)
				$(".inRecip").val(hasil.recipient)
				$(".inNumRecip").val(hasil.recipientphone)
				$(".inAlamat").val(hasil.address)
			}
		})
	}
	
	$(".btnsimpan").click(function(){
		var obj={}
		obj.id=iid
		obj.biodataid=biodataId
		obj.label=$(".inLabel").val()
		obj.recipient=$(".inRecip").val()
		obj.recipientphone=$(".inNumRecip").val()
		obj.address=$(".inAlamat").val()
		var myJson = JSON.stringify(obj)
		$.ajax({
			url:"http://localhost:81/api/tabalamat/add",
			type:"POST",
			contentType:"application/json",
			data: myJson,
			success: function(hasil){
				$("#ModalAlamat").modal("hide")
				alert("simpan berhasil")
				kembali()
			}
		})
	})
	
	$(".btnbatal").click(function() {
		$("#ModalAlamat").modal("hide")
	})
})