$(document).ready(function() {
	let biodataId = sessionStorage.getItem("biodataid")

	addlocation()
    function addlocation() {
        $.ajax({
            url: "http://localhost:81/api/tabalamat/listkotakec",
            type: "GET",
            success: function(hasil) {
                for (i = 0; i < hasil.length; i++) {
                    $(".inlocationid").append("<option value='" + hasil[i].id + "'>" + hasil[i].keckota + "</option>")
                }

            }
        })
    }

	for (i = 0; i <= 6; i++) {
		$(".pesan" + i + "").hide()
	}

	$(".btnbatal").click(function() {
		$("#ModalAlamat").modal('hide')
		$(".isiMain").html(hasil)
	})

	let pesan0 = 0, pesan1 = 0, pesan2 = 0, pesan3 = 0, pesan4 = 0, pesan5 = 0, pesan6 = 0
	$(".btnsimpan").click(function() {
		let lbl = $(".inLabel").val()
		let label = lbl.toLowerCase()
		let valid1 = lbl.replace(/ /g, "")
		let valid2 = $(".inRecip").val().replace(/ /g, "")
		let valid3 = $(".inNumRecip").val().replace(/ /g, "")
		let valnum = valid3.replace(/[0-9]/g, "")
		let valid5 = $(".inAlamat").val().replace(/ /g, "")

		if (valid1.length == 0) {
			$(".pesan1").show()
			pesan1 += 1
		} else {
			$(".pesan1").hide()
			pesan1 = 0
		}

		if (valid2.length == 0) {
			$(".pesan2").show()
			pesan2 += 1
		} else {
			$(".pesan2").hide()
			pesan2 = 0
		}

		if (valid3.length == 0) {
			$(".pesan3").show()
			pesan3 += 1
		} else {
			$(".pesan3").hide()
			pesan3 = 0
			if (valnum != 0) {
				$(".pesan6").show()
				pesan6 += 1
			} else {
				$(".pesan6").hide()
				pesan6 = 0
			}
		}

		if ($(".inlocationid").val().length == 0) {
			$(".pesan4").show()
			pesan4 += 1
		} else {
			$(".pesan4").hide()
			pesan4 = 0
		}

		if (valid5.length == 0) {
			$(".pesan5").show()
			pesan5 += 1
		} else {
			$(".pesan5").hide()
			pesan5 = 0
		}

		if (pesan1 == 0 && pesan2 == 0 && pesan3 == 0 && pesan4 == 0 && pesan5 == 0, pesan6 == 0) {
			$.ajax({
				url: "http://localhost:81/api/tabalamat/search/valid/" + biodataId + "/" + label,
				type: "GET",
				contentType: "application/json",
				success: function(hasil) {
					console.log(hasil)
					if (hasil.length != 0) {
						pesan0 += 1
						$(".pesan0").show()
					} else {
						pesan0 = 0
						$(".pesan0").hide()
						var obj = {}
						obj.biodataid = biodataId
						obj.label = $(".inLabel").val()
						obj.recipient = $(".inRecip").val()
						obj.recipientphone = $(".inNumRecip").val()
						obj.locationid = $(".inlocationid").val()
						obj.postalcode = $(".inKodePos").val()
						obj.address = $(".inAlamat").val()
						var myJson = JSON.stringify(obj)
						$.ajax({
							url: "http://localhost:81/api/tabalamat/add",
							type: "POST",
							contentType: "application/json",
							data: myJson,
							success: function(hasil) {
								kembali()
							}
						})
					}
				}
			})
		} else {
			alert("Alamat tidak tersimpan!")
			return false
		}
	})

	function kembali() {
		$.ajax({
			url: "http://localhost/tabalamat",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				console.log(hasil)
				$("#ModalAlamat").modal('hide')
				$(".isiMain").html(hasil)
			}
		})
	}
})