$(document).ready(function() {

	$(".btnbatal").click(function() {
		$("#ModalAlamat").modal("hide")
	})

	let alamat = sessionStorage.getItem("muldelid")
	let idString = alamat.split(",")
	alert(idString)
	let ids = []
	for (i = 0; i < idString.length; i++) {
		ids[i] = parseInt(idString[i])
	}

	alamatbyId()
	function alamatbyId() {
		for (i = 0; i < ids.length; i++) {
			$.ajax({
				url: "http://localhost:81/api/tabalamat/list/" + ids[i],
				type: "GET",
				success: function(hasil) {
					console.log(hasil)
					$(".hpslabel").append('<h5 class="text-primary" style="padding-left: 30px">' + hasil.label + '</h5>')
				}
			})
		}
	}
 
	$(".btnhapus").click(function() {
		for (i = 0; i < ids.length; i++) {
			var obj = {}
			obj.id = ids[i]

			var myjson = JSON.stringify(obj)

			$.ajax({
				url: "http://localhost:81/api/tabalamat/delete",
				type: "DELETE",
				contentType: "application/json",
				data: myjson,
				success: function(hasil) {
				},
			})
		}
		alert("Hapus Berhasil")
		kembali()
	})

	function kembali() {
		$.ajax({
			url: "/tabalamat",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalAlamat").modal('hide')
				$(".isiMain").html(hasil)
			}
		})
	}
})