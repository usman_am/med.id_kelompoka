$(document).ready(function(){
	let biodataId = sessionStorage.getItem("biodataid")
	
	profil()
	function profil(){
		$.ajax({
			url: "http://localhost:81/api/tabprofil/list/"+biodataId,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				$(".namaLengkap").val(hasil[0].fullname)
				$(".tanggalLahir").val(hasil[0].dob)
				let a= "+62"+hasil[0].mphone
				$(".noHP").val(a)
				$(".email").val(hasil[0].email)
				$(".password").val(hasil[0].password)
			}
		})
	}
	$(".btnUbah").click(function() {
		$.ajax({
			url: "http://localhost/editprofil",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalProfil").modal('show')//modal show
				$(".isiModalProfil").html(hasil)

			}
		})
		return false
	})
	
	$(".btnUbahEmail").click(function() {
		$.ajax({
			url: "http://localhost/editemail",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalProfil").modal('show')//modal show
				$(".isiModalProfil").html(hasil)

			}
		})
		return false
	})
	
	$(".btnUbahPass").click(function() {
		$.ajax({
			url: "http://localhost/editpassword",
			type: "GET",
			datatype: "html",
			success: function(hasil) {
				$("#ModalProfil").modal('show')//modal show
				$(".isiModalProfil").html(hasil)

			}
		})
		return false
	})
})