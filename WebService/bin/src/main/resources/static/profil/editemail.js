$(document).ready(function() {
		
let biodataid = sessionStorage.getItem("biodataid")

tampil()
function tampil(){
	$.ajax({
		url: "http://localhost:81/api/tabprofil/listprofil/" + biodataid,
		type: "GET",
		success: function(hasil) {
			
				$(".iemail").val(hasil[0].email)
		}
	})
}


	$("#iemail").val("")
	//$("#iotp").val("")
	$("#emailterdaftar").hide()
	$("#emailKosong").hide()
	$("#emailSalah").hide()

	//cek bentuk email sudah benar atau belum
	function IsEmail(email) {
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!regex.test(email)) {
			return false;
		} else {
			return true;
		}
	}

	//check email apakah sudah terdaftar / user salah menginputkan email
	let email = ""

	$(".btnSimpan").click(function() {
		$("#emailterdaftar").hide()
		$("#emailKosong").hide()
		$("#emailSalah").hide()

		email = $("#iemail").val();
		if (email == "") {
			$("#emailKosong").show()
		} else {
			var mail = IsEmail(email)
			if (mail == false) {
				$("#emailSalah").show()
			} else {
				$.ajax({
					url: "http://localhost:81/api/user/cekemail/" + email,
					type: "GET",
					success: function(hasil) {
						if (hasil != 0) {
							$("#emailHelp").show()
						} else {
							$.ajax({
								url: "profile/otp",
								type: "GET",
								dataType: "html",
								success: function(hasil) {
									$("#ModalProfile").modal('show')//modal show
									$(".isiModalProfile").html(hasil)
									
									newtoken(email)
								}
							})

						}
					}
				})
			}
		}
	})

	function newtoken(email) {
		//buat token
		var idtoken = 0;
		var obj = {}
		obj.email = email
		var myJson = JSON.stringify(obj)
		$.ajax({
			url: "http://localhost:81/api/token/post",
			type: "POST",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				$.ajax({
					url: "http://localhost:81/api/token/gettoken/" + email,
					type: "GET",
					success: function(hasil) {
						idtoken = hasil
						sessionStorage.setItem("idtoken", idtoken)
						sessionStorage.setItem("email", email)

					}
				})
			}
		})
	}

function kembali() {
	$.ajax({
		url: "http://localhost/tabprofil",
		type: "GET",
		dataType: "html",
		success: function(hasil) {
			$("#ModalProfil").modal('hide')
		}
	})
	
	
}

$(".btnKembali").click(function() {
	kembali()
	})
})




