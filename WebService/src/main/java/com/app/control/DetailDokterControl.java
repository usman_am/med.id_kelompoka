package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DetailDokterControl {
	
	@GetMapping("detaildokter")
	public String detailDokter() {
		return "detaildokter/detaildokter";
	}

}
