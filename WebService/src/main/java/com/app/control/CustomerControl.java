package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerControl {

	@GetMapping("listcustomer")
	public String list() {
		return "/customer/customer";
	}
	
	@GetMapping("addcustomer")
	public String addCustomer() {
		return "/customer/add";
	}
	
	@GetMapping("editcustomer")
	public String updateCustomer() {
		return "/customer/edit";
	}
	
	@GetMapping("deletecustomer")
	public String deleteCustomer() {	
		return "/customer/delete";
	}
	
	@GetMapping("deletemulticustomer")
	public String deleteMultiCustomer() {	
		return "/customer/deletemulti";
	}
}
