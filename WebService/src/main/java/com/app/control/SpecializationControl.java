package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SpecializationControl {
	
	@GetMapping("specialization")
	public String specialization() {
		return "specialization/specialization";
	}
	
	@GetMapping("addspecialization")
	public String addSpecialization() {
		return "specialization/add";
	}
	
	@GetMapping("editspecialization")
	public String editSpecialization() {
		return "specialization/edit";
	}
	
	@GetMapping("deletespecialization")
	public String deleteSpecialization() {	
		return "specialization/delete";
	}
	
	@GetMapping("button")
	public String button() {	
		return "dokter/button";
	}
	
	


}
