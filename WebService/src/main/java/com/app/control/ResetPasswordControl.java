package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ResetPasswordControl {

	@GetMapping("resetpassword/part1")
	public String resetpassword() {
		return "resetpassword/resetpassword";
	}
	
	@GetMapping("resetpassword/part2")
	public String resetpassword2() {
		return "resetpassword/resetpassword2";
	}
	
	@GetMapping("resetpassword/part3")
	public String resetpassword3() {
		return "resetpassword/resetpassword3";
	}
	
	@GetMapping("resetpassword/part4")
	public String resetpassword4() {
		return "resetpassword/resetpassword4";
	}
}
