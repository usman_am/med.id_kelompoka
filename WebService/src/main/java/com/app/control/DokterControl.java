package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DokterControl {
	
	@GetMapping("dokter")
	public String dokter() {
		return "dokter/dokter";
	}
	
	@GetMapping("adddokter")
	public String add() {
		return "dokter/add";
	}
	
	@GetMapping("editdokter")
	public String edit() {
		return "dokter/edit";
	}
	
	@GetMapping("tabtindakan")
	public String tabtindakan() {
		return "dokter/tabtindakan";
	}
	
	@GetMapping("tambahtindakan")
	public String tambahtindakan() {
		return "dokter/tambahtindakan";
	}
	
	@GetMapping("hapustindakan")
	public String hapustindakan() {
		return "dokter/hapustindakan";
	}

}
