package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdminControl {

	@GetMapping("admin/role")
	public String role() {
		return "role/role";
	}
	
	@GetMapping("admin/role/add")
	public String roleAdd() {
		return "role/add";
	}
	
	@GetMapping("admin/role/edit")
	public String roleEdit() {
		return "role/edit";
	}
	
	@GetMapping("admin/role/delete")
	public String roleDelete() {
		return "role/delete";
	}
}
