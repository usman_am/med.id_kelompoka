package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;


@Controller
@CrossOrigin(origins = "*")
public class LoginControl {

	@GetMapping("/")
	public String dashboard(HttpSession session, Model m) {
		Object userlogin = session.getAttribute("userlogin");
//		if(userlogin==null) {
//			return "dashboard";
//		}else {
//			return "dashboard";
//		}
		m.addAttribute("hEmail", userlogin);
		return "dashboard";
	}
	
	@GetMapping("login")
	public String registration() {
		return "login/login";
	}
	
	@GetMapping("ceksession")
	public String ceksession(HttpSession session) {
		Object userlogin = session.getAttribute("userlogin");
		if(userlogin==null)userlogin=new String();
		return "redirect:/";

	}
	
	//session login
	@PostMapping("setsession/{em}/{pw}")
	public String setsession(@PathVariable String em,
			@PathVariable String pw,
			HttpServletRequest request) {
		request.getSession().setAttribute("userlogin", em);
		return "redirect:/";
	}
	
	//session logout
	@GetMapping("logout")
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "redirect:/";
	}
	
	
}
