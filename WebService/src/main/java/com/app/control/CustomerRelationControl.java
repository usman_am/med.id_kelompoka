package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerRelationControl {

	@GetMapping("listhubunganpasien")
	public String list() {
		return "/hubunganpasien/hubunganpasien";
	}
	
	@GetMapping("addhubunganpasien")
	public String addHubunganPasien() {
		return "/hubunganpasien/add";
	}
	
	@GetMapping("edithubunganpasien")
	public String updateHubunganPasien() {
		return "/hubunganpasien/edit";
	}
	
	@GetMapping("deletehubunganpasien")
	public String deleteHubunganPasien() {	
		return "/hubunganpasien/delete";
	}
	
}
