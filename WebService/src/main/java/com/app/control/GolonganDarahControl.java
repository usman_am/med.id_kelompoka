package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GolonganDarahControl {

	@GetMapping("listgolongandarah")
	public String list() {
		return "/golongandarah/golongandarah";
	}
	
	@GetMapping("addgolongandarah")
	public String addGolonganDarah() {
		return "/golongandarah/add";
	}
	
	@GetMapping("editgolongandarah")
	public String updateGolonganDarah() {
		return "/golongandarah/edit";
	}
	
	@GetMapping("deletegolongandarah")
	public String deleteVariants() {	
		return "/golongandarah/delete";
	}
	
}
