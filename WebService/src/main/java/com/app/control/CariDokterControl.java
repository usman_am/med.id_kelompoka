package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CariDokterControl {

	@GetMapping("caridokter")
	public String cariDokter() {
		return "/caridokter/caridokter";
	}
}
