package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RegisterControl {

	@GetMapping("register")
	public String registration() {
		return "register/register";
	}
	
	@GetMapping("register/otp")
	public String otp() {
		return "register/otp";
	}
	
	@GetMapping("register/password")
	public String password() {
		return "register/password";
	}
	
	@GetMapping("register/akun")
	public String akun() {
		return "register/akun";
	}

	@GetMapping("register/success")
	public String success() {
		return "register/success";
	}
}
