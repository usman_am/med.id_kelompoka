package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class JanjiControl {

	@GetMapping("janji")
	public String janji() {
		return "janji/janji";
	}
	
	@GetMapping("tambahjanji")
	public String tambahjanji() {
		return "janji/tambahjanji";
	}
	
	@GetMapping("cekjanji")
	public String cekjanji() {
		return "janji/cekjanji";
	}
}
