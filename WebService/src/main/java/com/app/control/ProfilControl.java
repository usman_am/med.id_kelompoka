package com.app.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ProfilControl {
	
	@GetMapping("profil")
	public String profil() {
		return "profil/profil";
	}
	
	@GetMapping("tabprofil")
	public String tabprofil() {
		return "profil/tabprofil";
	}
	
	@GetMapping("tabalamat")
	public String tabalamat() {
		return "profil/tabalamat";
	}
	
	@GetMapping("tambahalamat")
	public String tambahalamat() {
		return "profil/tambahalamat";
	}
	
	@GetMapping("hapusalamat")
	public String hapusalamat() {
		return "profil/hapusalamat";
	}
	
	@GetMapping("editalamat")
	public String editalamat() {
		return "profil/editalamat";
	}
	
	@GetMapping("editprofil")
	public String editprofil() {
		return "profil/editprofil";
	}
	
	@GetMapping("editemail")
	public String editemail() {
		return "profil/editemail";
	}
	
	@GetMapping("editpassword")
	public String editpassword() {
		return "profil/editpassword";
	}
	
	@GetMapping("gambarprofil")
	public String gambarprofil() {
		return "profil/gambarprofil";
	}
}

