$(document).ready(function(){
	
	$(".pesan").hide()
	
	function golonganDarahById(){
		let id = sessionStorage.getItem("id")
		$.ajax({
			url : "http://localhost:81/api/golongandarah/list/"+id,
			type : "GET",
			success : function(hasil){
				$("#inKode").val(hasil.code)
				$("#inDeskripsi").val(hasil.description)
			}
		})
	}
	
	golonganDarahById()
	
	$("#btnSimpan").click(function(){
		let id = sessionStorage.getItem("id")
		
		if ($('.kode').val().length == 0 || $('.kode').val() == undefined || $('.kode').val() == " ") {
			$(".pesan").show()
			return false
		} else {
			var obj = {}
			obj.id = id
			obj.code = $("#inKode").val()
			obj.description = $("#inDeskripsi").val()

			var myjson = JSON.stringify(obj)

			$.ajax({
				url: "http://localhost:81/api/golongandarah/edit",
				type: "PUT",
				contentType: "application/json",
				data: myjson,
				success: function(hasil) {
					$("#modalGolonganDarah").modal('hide')
					alert("Edit Berhasil")
					//kembali()
				}
			})
		}
				
	})
	
	$("#btnBatal").click(function(){
		$("#modalGolonganDarah").modal('hide')
	})
	
})