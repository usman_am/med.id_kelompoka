$(document).ready(function(){
	
		$("#iemail").val("")
		$("#emailHelp").hide()
		$("#emailemptyHelp").hide()
		$("#emailwrongHelp").hide()
		
	function IsEmail(email) {
  		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		if(!regex.test(email)) {
    		return false;
  		}else{
    		return true;
  		}
  	}
	
	$("#btnotp").click(function(){
		$("#emailHelp").hide()
		$("#emailemptyHelp").hide()
		$("#emailwrongHelp").hide()
		
		let email = $("#iemail").val();
		if(email==""){
			$("#emailemptyHelp").show()
		}else{
			var mail=IsEmail(email)
			if(mail==false){
				$("#emailwrongHelp").show()
			}else{
				$.ajax({
					url		: "http://localhost:81/api/user/cekemail/"+email,
					type	: "GET",
					success	: function(hasil){
						if(hasil.valid!=undefined){
							$("#emailHelp").show()
						}else{
							buattoken(email)
							$.ajax({
							url		: "register/otp",
							type	: "GET",
							dataType: "html",
							success	: function(hasil){
								$(".content").html(hasil)
							}
							})
							return false
						}
					}
				})
			}
		}
	})
	
	function buattoken(email){
		//buat token
		var obj={}
		obj.email = email
		obj.used_for = "user create account"
		var myJson = JSON.stringify(obj)
		$.ajax({
		url			: "http://localhost:81/api/token/post",
		type		: "POST",
		contentType	: "application/json",
		data		: myJson,
		success		: function(hasil){
			gettoken(email)
		}})
	}
	
	var idtoken = 0;
	function gettoken(email){
		$.ajax({
		url			: "http://localhost:81/api/token/gettoken/"+email,
		type		: "GET",
		success		: function(hasil){
			idtoken = hasil
			sessionStorage.setItem("idtoken",idtoken)
			sessionStorage.setItem("email",email)
		}
	})
	}
})




