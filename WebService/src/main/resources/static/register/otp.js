$(document).ready(function(){
	$("#otpHelp").hide()
	$("#otpspoilHelp").hide()
	$("#btnresendotp").hide()
	$("#returntoemailver").hide()
	$("#otpemptyHelp").hide()
	
		var minute = 0
 		var second = 60
 		
		const intervalID = setInterval(loopcount, 1000)
		//gunakan email lain
		$("#returntoemailver").click(function(){
			clearInterval(intervalID)
			$.ajax({
			url		: "register",
			type	: "GET",
			dataType: "html",
			success	: function(hasil){
				$(".content").html(hasil)
			}
		})
		})
		
		$("#btnx").click(function(){
			minute = 0
			second = 60
			clearInterval(intervalID)
		})
		
		$("#btnmodaldaftar").click(function(){
			minute = 0
			second = 60
			//clearInterval(intervalID)
			$("#count").show()
		})
		
		$("#btnotp").click(function(){
			minute = 0
			second = 60
			//clearInterval(intervalID)
			$("#count").show()
		})
		
		$("#btnresendotp").click(function(){
			minute = 0
			second = 60
			$("#count").show()
			$("#btnresendotp").hide()
			//setInterval(loopcount, 1000)
		})
		
 		function loopcount(){
			
	 		if(minute==0 && second == 1){
				 $("#returntoemailver").show()
				 $("#btnresendotp").show()
				 $("#count").hide()
	 		}else{
		 		second--;
		 		if(second==0){
			 		minute--;
			 		second=60;
			 		if(minute==0){
				 		minute=minute;
			 		}
		 		}
		 		if(minute.toString().length == 1){
					 minute = "0"+minute;
				 }
				 if(second.toString().length == 1){
					 second = "0"+second;
				 }
		 		document.getElementById("count").innerHTML = "Kirim ulang kode OTP dalam "+minute+":"+second;
	 		}
 		}
 		
	
	$("#btnacceptotp").click(function(){
		$("#ipswd").val("");
		$("#irepswd").val("");
		$("#otpspoilHelp").hide()
		$("#otpHelp").hide()
		$("#otpemptyHelp").hide()
		
		let token = $("#iotp").val()
		if(token=="" || token==undefined){
			$("#otpemptyHelp").show()
		}else{
			cekvalid(token)
		}
	})
	
	function cekvalid(token){
		let idtoken = sessionStorage.getItem("idtoken")
		$.ajax({
				url		: "http://localhost:81/api/token/cekvalid/"+idtoken,
				type	: "GET",
				success	: function(hasil){
					if(hasil<=0){
						$("#otpspoilHelp").show()
					}else{
						cektoken(token, idtoken)
					}
				}
			})
	}
	
	function cektoken(token, idtoken){
		$.ajax({
				url		: "http://localhost:81/api/token/cektoken/"+idtoken+"/"+token,
				type	: "GET",
				success	: function(hasil){
					if(hasil<=0){
						$("#otpHelp").show()
					}else{
						$.ajax({
							url		: "register/password",
							type	: "GET",
							dataType: "html",
							success	: function(hasil){
								$(".content").html(hasil)
							}
							})
						let email = sessionStorage.getItem("email")
						$.ajax({
							url			: "http://localhost:81/api/token/setexpired/"+email,
							type		: "PUT",
							success		: function(hasil){
							}
						})	
						clearInterval(intervalID) 				
					}
				}
			})
	}
	
	//kirim ulang otp
		$("#btnresendotp").click(function(){
			deactiveotplama()
		})
		
	function deactiveotplama(){
		let email = sessionStorage.getItem("email")
		$.ajax({
			url			: "http://localhost:81/api/token/setexpired/"+email,
			type		: "PUT",
			success		: function(hasil){
				let email = sessionStorage.getItem("email")
				buattoken(email)
			}
		})
	}
	
	function buattoken(email){
		//buat token
		var obj={}
		obj.email = email
		obj.used_for = "account creation"
		var myJson = JSON.stringify(obj)
		$.ajax({
		url			: "http://localhost:81/api/token/post",
		type		: "POST",
		contentType	: "application/json",
		data		: myJson,
		success		: function(hasil){
			gettoken(email)
		}})
	}
	
	var idtoken = 0;
	function gettoken(email){
		$.ajax({
		url			: "http://localhost:81/api/token/gettoken/"+email,
		type		: "GET",
		success		: function(hasil){
			idtoken = hasil
			sessionStorage.setItem("idtoken",idtoken)
			sessionStorage.setItem("email",email)
		}
	})
	}

	$("#btnx").click(function(){
		let email = sessionStorage.getItem("email")
		$.ajax({
			url			: "http://localhost:81/api/token/setexpired/"+email,
			type		: "PUT",
			success		: function(hasil){
			}
		})
	})
})
