$(document).ready(function() {

	let doctorid = 3;
	localStorage.setItem("doctorid", doctorid)


	//--------------- Tampilan Nama Dokter
	$.ajax({
		url: "http://localhost:81/api/dokter/namadokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#nama").html(hasil.fullname)

		}
	})

	//--------------- Tampilan Spesialis Dokter
	$.ajax({
		url: "http://localhost:81/api/dokter/spesialisdokterbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#spesialis").html(hasil.name)

		}
	})

	//--------------- Tampilan Pengalaman Dokter
	$.ajax({
		url: "http://localhost:81/api/dokter/pengalamanbyid/" + doctorid,
		type: "GET",
		contentType: "application/json",
		success: function(hasil) {
			$("#pengalaman").html(hasil.date_part + " Tahun Pengalaman")

		}
	})
	
	//--------------- Buat Janji
	
		
})