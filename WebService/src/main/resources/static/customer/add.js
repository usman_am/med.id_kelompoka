$(document).ready(function(){
	
	$(".pesan").hide()
	
	addgoldar()
	addRelation()
	
	function addgoldar() {
		$.ajax({

			url: "http://localhost:81/api/golongandarah/list",
			type: "GET",
			success: function(hasil) {
				for (i = 0; i < hasil.length; i++) {
					$(".iGol").append("<option value='" + hasil[i].id + "'>" + hasil[i].code + "</option>")
				}

			}
		})
	}
	
	function addRelation() {
		$.ajax({

			url: "http://localhost:81/api/hubunganpasien/list",
			type: "GET",
			success: function(hasil) {
				for (i = 0; i < hasil.length; i++) {
					$(".iRel").append("<option value='" + hasil[i].id + "'>" + hasil[i].name + "</option>")
				}

			}
		})
	}
	
	$("#btnSimpan").click(function() {
        let jk=$('input[name="jk"]:checked').val()
        let rh=$('input[name="rh"]:checked').val()

        var obj = {}

        obj.fullname = $("#iNama").val()
        obj.dob = $("#iDob").val()
        obj.gender= jk
        obj.bloodGroupId=$(".iGol").val()
        obj.rhesusType= rh
        obj.height=$("#iTB").val()
        obj.weight=$("#iBB").val()
        obj.customerRelationId=$(".iRel").val()
        var myJson = JSON.stringify(obj)
        $.ajax({
            url: "http://localhost:81/api/customer/addcs",
            type: "POST",
            contentType: "application/json",
            data: myJson,
            success: function(hasil) {
                $("#modalHubungan").modal('hide')
                alert("simpan berhasil")
                //kembali();
            }
        })

    })
	
	//$("#btnBatal").click(function)
})