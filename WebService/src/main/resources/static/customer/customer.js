$(document).ready(function() {

	$(".thetable").DataTable({//buat datatable
		dom: '<lf<t>ip>', //turunin posisi entries
		searching: false,
		paging: true,
		info: false,
		"language": {
			"paginate": {
				"first": ">",
				"last": "<",
				"next": ">",
				"previous": "<"
			}
		}, //rename next back

		pageLength: 3, //cutome nilai paging
		lengthMenu: [[3, 5, 7, -1], [3, 5, 7, 'Todos']]
	});//hapus cari	

	list()

	function list(isi){
		let alamat = ""
		if(isi == undefined || isi == "") {
			alamat = "http://localhost:81/api/customer/listcs"
		} else {
			alamat = "http://localhost:81/api/customer/listbyname/"+isi
		}
		
		$.ajax({
			url: alamat,
			type: "GET",
			success:function(hasil){
				console.log(hasil)
				let txt = ""
				for(i=0; i<hasil.length; i++){
					txt += "<tr>"
					txt += "<td><input type='checkbox' class='multidelete' name='"+hasil[i].id+"'></td>"
					txt += "<td><p class='namaPasien'>"+hasil[i].fullname+"</p><p class='relasiPasien'>"+hasil[i].name+", "+hasil[i].usia+" Tahun</p><p class='statePasien'>n Chat Online, n Janji Dokter</p></td>"
					txt += "<td><button class='btnUbah btn btn-warning' name='"+hasil[i].id+"'>Ubah</button>   <button class='btnHapus btn btn-danger' name='"+hasil[i].id+"'>Hapus</button></td>"
					txt += "</tr>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)
				
				$(".btnUbah").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/editcustomer",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							//window.open("http://localhost/editgolongandarah")
							$("#modalCustomer").modal('show')
							$(".isiModal").html(hasil)
						}
					})
					//window.location.reload(true);
					return false
				})
				
				$(".btnHapus").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/deletecustomer",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							//window.open("http://localhost/deletegolongandarah")
							$("#modalCustomer").modal('show')
							$(".isiModal").html(hasil)
						}
					})
					return false
				})
				
				let m = ""
				$(".multidelete").click(function() {
					m = ""
					$("input[class='multidelete']:checked").each(function() {
						m += this.name
						//m += ","
					})
					//m = m.substr(0, m.length - 1);
					alert(m)
				})
				
				$("#btnHapus").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					sessionStorage.setItem("m", m)
					$.ajax({
						url : "http://localhost/deletemulticustomer",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							
							//window.open("http://localhost/deletegolongandarah")
							$("#modalCustomer").modal('show')
							$(".isiModal").html(hasil)
						}
					})
					return false
				})
			}
		})
	}
	
	$("#iSearch").keyup(function(){
		let cari = $(this).val()
		list(cari)
	})
	
	$("#btnTambah").click(function(){
		$.ajax({
			url: "http://localhost/addcustomer",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				//window.open("http://localhost/editgolongandarah")
				$("#modalCustomer").modal('show')
				$(".isiModal").html(hasil)
			}
		})
		//window.location.reload(true);
		return false
	})

})