$(document).ready(function(){
	$("#emailresetpassword").val("");
	$("#rpemailHelp").hide()
	$("#rpemailemptyHelp").hide()
	$("#rpemailwrongHelp").hide()

	function IsEmail(email) {
  		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  		if(!regex.test(email)) {
    		return false;
  		}else{
    		return true;
  		}
  	}

	let email = ""
	$("#btnotplp").click(function(){
		$("#rpemailHelp").hide()
		$("#rpemailemptyHelp").hide()
		$("#rpemailwrongHelp").hide()
		
		email = $("#iemailresetpassword").val();
		if(email==""){
			$("#rpemailemptyHelp").show()
		}else{
			var mail=IsEmail(email)
			if(mail==false){
				$("#rpemailwrongHelp").show()
			}else{
				$.ajax({
					url		: "http://localhost:81/api/user/cekemail/"+email,
					type	: "GET",
					success	: function(hasil){
						if(hasil.valid=="" || hasil.valid==undefined){
							$("#rpemailHelp").show()
						}else{
							sessionStorage.setItem("emailreset",email)
							$.ajax({
								url		: "http://localhost/resetpassword/part2",
								type	: "GET",
								dataType: "html",
								success	: function(hasil){
									$(".content5").html(hasil)
								}
							})
							return false
						}
					}
				})
			}
		}
	})
})