$(document).ready(function(){
	$("#otpHelprp").hide()
	$("#otpspoilHelprp").hide()
	$("#btnresendotprp").hide()
	$("#otpemptyHelprp").hide()
	
	buattokenrp()
	function buattokenrp(){
		let emailreset = sessionStorage.getItem("emailreset")
		$.ajax({
			url		: "http://localhost:81/api/user/getidbyemail/"+emailreset,
			type	: "GET",
			success	: function(hasil){
				var idreset = hasil
				sessionStorage.setItem("idreset",idreset)
				buattoken2(idreset)
			}
		})
	}
	
	function buattoken2(idreset){
		let emailreset = sessionStorage.getItem("emailreset")
		var obj={}
		obj.email = emailreset
		obj.user_id = idreset
		obj.used_for = "change password"
		obj.created_by = idreset
		var myJson = JSON.stringify(obj)
		$.ajax({
		url			: "http://localhost:81/api/token/post",
		type		: "POST",
		contentType	: "application/json",
		data		: myJson,
		success		: function(hasil){
			gettokenrp(emailreset)
		}})
	}
	
	
		var minute = 0
 		var second = 60
 		
		const intervalID = setInterval(loopcount, 1000)
		
		$("#btnx").click(function(){
			minute = 0
			second = 60
			clearInterval(intervalID)
		})
		
		$("#btnresendotprp").click(function(){
			minute = 0
			second = 60
		})
		
 		function loopcount(){
	 		if(minute==0 && second == 1){
				 $("#btnresendotprp").show()
				 $("#countrp").hide()
	 		}else{
		 		second--;
		 		if(second==0){
			 		minute--;
			 		second=60;
			 		if(minute==0){
				 		minute=minute;
			 		}
		 		}
		 		if(minute.toString().length == 1){
					 minute = "0"+minute;
				 }
				 if(second.toString().length == 1){
					 second = "0"+second;
				 }
		 		document.getElementById("countrp").innerHTML = "Kirim ulang kode OTP dalam "+minute+":"+second;
	 		}
 		}
 		
	$("#btnacceptotprp").click(function(){
		$("#otpspoilHelprp").hide()
		$("#otpHelprp").hide()
		$("#otpemptyHelprp").hide()
		
		let tokenreset = $("#iotprp").val()
		if(tokenreset=="" || tokenreset==undefined){
			$("#otpemptyHelprp").show()
		}else{
			cekvalid(tokenreset)
		}
	})
	
	function cekvalid(tokenreset){
		let idtokenrp = sessionStorage.getItem("idtokenrp")
		$.ajax({
				url		: "http://localhost:81/api/token/cekvalid/"+idtokenrp,
				type	: "GET",
				success	: function(hasil){
					if(hasil<=0){
						$("#otpspoilHelprp").show()
					}else{
						cektoken(tokenreset, idtokenrp)
					}
				}
			})
	}
	
	function cektoken(tokenrp, idtokenrp){
		$.ajax({
				url		: "http://localhost:81/api/token/cektoken/"+idtokenrp+"/"+tokenrp,
				type	: "GET",
				success	: function(hasil){
					if(hasil<=0){
						$("#otpHelprp").show()
					}else{
						clearInterval(intervalID)
						$.ajax({		
							url		: "http://localhost/resetpassword/part3",
							type	: "GET",
							dataType: "html",
							success	: function(hasil){
								$(".content5").html(hasil)
								deactiveotplama()
							}
						})
						return false			
					}
				}
			})
	}
	
	$("#btnresendotprp").click(function(){
		$("#btnresendotprp").hide()
		$("#countrp").show()
		deactiveotplama()
		buattoken()
	})
		
	function deactiveotplama(){
		let email = sessionStorage.getItem("emailreset")
		$.ajax({
			url			: "http://localhost:81/api/token/setexpired/"+email,
			type		: "PUT",
			success		: function(hasil){
			}
		})
	}
	
	function buattoken(){
		let emailreset = sessionStorage.getItem("emailreset")
		$.ajax({
					url		: "http://localhost:81/api/user/getidbyemail/"+emailreset,
					type	: "GET",
					success	: function(hasil){
						var idreset = hasil
						sessionStorage.setItem("idreset",idreset)
						buattoken2(idreset)
					}
		})
		//buat token
	}
	
	var idtokenrp = 0;
	function gettokenrp(emailreset){
		$.ajax({
		url			: "http://localhost:81/api/token/gettoken/"+emailreset,
		type		: "GET",
		success		: function(hasil){
			idtokenrp = hasil
			sessionStorage.setItem("idtokenrp",idtokenrp)
		}
	})
	}

	$("#btnx").click(function(){
		let email = sessionStorage.getItem("emailreset")
		$.ajax({
			url			: "http://localhost:81/api/token/setexpired/"+email,
			type		: "PUT",
			success		: function(hasil){
			}
		})
		})
})
