$(document).ready(function() {

	gambarProfilById()
	function gambarProfilById() {
		let id = localStorage.getItem("biodataid")
		$.ajax({
			url: "http://localhost:81/api/profil/files/" + id,
			type: "GET",
			success: function(hasil) {
				$(".isiGambarProfil").val(hasil.uploadFile)
			}

		})
	}

	$("#btnSimpan").click(function() {
		var obj = {}
		obj.biodataId = localStorage.getItem("biodataid")
		var myJson = JSON.stringify(obj)
		$.ajax({
			url: "http://localhost:81/api/profil/uploadgbr",
			type: "PUT",
			contentType: "application/json",
			data: myJson,
			success: function(hasil) {
				alert("Gambar berhasil diubah")

			}
		})
	})

	$("#btnBatal").click(function() {
		$("#modalGambarProfil").modal('hide')
	})
})