$(document).ready(function() {
	let biodataId = sessionStorage.getItem("biodataid")

	$(".sortingby").append('<option value="label">Label Alamat</option>')
	$(".sortingby").append('<option value="recipient">Nama Penerima</option>')

	$('.urut').click(function() {
		if ($(this).text() == 'A-Z') {
			$(this).val('desc');
			$(this).text('Z-A');

		} else {
			$(this).val('asc');
			$(this).text('A-Z');
		}
		alert(az)
	});

	$(".sortingby").change(function() {
		
	})

	$(".search").keyup(function() {
		let cari = $(this).val()
		list(cari)
	})

	function kembali() {
		$.ajax({
			url: "/tabalamat",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".ModalAlamat").modal('hide')
				$(".isiMain").html(hasil)
			}
		})
	}

	function list(isi) {
		let alamat = ""
		if (isi == undefined || isi.replace(/ /g, "").length == 0) {
			alamat = "http://localhost:81/api/tabalamat/listalamat/" + biodataId
		/*} else if (isi != undefined && az % 2 == 0 && lp % 2 == 0) {
			alamat = "http://localhost:81/api/tabalamat/search/Lasc/" + biodataId + "/" + isi
			alert("Lasc")
		} else if (az % 2 != 0 && lp % 2 == 0) {
			alamat = "http://localhost:81/api/tabalamat/search/Ldesc/" + biodataId + "/" + isi
			alert("Ldesc")
		} else if (az % 2 == 0 && lp % 2 != 0) {
			alamat = "http://localhost:81/api/tabalamat/search/Rasc/" + biodataId + "/" + isi
			alert("Rasc")
		} else if (az % 2 != 0 && lp % 2 != 0) {
			alamat = "http://localhost:81/api/tabalamat/search/Rdesc/" + biodataId + "/" + isi
			alert("Rdesc")*/
		} else {
			alamat = "http://localhost:81/api/tabalamat/search/Lasc/" + biodataId + "/" + isi
		}
		$.ajax({
			url: alamat,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<tr>"
					txt += "<td style='width:10%;text-align: center'><br> <input type='checkbox' class='muldel' name=" + hasil[i].id + "></td>"
					txt += "<td style='width:60%'><h5 class='text-primary'>" + hasil[i].label + "</h5>"
					txt += "" + hasil[i].recipient + ", "
					txt += "" + hasil[i].recipientphone + "<br>"
					txt += "" + hasil[i].postalcode + ", "
					txt += "" + hasil[i].address + "</td>"
					txt += "<td style='text-align: right;width:30% '><br><input type='button' value='Ubah' class='btnedit btn btn-warning btn-sm' name=" + hasil[i].id + ">"
					txt += " <input type='button' value='Hapus' class='btndelete btn btn-danger btn-sm' name=" + hasil[i].id + "></td>"
					txt += "</tr>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)

				$(".btnedit").click(function() {
					let id = $(this).attr("name")
					sessionStorage.setItem("id", id)
					$.ajax({
						url: "http://localhost/editalamat",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#ModalAlamat").modal("show")
							$(".isiModalAlamat").html(hasil)
						}
					})
					return false
				})
				$(".btndelete").click(function() {
					let id = $(this).attr("name")
					sessionStorage.setItem("muldelid", id)
					$.ajax({
						url: "http://localhost/hapusalamat",
						type: "Get",
						dataType: "html",
						success: function(hasil) {
							$("#ModalAlamat").modal("show")
							$(".isiModalAlamat").html(hasil)
						}
					})
					return false
				})

				$(".muldel").click(function() {
					if ($('.muldel').is(':checked') == true) {
						$(".multidelete").removeAttr("disabled")
					}
					else if ($('.checkPasien').is(':checked') == false) {
						$(".multidelete").attr("disabled", "disabled")
					}
				})
			}
		})
	}

	let ids = ""
	$(".multidelete").click(function() {
		ids = ""
		$("input[class='muldel']:checked").each(function() {
			ids += this.name
			ids += ","
		})
		ids=ids.substr(0,ids.length-1)
		sessionStorage.setItem("muldelid", ids)
		$.ajax({
			url: "http://localhost/hapusalamat",
			type: "Get",
			dataType: "html",
			success: function(hasil) {
				$("#ModalAlamat").modal("show")
				$(".isiModalAlamat").html(hasil)
			}
		})
		return false
	})

	$(".tambah").click(function() {
		$.ajax({
			url: "http://localhost/tambahalamat",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#ModalAlamat").modal("show")
				$(".isiModalAlamat").html(hasil)
			}
		})
		return false
	})

	list()
})