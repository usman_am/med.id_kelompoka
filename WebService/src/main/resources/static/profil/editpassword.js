$(document).ready(function() {

	let id= sessionStorage.getItem("id")
	
	function kembali() {
		$.ajax({
			url: "http://localhost/tabprofil",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$(".ModalProfil").modal('hide')
				$(".isiMain").html(hasil)
			}
		})
	}
	
	profilbyId()
	let iid=0
	function profilbyId(){
		$.ajax({
			url:"http://localhost:81/api/profil/list/"+id,
			type:"GET",
			success: function(hasil){
				console.log(hasil)
				iid=hasil.id
				$(".namaLengkap").val(hasil.fullname)
				$(".tanggalLahir").val(hasil.dob)
				$(".noHp").val(hasil.mobilephone)
			}
		})
	}
	
	$(".btnsimpan").click(function(){
		var obj={}
		obj.id=iid
		obj.fullname=$(".namaLengkap").val()
		obj.dob=$(".tanggalLahir").val()
		obj.mobilephone=$(".noHp").val()
		
		var myJson = JSON.stringify(obj)
		$.ajax({
			url:"http://localhost:81/api/profil/add",
			type:"POST",
			contentType:"application/json",
			data: myJson,
			success: function(hasil){
				$("#ModalProfil").modal("hide")
				alert("simpan berhasil")
				kembali()
			}
		})
	})

	$(".btnbatal").click(function() {
		$("#ModalAlamat").modal("hide")
	})
})