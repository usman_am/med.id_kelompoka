$(document).ready(function(){

	$("#btnRefreshRole").hide()

	$("#btnAddRole").click(function(){
		$.ajax({
			url		: "http://localhost/admin/role/add",
			type	: "GET",
			dataType: "html",
			success	: function(hasil){
				$("#modalemail").modal("show") //show modal dulu
				$(".content").html(hasil)
			}
		})
		return false
	})
	
	$("#RoleCari").keyup(function(){
		let cari = $(this).val()
		list(cari)
	})
	
	list()
	function list(isi){
		let alamat = ""
		if(isi==undefined || isi==""){
			alamat="http://localhost:81/api/role/listadmin"
		}else{
			alamat="http://localhost:81/api/role/listadminbyname/"+isi
		}
		$.ajax({
			url : alamat,
			type: "GET",
			success : function(hasil){
				let txt=""
				txt+="<table class='thetable table table-hover'>"
				txt+="<thead>"
				txt+="<tr>"
				txt+="<th>Id</th>"
				txt+="<th>Nama</th>"
				txt+="<th>Kode</th>"
				txt+="<th> </th>"
				txt+="</tr>"
				txt+="</thead>"
				txt+="<tbody>"
				for(i=0;i<hasil.length;i++){
					txt+="<tr>"
					txt+="<td>"+hasil[i].id+"</td>"
					txt+="<td>"+hasil[i].name+"</td>"
					txt+="<td>"+hasil[i].code+"</td>"
					txt+="<td> <input type='button' value='ubah' class='btnEdit btn btn-warning' name='"+hasil[i].id+"'> "
					txt+="<input type='button' value='hapus' class='btnDelete btn btn-danger' name='"+hasil[i].id+"'> </td>"
					txt+="</tr>"
				}
				txt+="</tbody>"
				txt+="</table>"
				$("#isiTableRole").empty();
				$("#isiTableRole").append(txt)
				
			
					
				$(".btnEdit").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("idupdaterole",id);
					$.ajax({
						url		: "http://localhost/admin/role/edit",
						type	: "GET",
						dataType: "html",
						success	: function(hasil){
							$("#modalemail").modal("show") //show modal dulu
							$(".content").html(hasil)
						}
					})
					return false
				})
				
				$(".btnDelete").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("iddeleterole",id);
					$.ajax({
						url		: "http://localhost/admin/role/delete",
						type	: "GET",
						dataType: "html",
						success	: function(hasil){
							$("#modalemail").modal("show") //show modal dulu
							$(".content").html(hasil)
						}
					})
					return false
				})
				
					$(".thetable").DataTable({//buat datatable
					dom:  '<lf<t>ip>', //turunin posisi entries
					searching: false, 
					paging: true, 
					info: false,
					"language": {
						"paginate": {
				            "first": ">",
				            "last": "<",
				            "next": ">",
				            "previous": "<"
			        }}, //rename next back
			        
					pageLength : 3, //cutome nilai paging
    				lengthMenu: [[3, 5, 7, -1], [3, 5, 7, 'Todos']]
					});//hapus cari
			}
		})
	}
	
	$("#btnRefreshRole").click(function(){
		list()
	})
	
	
	
})