$(document).ready(function(){
	
	roleById()

function roleById(){
	let id = sessionStorage.getItem("idupdaterole")
	$.ajax({
		url		: "http://localhost:81/api/role/rolebyid/"+id,
		type	: "GET",
		success	: function(hasil){
			$("#iidrole").val(hasil.id)
			$("#inamerole").val(hasil.name)
			$("#icoderole").val(hasil.code)
		}
	})
}
	
	$("#rolenameempty").hide()
	$("#rolekodeempty").hide()
	$("#rolenochange").hide()
	$("#lblrole").attr("class","p1 text-muted")
	$("#lblkode").attr("class","p1 text-muted")
	
	$("#btnacceptrole").click(function(){
		$("#rolenameempty").hide()
		$("#rolekodeempty").hide()
		$("#rolenochange").hide()
		$("#lblrole").attr("class","p1 text-muted")
		$("#lblkode").attr("class","p1 text-muted")
		
		var role = $("#inamerole").val();
		var kode = $("#icoderole").val();
		var sp=false;
		var sp2=false;
		var sp3=false;
		
		cekrolename()
		function cekrolename(){
			if(role=="" || role==undefined){
				sp3=true
			$("#rolenameempty").show()
			$("#lblrole").attr("class","p4 text-muted")
			}else{
			$.ajax({
				url		: "http://localhost:81/api/role/rolename/"+role,
				type	: "GET",
				success	: function(hasil){
					if(hasil==0){sp=true}
					cekrolecode(sp)
				}
			})
			}
		}
			
		function cekrolecode(sp){
			if(kode=="" || kode==undefined){
				sp3=true
				$("#rolekodeempty").show()
				$("#lblkode").attr("class","p4 text-muted")
			}else{
				$.ajax({
						url		: "http://localhost:81/api/role/rolecode/"+kode,
						type	: "GET",
						success	: function(hasil){
							if(hasil==0){sp2=true}
							gethasil(sp,sp2)
						}
					})	
			}
		}
		
		function gethasil(sp,sp2){
			if(sp3==true){}
			else if(sp==false && sp2==false){$("#rolenochange").show()
			}else{
				saveedit()
			}				
		}
	})
	
	function saveedit(){
		var user = $("#userlogin").val()
		var obj={}
		obj.id = $("#iidrole").val()
		obj.name = $("#inamerole").val()
		obj.code = $("#icoderole").val()
		obj.update_by = user
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/role/edit",
			type		: "PUT",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				$("#modalemail").modal("hide")
				$("#btnRefreshRole").trigger('click')
			}
		})
	}
	
	
	$("#btncancelrole").click(function(){
		$("#modalemail").modal("hide")	
	})
	
})