$(document).ready(function(){
	
	$("#rolenameempty").hide()
	$("#rolekodeempty").hide()
	$("#rolenameexists").hide()
	$("#rolekodeexists").hide()
	$("#lblrole").attr("class","p1 text-muted")
	$("#lblkode").attr("class","p1 text-muted")
	
	$("#btnacceptrole").click(function(){
		$("#rolenameempty").hide()
		$("#rolekodeempty").hide()
		$("#rolenameexists").hide()
		$("#rolekodeexists").hide()
		$("#lblrole").attr("class","p1 text-muted")
		$("#lblkode").attr("class","p1 text-muted")
		
		var role = $("#inamerole").val();
		var kode = $("#icoderole").val();
		if(role=="" || role==undefined){
			$("#rolenameempty").show()
			$("#lblrole").attr("class","p4 text-muted")
		}else{
			$.ajax({
				url		: "http://localhost:81/api/role/rolename/"+role,
				type	: "GET",
				success	: function(hasil){
					if(hasil!=0){
						$("#rolenameexists").show()
					}
				}
			})
		}
		
		next12()
		function next12(){
			if(kode=="" || kode==undefined){
				$("#rolekodeempty").show()
				$("#lblkode").attr("class","p4 text-muted")
				}else{
					$.ajax({
						url		: "http://localhost:81/api/role/rolecode/"+kode,
						type	: "GET",
						success	: function(hasil){
							if(hasil!=0){
								$("#rolekodeexists").show()
							}else{
								addrole(role, kode)
							}
						}
					})	
				}
		}
		
	})
	
	function addrole(role, kode){
		var user = $("#userlogin").val()
		$.ajax({
			url		: "http://localhost:81/api/user/getidbyemail/"+user,
			type	: "GET",
			success	: function(hasil){
				var nil = hasil;
				addrole2(role, kode, nil)
			}
		})
	}
	
	function addrole2(role, kode, nil){
		var obj={}
		obj.name = role
		obj.code = kode
		obj.created_by = nil
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/role/post",
			type		: "POST",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				$("#modalemail").modal("hide")
				$("#btnRefreshRole").trigger('click')
			}
		})
	}
	
	$("#btncancelrole").click(function(){
		$("#modalemail").modal("hide")	
	})
	
})
