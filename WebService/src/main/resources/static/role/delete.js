$(document).ready(function(){
	
	roleById()

	function roleById(){
		let id = sessionStorage.getItem("iddeleterole")
		$.ajax({
			url		: "http://localhost:81/api/role/rolebyid/"+id,
			type	: "GET",
			success	: function(hasil){
				//$("#iidrole").val(hasil.id)
				//$("#inamerole").val(hasil.name)
				//$("#icoderole").val(hasil.code)
				document.getElementById("roledeleteshow").innerHTML = hasil.name+" ?";
			}
		})
	}
	
	$("#delroleaccept").click(function(){
		let id = sessionStorage.getItem("iddeleterole")
		var user = $("#userlogin").val()
		var obj={}
		obj.id = id
		obj.update_by = user
		var myJson = JSON.stringify(obj)
		$.ajax({
			url			: "http://localhost:81/api/role/delete",
			type		: "DELETE",
			contentType	: "application/json",
			data		: myJson,
			success		: function(hasil){
				$("#modalemail").modal("hide")
				$("#btnRefreshRole").trigger('click')
			}
		})
})
	
	
	$("#delrolecancel").click(function(){
		$("#modalemail").modal("hide")	
	})
})