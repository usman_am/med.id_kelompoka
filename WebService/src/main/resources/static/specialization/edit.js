$(document).ready(function(){
	
	$(".pesan").hide()
	
	function specializationById(){
		let id = sessionStorage.getItem("id")
		$.ajax({
			url : "http://localhost:81/api/specialization/list/"+id,
			type : "GET",
			success : function(hasil){
				$("#inNama").val(hasil.name)
			}
		})
	}
	
	specializationById()
	
	$("#btnSimpan").click(function(){
		let id = sessionStorage.getItem("id")
		
		if ($('.name').val().length == 0 || $('.name').val() == undefined || $('name').val() == " ") {
			$(".pesan").show()
			return false
		} else {
			var obj = {}
			obj.id = id
			obj.name = $("#inNama").val()

			var myjson = JSON.stringify(obj)

			/*$.ajax({
				url: "http://localhost:81/api/specialization/edit",
				type: "PUT",
				contentType: "application/json",
				data: myjson,
				success: function(hasil) {
					$("#modalSpecialization").modal('hide')
					
					kembali()
				}
			})*/
			
			$.ajax({
				url: "http://localhost:81/api/specialization/edit",
				type: "PUT",
				contentType: "application/json",
				data: myjson,
				success: function(hasil) {
					confirm()
					$("#btnSimpan").click(function() {
						$.ajax({
							url: "http://localhost/confirmspecialization",
							type: "GET",
							dataType: "html",
							success: function(hasil) {
								//alert(1)
								$("#modalSpecialization1").modal('show')
								$(".isiModalSpecialization1").html(hasil)
							}
						})
					})
					
		
						/*$("#modalSpecialization").modal('hide')
						
						kembali()*/
				}
			})
		}
				
	})
	
	$("#btnBatal").click(function(){
		$("#modalSpecialization").modal('hide')
	})
	
	function kembali(){
		$.ajax({
				url: "http://localhost/specialization",
				type: "GET",
				dataType:"html",
				success: function(hasil) {
					$(".isimain").html(hasil)
				}
			})
	}
	
})
