$(document).ready(function(){
	
	/*$("#iSearch").keyup(function(){
		let cari = $(this).val()
		if(list(cari) == null) {
			$("ModalSpecialization").html("Data Not  Found")
		} else {
			list(cari)
		}
		
	})*/
	
	$("#iSearch").keyup(function(){
		let cari = $(this).val()
		if(cari.name == null) {
			list(cari).html("Data Not Found")
		} else {
			list(cari)
		}
		
	})
	
	
	list()
	
	function list(isi){
		let alamat = ""
		if(isi == undefined || isi == "") {
			alamat = "http://localhost:81/api/specialization/list"
		} else {
			alamat = "http://localhost:81/api/specialization/listbyname/"+isi
		}
		
		$.ajax({
			url: alamat,
			type: "GET",
			success:function(hasil){
				let txt = ""
				for(i=0; i<hasil.length; i++){
					txt += "<tr>"
					txt += "<td>"+hasil[i].name+"</td>"
					txt += "<td><button class='btnedit btn btn-warning' name='"+hasil[i].id+"'>Edit</button>   <button class='btndelete btn btn-danger' name='"+hasil[i].id+"'>Delete</button></td>"
					txt += "</tr>"
				}
				$(".tb1").empty()
				$(".tb1").append(txt)
				
				$(".btnedit").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/editspecialization",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							if(hasil == null){
								$("#modalSpecialization").modal('show')
								$(".isiModalSpecialization").html("Data Not Found")
							} else {
								$("#modalSpecialization").modal('show')
								$(".isiModalSpecialization").html(hasil)
							}
									
						}
					})
					return false
				})
				
				$(".btndelete").click(function(){
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id)
					$.ajax({
						url : "http://localhost/deletespecialization",
						type : "GET",
						dataType : "html",
						success : function(hasil){
							$("#modalSpecialization").modal('show')
							$(".isiModalSpecialization").html(hasil)
						}
					})
					return false
				})
			}
		})
	}
	
	/*$("#iSearch").keyup(function(){
		let cari = $(this).val()
		list(cari)
	})*/
	
	$("#btnTambah").click(function(){
		$.ajax({
			url: "http://localhost/addspecialization",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#modalSpecialization").modal('show')
				$(".isiModalSpecialization").html(hasil)
			}
		})
		return false
	})
	
})
