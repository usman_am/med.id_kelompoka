$(document).ready(function() {
	let doctorid = sessionStorage.getItem("doctorid")

	list()
	function list() {
		$.ajax({
			url: "http://localhost:81/api/tabtindakan/list/" + doctorid,
			type: "GET",
			success: function(hasil) {
				console.log(hasil)
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<span>"
					txt += "<span class='badge rounded-pill bg-primary'>"+hasil[i].name+"<a href='#' class='hapus text-light' style='text-decoration:none;' name='"+hasil[i].id+"'><span class='fg'> X </span></a></span>"
					txt += "</span>"
				}
				$(".isiTindakan").append(txt)
				
				$(".hapus").click(function() {
					let id = $(this).attr("name")
					sessionStorage.setItem("treatmentid", id)
					$.ajax({
						url: "http://localhost/hapustindakan",
						type: "Get",
						dataType: "html",
						success: function(hasil) {
							$("#ModalTindakan").modal("show")
							$(".isiModalTindakan").html(hasil)
						}
					})
					return false
				})
			}
		})
	}

	$(".tambah").click(function() {
		$.ajax({
			url: "http://localhost/tambahtindakan",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#ModalTindakan").modal("show")
				$(".isiModalTindakan").html(hasil)
			}
		})
		return false
	})
})