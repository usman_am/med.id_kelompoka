$(document).ready(function(){
	
	$(".pesan").hide()
	
	function hubunganPasienById(){
		let id = sessionStorage.getItem("id")
		$.ajax({
			url : "http://localhost:81/api/hubunganpasien/list/"+id,
			type : "GET",
			success : function(hasil){
				$("#inNama").val(hasil.name)
			}
		})
	}
	
	hubunganPasienById()
	
	$("#btnSimpan").click(function(){
		let id = sessionStorage.getItem("id")
		
		if ($('.nama').val().length == 0 || $('.nama').val() == undefined || $('.nama').val() == " ") {
			$(".pesan").show()
			return false
		} else {
			var obj = {}
			obj.id = id
			obj.name = $("#inNama").val()

			var myjson = JSON.stringify(obj)

			$.ajax({
				url: "http://localhost:81/api/hubunganpasien/edit",
				type: "PUT",
				contentType: "application/json",
				data: myjson,
				success: function(hasil) {
					$("#modalHubunganPasien").modal('hide')
					alert("Edit Berhasil")
					//kembali()
				}
			})
		}		
	})
	
	$("#btnBatal").click(function(){
		$("#modalHubunganPasien").modal('hide')
	})
	
})