$(document).ready(function(){
	
	hubunganPasienById()
	
	function hubunganPasienById(){
		let id = sessionStorage.getItem("id")
		$.ajax({
			url : "http://localhost:81/api/hubunganpasien/list/"+id,
			type : "GET",
			success : function(hasil){
				$("#inNama").text("Anda akan menghapus "+hasil.name+" ?")
			}
		})
	}
	
	$("#btnHapus").click(function(){
		let id = sessionStorage.getItem("id")
		
		var obj={}
		obj.id = id
		obj.name = $("#inNama").val()
		
		var myjson = JSON.stringify(obj)
		
		$.ajax({
			url : "http://localhost:81/api/hubunganpasien/delete",
			type : "DELETE",
			contentType : "application/json",
			data : myjson,
			success : function(hasil){
				$("#modalHubunganPasien").modal('hide')
				alert("Delete Berhasil")
				//kembali()
			},
			error: function(ts) { alert(ts.responseText) }
		})
	})
	
	$("#btnBatal").click(function(){
		$("#modalHubunganPasien").modal('hide')
	})
	
})